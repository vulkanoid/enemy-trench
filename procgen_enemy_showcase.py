from __future__ import annotations
import random
import copy
from typing import Dict, Iterator, List, Tuple, TYPE_CHECKING

import tcod

import entity_factories
from game_map import GameMap
import tile_types

if TYPE_CHECKING:
    from engine import Engine
    from entity import Entity

max_items_by_floor = [(1,1), (4, 2)]
max_enemies_by_floor =[(1, 2), (4, 3), (6, 5)]

item_chances: Dict[int, List[tuuple[Entity, int]]] = {
    0: [(entity_factories.health_pack, 30)],
    2: [(entity_factories.rock, 10)],
    4: [(entity_factories.throwing_knife, 25), (entity_factories.sword, 5)],
    6: [(entity_factories.grenade, 25), (entity_factories.helmetMark1, 15)],
}

def get_max_value_for_floor(weighted_chances_by_floor: List[Tuple[int, int]], floor: int) -> int:
    current_value = 0

    for floor_minimum, value in weighted_chances_by_floor:
        if floor_minimum > floor:
            break
        else:
            current_value = value
    return current_value

def get_entities_at_random(weighted_chances_by_floor: Dict[int, List[Tuple[Entity, int]]], number_of_entities: int, floor: int) -> List[Entity]:
    entity_weighted_chances = {}

    for key, values in weighted_chances_by_floor.items():
        if key > floor:
            break
        else:
            for value in values:
                entity = value[0]
                weighted_chance = value[1]

                entity_weighted_chances[entity] = weighted_chance

    entities = list(entity_weighted_chances.keys())
    entity_weighted_chance_values = list(entity_weighted_chances.values())

    chosen_entities = random.choices(entities, weights=entity_weighted_chance_values, k=number_of_entities)

    return chosen_entities

class RectangularRoom:
    def __init__(self, x: int, y: int, width: int, height: int):
        self.x1 = x
        self.y1 = y
        self.x2 = x + width
        self.y2 = y + height

    @property
    def center(self) -> tuple[int, int]:
        center_x = int((self.x1 + self.x2) / 2)
        center_y = int((self.y1 + self.y2) / 2)
        return center_x, center_y

    @property
    def inner(self) -> Tuple[slice, slice]:
        return slice(self.x1 + 1, self.x2), slice(self.y1+1, self.y2)

    def intersects(self, other: RectangularRoom) -> bool:
        return (self.x1 <= other.x2 and self.x2 >= other.x1 and self.y1 <= other.y2 and self.y2 >= other.y1)

def place_entities(room: RectangularRoom, dungeon: GameMap, floor_number: int, engine: Engine) -> None:
    if engine.is_hardcore_mode == False:
        number_of_enemies = random.randint(0, get_max_value_for_floor(max_enemies_by_floor, floor_number))
        number_of_items = random.randint(0, get_max_value_for_floor(max_items_by_floor, floor_number))
    else:
        number_of_enemies = random.randint(0, get_max_value_for_floor(max_enemies_by_floor_hard, floor_number))
        number_of_items = random.randint(0, get_max_value_for_floor(max_items_by_floor_hard, floor_number))

    items: List[Entity] = get_entities_at_random(item_chances, number_of_items, floor_number)
        
        
    

def tunnel_between(start: Tuple[int, int], end: Tuple[int, int]) -> Iterator[Tuple[int, int]]:
    x1, y1 = start
    x2, y2 = end
    if random.random() < 0.5: # 50% chance
        #move horrizontally then vertically
        corner_x, corner_y = x2, y1
    else:
        #move vertically then horizonally
        corner_x, corner_y = x1, y2

    # generate coridinates for tunnel
    for x, y in tcod.los.bresenham((x1, y1), (corner_x, corner_y)).tolist():
        yield x, y
    for x, y in tcod.los.bresenham((corner_x, corner_y), (x2, y2)).tolist():
        yield x, y    


def generate_dungeon_showcase(max_rooms: int, room_min_size: int, room_max_size: int, map_width: int, map_height: int, engine: Engine) -> GameMap:
    #generate a new map
    player = engine.player
    dungeon = GameMap(engine, map_width, map_height, False, entities=[player])
    #add rooms to list
    rooms: List[RectangularRoom] = []

    center_of_last_room = (0, 0)
    

    #loop through all rooms
    for r in range(max_rooms):
        room_width = random.randint(room_min_size, room_max_size)
        room_height = random.randint(room_min_size, room_max_size)

        x = random.randint(0, dungeon.width - room_width - 1)
        y = random.randint(0, dungeon.height - room_height - 1)

        
        #create room
        new_room = RectangularRoom(x, y, room_width, room_height)
       
        
        #check if any room collides
        if any(new_room.intersects(other_room) for other_room in rooms):
            continue # This room intercets
        
        
        dungeon.tiles[new_room.inner] = tile_types.floor
        

        if len(rooms) == 0:
            player.place(*new_room.center, dungeon)
            '''entity = copy.deepcopy(entity_factories.digger)
            entity.place(player.get_x, player.get_y + 2, dungeon)

            entity = copy.deepcopy(entity_factories.scout)
            entity.place(player.get_x + 2, player.get_y + 2, dungeon)

            entity = copy.deepcopy(entity_factories.rifleman)
            entity.place(player.get_x + 2, player.get_y, dungeon)

            entity = copy.deepcopy(entity_factories.heavy)
            entity.place(player.get_x + 2, player.get_y - 2, dungeon)

            entity = copy.deepcopy(entity_factories.explosives)
            entity.place(player.get_x, player.get_y - 2, dungeon)

            entity = copy.deepcopy(entity_factories.fieldMedic)
            entity.place(player.get_x - 2, player.get_y - 2, dungeon)

            entity = copy.deepcopy(entity_factories.marksman)
            entity.place(player.get_x - 2, player.get_y, dungeon)

            entity = copy.deepcopy(entity_factories.bruiser)
            entity.place(player.get_x - 2, player.get_y + 2, dungeon)

            entity = copy.deepcopy(entity_factories.officer)
            entity.place(player.get_x + 3, player.get_y + 3, dungeon)'''
            entity = copy.deepcopy(entity_factories.heavy_boss)
            entity.place(player.get_x - 2, player.get_y, dungeon)

            entity = copy.deepcopy(entity_factories.marksman_boss)
            entity.place(player.get_x, player.get_y + 2, dungeon)

            entity = copy.deepcopy(entity_factories.flamer_boss)
            entity.place(player.get_x + 2, player.get_y, dungeon)
        else:
            for x, y in tunnel_between(rooms[-1].center, new_room.center):
                dungeon.tiles[x, y] = tile_types.floor
            center_of_last_room = new_room.center
            place_entities(new_room, dungeon, engine.game_world.current_floor, engine)

            dungeon.tiles[center_of_last_room] = tile_types.down_stairs
            dungeon.downstairs_location = center_of_last_room

            

        rooms.append(new_room)
    

   
    return dungeon
