from enum import auto, Enum

class RenderOrder(Enum):
    GAS = auto()
    CORPSE = auto()
    ITEM = auto()
    ACTOR = auto()