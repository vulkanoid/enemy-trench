class Impossible(Exception):
    """Impossible exception raised"""

class QuitWithoutSaving(SystemExit):
    """Exit the the game without saving"""