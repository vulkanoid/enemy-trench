from __future__ import annotations
import random
import copy
from typing import Dict, Iterator, List, Tuple, TYPE_CHECKING

import tcod

import entity_factories
from game_map import GameMap
import tile_types
import g
import soundfile
import tcod.sdl.audio

if TYPE_CHECKING:
    from engine import Engine
    from entity import Entity

max_items_by_floor = [(1,3), (4, 4), (8, 6), (15, 8)]
max_enemies_by_floor =[(1, 2), (6, 3), (17, 4), (25, 5)]

max_items_by_floor_hard = [(1,1), (4, 2), (8, 3), (15, 4)]
max_enemies_by_floor_hard =[(1, 3), (6, 4), (17, 5), (25, 6)]

item_chances: Dict[int, List[tuuple[Entity, int]]] = {
    0: [(entity_factories.health_pack, 10)],
    1: [(entity_factories.rifle_ammo_pack, 10)],
    2: [(entity_factories.pistol_ammo_pack, 10)],
    3: [(entity_factories.rock, 8)],
    8: [(entity_factories.throwing_knife, 8)],
    11: [(entity_factories.mg_ammo_pack, 10)],
    12: [(entity_factories.sword, 5)],
    15: [(entity_factories.rifle_ammo_box, 10)],
    16: [(entity_factories.grenade, 5), (entity_factories.bulletProofVest, 10)],
    18: [(entity_factories.pistol_ammo_box, 10)],
    21: [(entity_factories.helmetMark1, 5)],
    22: [(entity_factories.mg_ammo_box, 10)],
    25: [(entity_factories.metalArmour, 5)],
}

enemy_chances: Dict[int, List[Tuple[Entity, int]]] = {
    0: [(entity_factories.digger, 20)],
    2: [(entity_factories.scout, 20)],
    4: [(entity_factories.rifleman, 10)],
    6: [(entity_factories.rifleman, 15)],
    8: [(entity_factories.rifleman, 20)],
    11: [(entity_factories.heavy, 5)],
    12: [(entity_factories.heavy, 10)],
    14: [(entity_factories.explosives, 5)],
    16: [(entity_factories.explosives, 10)],
    18: [(entity_factories.fieldMedic, 5)],
    19: [(entity_factories.fieldMedic, 10)],
    21: [(entity_factories.marksman, 5)],
    22: [(entity_factories.marksman, 10)],
    24: [(entity_factories.bruiser, 5)],
    26: [(entity_factories.bruiser, 10)],
    28: [(entity_factories.officer, 5)],
    29: [(entity_factories.officer, 10)]
}


def get_max_value_for_floor(weighted_chances_by_floor: List[Tuple[int, int]], floor: int) -> int:
    current_value = 0

    for floor_minimum, value in weighted_chances_by_floor:
        if floor_minimum > floor:
            break
        else:
            current_value = value
    return current_value

def get_entities_at_random(weighted_chances_by_floor: Dict[int, List[Tuple[Entity, int]]], number_of_entities: int, floor: int) -> List[Entity]:
    entity_weighted_chances = {}

    for key, values in weighted_chances_by_floor.items():
        if key > floor:
            break
        else:
            for value in values:
                entity = value[0]
                weighted_chance = value[1]

                entity_weighted_chances[entity] = weighted_chance

    entities = list(entity_weighted_chances.keys())
    entity_weighted_chance_values = list(entity_weighted_chances.values())

    chosen_entities = random.choices(entities, weights=entity_weighted_chance_values, k=number_of_entities)

    return chosen_entities

class RectangularRoom:
    def __init__(self, x: int, y: int, width: int, height: int):
        self.x1 = x
        self.y1 = y
        self.x2 = x + width
        self.y2 = y + height

    @property
    def center(self) -> tuple[int, int]:
        center_x = int((self.x1 + self.x2) / 2)
        center_y = int((self.y1 + self.y2) / 2)
        return center_x, center_y

    @property
    def inner(self) -> Tuple[slice, slice]:
        return slice(self.x1 + 1, self.x2), slice(self.y1+1, self.y2)

    def intersects(self, other: RectangularRoom) -> bool:
        return (self.x1 <= other.x2 and self.x2 >= other.x1 and self.y1 <= other.y2 and self.y2 >= other.y1)

def place_entities(room: RectangularRoom, dungeon: GameMap, floor_number: int, engine: Engine) -> None:
    if engine.is_hardcore_mode == False:
        number_of_enemies = random.randint(0, get_max_value_for_floor(max_enemies_by_floor, floor_number))
        number_of_items = random.randint(0, get_max_value_for_floor(max_items_by_floor, floor_number))
    else:
        number_of_enemies = random.randint(0, get_max_value_for_floor(max_enemies_by_floor_hard, floor_number))
        number_of_items = random.randint(0, get_max_value_for_floor(max_items_by_floor_hard, floor_number))

    enemies: List[Entity] = get_entities_at_random(enemy_chances, number_of_enemies, floor_number)
    items: List[Entity] = get_entities_at_random(item_chances, number_of_items, floor_number)
    for entity in enemies + items:
        x = random.randint(room.x1 + 1, room.x2 - 1)
        y = random.randint(room.y1 + 1, room.y2 - 1)
        
        if entity in enemies:
            if entity.inventory.inventory_empty():
                if entity.name == "Digger":
                    spade = copy.deepcopy(entity_factories.spade)
                    spade.parent = entity.inventory

                    entity.inventory.items.append(spade)
                    entity.equipment.toggle_equip(spade, add_message=False)

                if entity.name == "Scout":
                    randWeapon = random.randint(0, 10)
                    if randWeapon == 1:
                        pistol = copy.deepcopy(entity_factories.RevMod1892)
                    elif randWeapon == 2:
                        pistol = copy.deepcopy(entity_factories.LangPistol)
                    elif randWeapon == 3:
                        pistol = copy.deepcopy(entity_factories.SModel1908)
                    else:
                        pistol = copy.deepcopy(entity_factories.MC96)
                    pistol.parent = entity.inventory

                    entity.inventory.items.append(pistol)
                    entity.equipment.toggle_equip(pistol, add_message=False)

                    pistol_ammo_pack = copy.deepcopy(entity_factories.pistol_ammo_pack)
                    pistol_ammo_pack.parent = entity.inventory

                    entity.inventory.items.append(pistol_ammo_pack)

                if entity.name == "Rifleman":
                    if floor_number < 16:
                        randWeapon = random.randint(0, 10)
                        if randWeapon == 1:
                            rifle = copy.deepcopy(entity_factories.LE186)
                        elif randWeapon == 2:
                            rifle = copy.deepcopy(entity_factories.BERT)
                        elif randWeapon == 3:
                            randBayo = random.randint(0, 10)
                            if randBayo == 1:
                                rifle = copy.deepcopy(entity_factories.LE186Bayonet)
                            if randBayo == 2:
                                rifle = copy.deepcopy(entity_factories.BERTBayonet)
                            else:
                                rifle = copy.deepcopy(entity_factories.g98Bayonet)
                        else:
                            rifle = copy.deepcopy(entity_factories.g98)
                    else:
                        randBayo = random.randint(0, 10)
                        if randBayo == 1:
                            rifle = copy.deepcopy(entity_factories.KARBA90Bayonet)
                        else:
                            rifle = copy.deepcopy(entity_factories.KARBA90)
                    rifle.parent = entity.inventory

                    entity.inventory.items.append(rifle)
                    entity.equipment.toggle_equip(rifle, add_message=False)

                    rifle_ammo_pack = copy.deepcopy(entity_factories.rifle_ammo_pack)
                    rifle_ammo_pack.parent = entity.inventory

                    entity.inventory.items.append(rifle_ammo_pack)

                if entity.name == "Heavy":
                    if floor_number < 18:
                        randWeapon = random.randint(0, 10)
                        if randWeapon == 1:
                            MG = copy.deepcopy(entity_factories.VICR)
                        else:
                            MG = copy.deepcopy(entity_factories.MAG80)
                    else:
                        MG = copy.deepcopy(entity_factories.MAG1580)
                    MG.parent = entity.inventory

                    entity.inventory.items.append(MG)
                    entity.equipment.toggle_equip(MG, add_message=False)

                    mg_ammo_pack = copy.deepcopy(entity_factories.mg_ammo_pack)
                    mg_ammo_pack.parent = entity.inventory

                    entity.inventory.items.append(mg_ammo_pack)

                if entity.name == "Explosives":
                    if floor_number < 21:
                        randWeapon = random.randint(0, 10)
                        if randWeapon == 1:
                            pistol = copy.deepcopy(entity_factories.RevMod1892)
                        elif randWeapon == 2:
                            pistol = copy.deepcopy(entity_factories.LangPistol)
                        elif randWeapon == 3:
                            pistol = copy.deepcopy(entity_factories.SModel1908)
                        else:
                            pistol = copy.deepcopy(entity_factories.MC96)
                    else:
                        pistol = copy.deepcopy(entity_factories.LPistol)
                    pistol.parent = entity.inventory

                    entity.inventory.items.append(pistol)
                    entity.equipment.toggle_equip(pistol, add_message=False)


                    grenade = copy.deepcopy(entity_factories.grenade)
                    grenade.parent = entity.inventory
                    entity.inventory.items.append(grenade)
                    
                if entity.name == "FieldMedic":
                    if floor_number < 23:
                        randWeapon = random.randint(0, 10)
                        if randWeapon == 1:
                            pistol = copy.deepcopy(entity_factories.RevMod1892)
                        elif randWeapon == 2:
                            pistol = copy.deepcopy(entity_factories.LangPistol)
                        elif randWeapon == 3:
                            pistol = copy.deepcopy(entity_factories.SModel1908)
                        else:
                            pistol = copy.deepcopy(entity_factories.MC96)
                    else:
                        pistol = copy.deepcopy(entity_factories.LPistol)
                    pistol.parent = entity.inventory

                    entity.inventory.items.append(pistol)
                    entity.equipment.toggle_equip(pistol, add_message=False)

                    health_pack = copy.deepcopy(entity_factories.health_pack)
                    health_pack.parent = entity.inventory
                    entity.inventory.items.append(health_pack)

                if entity.name == "Marksman":
                    if floor_number < 28:
                        randWeapon = random.randint(0, 10)
                        if randWeapon == 1:
                            rifle = copy.deepcopy(entity_factories.LE186Sniper)
                        elif randWeapon == 2:
                            rifle = copy.deepcopy(entity_factories.BERTSniper)
                        else:
                            rifle = copy.deepcopy(entity_factories.g98Sniper)
                    else:
                        rifle = copy.deepcopy(entity_factories.KARBA90Sniper)
                    rifle.parent = entity.inventory

                    entity.inventory.items.append(rifle)
                    entity.equipment.toggle_equip(rifle, add_message=False)

                    rifle_ammo_pack = copy.deepcopy(entity_factories.rifle_ammo_pack)
                    rifle_ammo_pack.parent = entity.inventory

                    entity.inventory.items.append(rifle_ammo_pack)

                if entity.name == "Officer":
                    LPistol = copy.deepcopy(entity_factories.LPistol)
                    LPistol.parent = entity.inventory

                    entity.inventory.items.append(LPistol)
                    entity.equipment.toggle_equip(LPistol, add_message=False)

                    pistol_ammo_pack = copy.deepcopy(entity_factories.pistol_ammo_pack)
                    pistol_ammo_pack.parent = entity.inventory

                    entity.inventory.items.append(pistol_ammo_pack)
                

                gasMask = copy.deepcopy(entity_factories.gasMask)
                gasMask.parent = entity.inventory

                entity.inventory.items.append(gasMask)

        if not any(entity.x == x and entity.y == y for entity in dungeon.entities):
            entity.spawn(dungeon, x, y)
        
        
        
    

def tunnel_between(start: Tuple[int, int], end: Tuple[int, int]) -> Iterator[Tuple[int, int]]:
    x1, y1 = start
    x2, y2 = end
    if random.random() < 0.5: # 50% chance
        #move horrizontally then vertically
        corner_x, corner_y = x2, y1
    else:
        #move vertically then horizonally
        corner_x, corner_y = x1, y2

    # generate coridinates for tunnel
    for x, y in tcod.los.bresenham((x1, y1), (corner_x, corner_y)).tolist():
        yield x, y
    for x, y in tcod.los.bresenham((corner_x, corner_y), (x2, y2)).tolist():
        yield x, y    


def generate_trench(max_rooms: int, room_min_size: int, room_max_size: int, map_width: int, map_height: int, engine: Engine) -> GameMap:
    #generate a new map
    player = engine.player
    dungeon = GameMap(engine, map_width, map_height, False, entities=[player])
    #add rooms to list
    rooms: List[RectangularRoom] = []
    indoor: List[RectangularRoom] = []
    currentTrenchRooms = []

    center_of_exit_room = (0, 0)

    gas_x = random.randint(0, map_width-1)
    gas_y = random.randint(0, map_height-1)
    

    spawnGas = random.randint(0, 3)
    

    trench_width = dungeon.width - 2
    trench_height = random.randint(3, 5)

    x = 0
    y = dungeon.height

    isRandPlayerSpawn = random.randint(1, trench_width-1)

    trenchNumb = random.randint(2, 4)
    trenchDist = random.randint(6, 8)
    startY = y

    room_offset = dungeon.height
    prev_offset = 0
    trenchOrRooms = 1
    #create trench and rooms
    for y_width in range(trenchNumb):
        trench_height = random.randint(3, 5)
        startY = y
        prev_offset = room_offset
        room_offset = startY - trench_height - trenchDist
        tunnel_spawn = random.randint(0, trench_width)
        if y_width > 0:
            trenchOrRooms = random.randint(0, 2)
        if trenchOrRooms >= 1:
            for x_width in range(trench_width):
                isRandDir = random.randint(0, 4)
                if isRandDir == 1:
                    y = y + 1
                if isRandDir == 2:
                    y = y - 1
                if y >= dungeon.height - trench_height:
                    y = dungeon.height - trench_height - 1
                if y < 0:
                    y = 0
                if y <= room_offset:
                    y = room_offset + 1
                if y + trench_height >= prev_offset + 1:
                    y = prev_offset - trench_height - 1
                
                
                new_room = RectangularRoom(x + x_width, y, 2, trench_height)
                
                isRandElements = random.randint(0, 10)
                
                dungeon.tiles[new_room.inner] = tile_types.floor

                if x_width == isRandPlayerSpawn and y_width == 0:
                    player.place(*new_room.center, dungeon)

                
                if isRandElements == 5:
                    place_entities(new_room, dungeon, engine.game_world.current_floor, engine)

                
                
                if y_width > 0:
                    if x_width == tunnel_spawn:
                        for x_tun, y_tun in tunnel_between(new_room.center, rooms[x_width].center):
                            dungeon.tiles[x_tun, y_tun] = tile_types.floor

                rooms.append(new_room)
            y = startY - trench_height - trenchDist
                
        else:
            numbOfRooms = random.randint(1, 4)
            room_width = random.randint(room_min_size, room_max_size)
            room_height = random.randint(room_min_size, room_max_size)
            rooms_created = 0
            for roomNumb in range(numbOfRooms):

                roomX = random.randint (0, dungeon.width)
                if roomX - room_width < 0:
                    roomX = 0
                elif roomX + room_width >= dungeon.width:
                    roomX = dungeon.width - room_width - 1
                roomY = y
                if y <= room_offset:
                    roomY = room_offset + 1
                    room_height = room_min_size
                if y + room_height >= prev_offset:
                    room_height = room_min_size
                    roomY = prev_offset - room_height - 1
                #create room
                new_room = RectangularRoom(roomX, roomY, room_width, room_height)
                place_entities(new_room, dungeon, engine.game_world.current_floor, engine)
                #check if any room collides
                if any(new_room.intersects(other_room) for other_room in rooms):
                    continue
                
                
                dungeon.tiles[new_room.inner] = tile_types.pallet
                if rooms_created > 0:
                    for x_tun, y_tun in tunnel_between(rooms[-1].center, new_room.center):
                        if y_tun == new_room.y2:
                            dungeon.tiles[x_tun, y_tun] = tile_types.door_closed
                        else:
                            dungeon.tiles[x_tun, y_tun] = tile_types.pallet
                for x_tun, y_tun in tunnel_between(new_room.center, rooms[roomX].center):
                    if y_tun == new_room.y2:
                        dungeon.tiles[x_tun, y_tun] = tile_types.door_closed
                    else:
                        dungeon.tiles[x_tun, y_tun] = tile_types.pallet
                rooms_created = rooms_created + 1
                rooms.append(new_room)
            y = startY - room_height - trenchDist
                
                    
                    

        
        
    

    randRoom = random.randint(0, len(rooms)-1)
    exit_room = rooms[randRoom]
    center_of_exit_room = exit_room.center
    dungeon.tiles[center_of_exit_room] = tile_types.down_stairs
    dungeon.downstairs_location = center_of_exit_room

    if dungeon.tiles[gas_x, gas_y] == tile_types.floor and spawnGas == 2:
        entity_factories.gas_canister.spawn(dungeon,  gas_x, gas_y)
    for entity in list(dungeon.entities).copy():
        if not dungeon.tiles["walkable"][entity.x, entity.y]:
            dungeon.entities.remove(entity)

   
    return dungeon
