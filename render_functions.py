from __future__ import annotations

from typing import Tuple, TYPE_CHECKING

import colour
import tcod
if TYPE_CHECKING:
    from tcod import Console
    from engine import Engine
    from game_map import GameMap

def get_names_at_location(x: int, y: int, game_map: GameMap) -> str:
    if not game_map.in_bounds(x, y) or not game_map.visible[x, y]:
        return ""

    names = ", ".join(entity.name for entity in game_map.entities if entity.x == x and entity.y == y)

    return names.capitalize()

def render_bar(console: Console, current_value: int, maximum_value: int, total_width: int) -> None:
    bar_width = int(float(current_value) / maximum_value * total_width)

    console.draw_rect(x=0, y=44, width=20, height=1, ch=1, bg=colour.bar_empty)

    if bar_width > 0:
        console.draw_rect(x=0, y=44, width=bar_width, height=1, ch=1, bg=colour.bar_filled)

    console.print(x=1, y=44, string=f"hp: {current_value}/{maximum_value}", fg=colour.bar_text)

def render_area_level(console: Console, room_level: int, location: Tuple[int, int]) -> None:
    x, y = location

    console.print(x=x, y=y, string=f"Area level: {room_level}")

def render_names_at_mouse_location(console: Console, x: int, y: int, engine: Engine) -> None:
    mouse_x, mouse_y = engine.mouse_location

    names_at_mouse_location = get_names_at_location(x=mouse_x, y=mouse_y, game_map=engine.game_map)

    console.print(x=x, y=y, string=names_at_mouse_location)

def render_frames(console: Console, location1: Tuple[int, int], location2: Tuple[int, int]) -> None:
    x1, y1 = location1
    x2, y2 = location2
    console.draw_frame(x=x1, y=y1, width=90, height=30, decoration="╔═╗║ ║╚═╝")
    #console.draw_frame(x=x2, y=y2, width=40, height=90, decoration="╔═╗║ ║╚═╝")


def render_weapon_name(console: Console, weapon_name: str, location: Tuple[int, int]) -> None:
    x, y = location
    if "w/ Bayo" not in weapon_name:
        console.print(x=x, y=y, string=f"Weapon:")
        console.print(x=x, y=y+1, string=f"{weapon_name}")
    else:
        weapon_sub = weapon_name.split("w/")
        console.print(x=x, y=y, string=f"Weapon:")
        console.print(x=x, y=y+1, string=f"{weapon_sub[0]}")
        console.print(x=x, y=y+2, string=f"w/ Bayonet")

def render_ammo_amount(console: Console, ammo_amount: int, max_ammo: int, location: Tuple[int, int]) -> None:
    x, y = location

    console.print(x=x, y=y, string=f"Ammo: {ammo_amount}/{max_ammo}")
    console.print(x=x, y=y+1, string=f"{ammo_amount}/{max_ammo}")

def render_helmet_name(console: Console, helmet_name: str, location: Tuple[int, int]) -> None:
    x, y = location
    console.print(x=x, y=y, string=f"Helmet:")
    if helmet_name == "":
        console.print(x=x, y=y+1, string=f"[Empty]")
    else:
        console.print(x=x, y=y+1, string=f"{helmet_name}")

def render_mask_equipped(console: Console, mask_equiped: bool, location: Tuple[int, int]) -> None:
    x, y = location
    console.print(x=x, y=y, string=f"Mask:")
    if mask_equiped == False:
        console.print(x=x, y=y+1, string=f"[Empty]")
    else:
        console.print(x=x, y=y+1, string=f"Eqiupped")

def render_armour_name(console: Console, armour_name: str, location: Tuple[int, int]) -> None:
    x, y = location
    console.print(x=x, y=y, string=f"Armour:")
    if armour_name == "":
        console.print(x=x, y=y+1, string=f"[Empty]")
    else:
        console.print(x=x, y=y+1, string=f"{armour_name}")

def render_interface_titles(console: Console, name: str, location: Tuple[int, int]) -> None:
    x, y = location
    console.print(x=x, y=y, string=f"{name}")

def render_equipment(console: Console, weapon_name: str, ammo_amount: int, max_ammo: int, helmet_name: str, mask_equiped: bool, armour_name: str, location: Tuple[int, int], inv_hover: bool) -> None:
    x, y = location
    if inv_hover == True:
        console.draw_frame(x=x-1, y=y, width=20, height=7)
        #console.draw_rect(x=x, y=y, width=19, height=6, ch=1, bg=colour.white)
    console.print(x=x, y=y, string=f"Equipment: ")

    if helmet_name == "":
        console.print(x=x, y=y+1, string=f"He: [Empty]")
    else:
        console.print(x=x, y=y+1, string=f"He: {helmet_name}")

    if mask_equiped == False:
        console.print(x=x, y=y+2, string=f"Ma: [Empty]")
    else:
        console.print(x=x, y=y+2, string=f"Ma: Eqiupped")

    if weapon_name == "":
        console.print(x=x, y=y+3, string=f"We: [Empty]")
    else: 
        if "w/ Bayo" not in weapon_name:
            console.print(x=x, y=y+3, string=f"We: {weapon_name}")
        else:
            weapon_sub = weapon_name.split("w/")
            console.print(x=x, y=y+3, string=f"We: {weapon_sub[0]} w/ B")

        console.print(x=x+1, y=y+4, string=f"Am: {ammo_amount}/{max_ammo}")
    
    if armour_name == "":
        console.print(x=x, y=y+5, string=f"Ar: [Empty]")
    else:
        console.print(x=x, y=y+5, string=f"Ar: {armour_name}")

def render_btn(console: Console, text: str, is_hover: bool, location: Tuple[int, int]) -> None:
    x, y = location
    if is_hover == True:
        console.draw_frame(x=x-1, y=y-1, width=6, height=3)
    console.print(x=x, y=y, string=text)

def render_player_panic(console: Console, turns: int, location: Tuple[int, int]) -> None:
    x, y = location
    console.print(x=x, y=y, string="PANICKED!!!")

def render_story(console: Console, area_lvl: int) -> None:
    x = 0
    title = ""
    if area_lvl == 1:
        title = "Chapter 1"
    elif area_lvl == 11:
        title = "Chapter 2"
    elif area_lvl == 21:
        title = "Chapter 3"
    console.draw_frame(x=x, y=0, width=80, height=50, title=title, clear=True, fg=(255, 255, 255), bg=(0, 0, 0))

    if area_lvl == 1:
        console.print(x=x + 1, y=1, string="The 1st of September 1916 - The Somme")
        console.print(x=x + 1, y=2, string="For weeks the battle has been raging. An endless barrage of artillery screams")
        console.print(x=x + 1, y=3, string="through the battlefield as British men try to take German positions.")
        
        console.print(x=x + 1, y=5, string="You are one of those British men. You have been called up to be sent over the")
        console.print(x=x + 1, y=6, string="top and to try and infiltrate German positions on the other side of no man's")
        console.print(x=x + 1, y=7, string="land.")

        console.print(x=x + 1, y=9, string="At the blow of the whistle you and your men go over the top and charge at the")
        console.print(x=x + 1, y=10, string="enemy. A hale of bullets screeches past you as artillery fire hammers the")
        console.print(x=x + 1, y=11, string="German trenches. You make your way quickly into the trench dodging past")
        console.print(x=x + 1, y=12, string="various bullets and barbbed wire.")

        console.print(x=x + 1, y=14, string="Upon arriving, you notice this section is quite barren. All that remains after")
        console.print(x=x + 1, y=15, string="the barrarge are a few men who were digging dugouts at the time of the")
        console.print(x=x + 1, y=16, string="attack. Sadly, it appears you're the only one left of your squad, the")
        console.print(x=x + 1, y=17, string="men you marched with lay in the fields of no man's land.")

        console.print(x=x + 1, y=19, string="Up the line you hear machine gun fire and the screams of men as they")
        console.print(x=x + 1, y=20, string="desperatly try to break the line. The British artillery must be unable")
        console.print(x=x + 1, y=21, string="to reach a machine gunner duggout. You'll need to take care of that if")
        console.print(x=x + 1, y=22, string="you hope to save any of your fellow men and secure this positon.")

        console.print(x=x + 1, y=24, string="Don't let those who have fallen die in vain.")


        console.print(x=x + 1, y=26, string="Press any key to continue.", fg=colour.welcome_text)
    elif area_lvl == 11:
        console.print(x=x + 1, y=1, string="With the machine gunner now vanquished you leave the area.")
        
        console.print(x=x + 1, y=3, string="Upon leaving, you notice a soldier on the floor and that he's one of yours.")

        console.print(x=x + 1, y=5, string="After approaching him you discover the man is alive but is very close")
        console.print(x=x + 1, y=6, string="to death.")

        console.print(x=x + 1, y=8, string="He tells you he's gathered some German intelligence about")
        console.print(x=x + 1, y=9, string="an ambush planned in the forests by a well-trained sniper that one")
        console.print(x=x + 1, y=10, string="of the companies will soon be heading into. They've set a trap.")

        console.print(x=x + 1, y=12, string="He pleads with you to approach the forest from a different direction and")
        console.print(x=x + 1, y=13, string="to take out the sniper and those planning to set the trap before the company")
        console.print(x=x + 1, y=14, string="arrives.")


        console.print(x=x + 1, y=16, string="Realising you're alone and any delay could lead to their deaths you make")
        console.print(x=x + 1, y=17, string="your way to the muddy forest, hoping to stop the sniper before it's too")
        console.print(x=x + 1, y=18, string="late.")


        console.print(x=x + 1, y=20, string="Press any key to continue.", fg=colour.welcome_text)
    elif area_lvl == 21:
        console.print(x=x + 1, y=1, string="After the snipers defeat you breath a sigh of relief, knowing your")
        console.print(x=x + 1, y=2, string="comrades are safe.")
        
        console.print(x=x + 1, y=4, string="The feeling is cut short though as you notice off in the distance a great")
        console.print(x=x + 1, y=5, string="blaze.")

        console.print(x=x + 1, y=7, string="Using the scope from the sniper you make out that the blaze is being caused")
        console.print(x=x + 1, y=8, string="by a German soldier carrying a Flamethrower. The Germans must be sending")
        console.print(x=x + 1, y=9, string="these guys in to retake the line.")

        console.print(x=x + 1, y=11, string="You leap to your feet and mark out some cordinates to approach")
        console.print(x=x + 1, y=12, string="the flamethrower by suprise.")

        console.print(x=x + 1, y=14, string="This will be a difficult task but if you fail, one can only wonder the")
        console.print(x=x + 1, y=15, string="horrors that will await your men.")

        console.print(x=x + 1, y=17, string="Press any key to continue.", fg=colour.welcome_text)

    elif area_lvl == 31:
        console.print(x=x + 1, y=1, string="After what must be one of the most daunghting battles you have ever")
        console.print(x=x + 1, y=2, string="faced, you exit the area.")
        
        console.print(x=x + 1, y=4, string="You wonder through an empty trench filled with the bodies of men")
        console.print(x=x + 1, y=5, string="defeated in battle.")

        console.print(x=x + 1, y=7, string="Some of them are your men, some are the enemies. You can hardly tell.")

        console.print(x=x + 1, y=9, string="What you know is that this trench is now under your control and all")
        console.print(x=x + 1, y=10, string="the sacrifices made to take and defend this lie before you.")

        console.print(x=x + 1, y=12, string="That is your victory.")

        console.print(x=x + 1, y=14, string="Thank you for playing Enemy Trench. Exiting this level will", fg=colour.status_effect_applied)
        console.print(x=x + 1, y=15, string="return you to the main menu.", fg=colour.status_effect_applied)

        console.print(x=x + 1, y=17, string="Press any key to continue.", fg=colour.welcome_text)