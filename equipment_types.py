from enum import auto, Enum

class EquipmentType(Enum):
    WEAPON = auto()
    HEAD_ARMOUR = auto()
    BODY_ARMOUR = auto()
    MASK = auto()
    TORCH = auto()