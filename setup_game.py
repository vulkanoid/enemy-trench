from __future__ import annotations

import copy
import lzma
import pickle
import traceback
from typing import Optional

import tcod
import tcod.sdl.video

import colour
from engine import Engine
import entity_factories
from game_map import GameWorld
import input_handlers
from entity import Actor

import g

import time
import soundfile
import tcod.sdl.audio

import exceptions

import sys
import os
from os import path
from tcod import libtcodpy


if getattr(sys, 'frozen', False):
    game_dir = path.dirname(sys.executable)
else:
    game_dir=path.dirname(__file__)

background_image = tcod.image.load(path.join(game_dir, "media/Images/menu_background.png"))[:, :, :3]

def new_game(playerClass : int, playerName: str, hardcore_mode: bool) -> Engine:
    screen_width = 80
    screen_height = 50

    #viewport_width = 69
    viewport_width = 80
    viewport_height = 43

    map_width = 120
    map_height = 63

    room_max_size = 10
    room_min_size = 6
    max_rooms = 50

    

    if playerClass == 1:
        player = copy.deepcopy(entity_factories.player_infantry)
        LERBayonet = copy.deepcopy(entity_factories.LERBayonet)
        LERBayonet.parent = player.inventory
        player.inventory.items.append(LERBayonet)
        player.equipment.toggle_equip(LERBayonet, add_message=False)
        for i in range(2):
            rifle_ammo_pack = copy.deepcopy(entity_factories.rifle_ammo_pack)
            rifle_ammo_pack.parent = player.inventory
            player.inventory.items.append(rifle_ammo_pack)
        health_pack = copy.deepcopy(entity_factories.health_pack)
        health_pack.parent = player.inventory
        player.inventory.items.append(health_pack)
    if playerClass == 2:
        player = copy.deepcopy(entity_factories.player_medic)
        LER = copy.deepcopy(entity_factories.LER)
        LER.parent = player.inventory
        player.inventory.items.append(LER)
        player.equipment.toggle_equip(LER, add_message=False)
        for i in range(2):
            health_pack = copy.deepcopy(entity_factories.health_pack)
            health_pack.parent = player.inventory
            player.inventory.items.append(health_pack)
        rifle_ammo_pack = copy.deepcopy(entity_factories.rifle_ammo_pack)
        rifle_ammo_pack.parent = player.inventory
        player.inventory.items.append(rifle_ammo_pack)
    if playerClass == 3:
        player = copy.deepcopy(entity_factories.player_sergeant)
        LER = copy.deepcopy(entity_factories.LER)
        LER.parent = player.inventory
        player.inventory.items.append(LER)
        player.equipment.toggle_equip(LER, add_message=False)
        WebRev = copy.deepcopy(entity_factories.WebRev)
        WebRev.parent = player.inventory
        player.inventory.items.append(WebRev)
        health_pack = copy.deepcopy(entity_factories.health_pack)
        health_pack.parent = player.inventory
        player.inventory.items.append(health_pack)
        rifle_ammo_pack = copy.deepcopy(entity_factories.rifle_ammo_pack)
        rifle_ammo_pack.parent = player.inventory
        player.inventory.items.append(rifle_ammo_pack)
        pistol_ammo_pack = copy.deepcopy(entity_factories.pistol_ammo_pack)
        pistol_ammo_pack.parent = player.inventory
        player.inventory.items.append(pistol_ammo_pack)
    if playerClass == 4:
        player = copy.deepcopy(entity_factories.player_bruiser)
        spade = copy.deepcopy(entity_factories.spade)
        spade.parent = player.inventory
        player.inventory.items.append(spade)
        player.equipment.toggle_equip(spade, add_message=False)
        health_pack = copy.deepcopy(entity_factories.health_pack)
        health_pack.parent = player.inventory
        player.inventory.items.append(health_pack)
    if playerClass == 5:
        player = copy.deepcopy(entity_factories.player_officer)
        sword = copy.deepcopy(entity_factories.sword)
        sword.parent = player.inventory
        player.inventory.items.append(sword)
        WebRev = copy.deepcopy(entity_factories.WebRev)
        WebRev.parent = player.inventory
        player.inventory.items.append(WebRev)
        player.equipment.toggle_equip(WebRev, add_message=False)
        pistol_ammo_pack = copy.deepcopy(entity_factories.pistol_ammo_pack)
        pistol_ammo_pack.parent = player.inventory
        player.inventory.items.append(pistol_ammo_pack)

    player.name = playerName
    
    engine = Engine(player=player)

    if hardcore_mode == True:
        engine.is_hardcore_mode = True
    else:
        engine.is_hardcore_mode = False

    engine.game_world = GameWorld(engine=engine, max_rooms=max_rooms, room_min_size=room_min_size, room_max_size=room_max_size, map_width=map_width, map_height=map_height, viewport_width=viewport_width, viewport_height=viewport_height)

    engine.game_world.generate_floor()

    engine.update_fov()

    engine.message_log.add_message("You got into the Enemy Trench!", colour.welcome_text)


    



    gasMask = copy.deepcopy(entity_factories.gasMask)
    gasMask.parent = player.inventory
    player.inventory.items.append(gasMask)


    return engine

def load_game(filename: str) -> Engine:
    with open(path.join(game_dir, "saves/"+filename), "rb") as f:
        engine = pickle.loads(lzma.decompress(f.read()))
    assert isinstance(engine, Engine)
    g.music = tcod.sdl.audio.BasicMixer(tcod.sdl.audio.open())
    music = g.music
    sound, sample_rate = soundfile.read("media/Music/"+engine.music_name)
    sound = music.device.convert(sound, sample_rate)
    channel = music.play(sound=sound, loops=-1,  volume=g.music_volume)
    if hasattr(engine, "is_hardcore_mode") == False:
        setattr(engine, "is_hardcore_mode", False)
        print("no att")
    for entity in engine.game_map.entities:
        if isinstance(entity, Actor):
            setattr(entity, "panicNumber", 0)

    return engine

class MainMenu(input_handlers.BaseEventHandler):
    menu_mode = 0
    selected_item = 0
    character_name = ""
    save_file_names = []
    current_files = []
    adder = 0
    selected_file = 0
    file_list_length = 0
    saveButtonSet = 0
    hard_mode = False
    if path.exists(path.join(game_dir, "settings.ini")):
        try:
            ini_file=open("settings.ini","r")
            file_data = ini_file.read()
            ini_file.close()
            split_data = file_data.split(",")
            m_res = split_data[1].split(":")
            g.music_volume = float(m_res[1])
            a_res = split_data[2].split(":")
            g.mixer_volume = float(a_res[1])
        except Exception as exc:
            traceback.print_exc()
    g.music = tcod.sdl.audio.BasicMixer(tcod.sdl.audio.open())
    music = g.music
    sound, sample_rate = soundfile.read("media/Music/track_6.ogg")
    sound = music.device.convert(sound, sample_rate)
    channel = music.play(sound=sound, loops=-1,  volume=g.music_volume)
    class_numb = 0
    namelength=32
    namecursor = 0
    character_map = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0', ',', '.', '#', '^', ';', ':', '~', '!', '-', '+', '%', '&', '(', ')', '[', ']' ]
    v_kb_open = False
    def on_render(self, console: tcod.Console) -> None:
        if self.menu_mode == 0:
            console.clear()
            console.draw_semigraphics(background_image, 0, 0)

            console.print(0, console.height // 2 - 4, "ENEMY TRENCH", fg=colour.menu_title, alignment=libtcodpy.LEFT)

            console.print(0, console.height - 2, "By Vulkanoid", fg=colour.menu_title, alignment=libtcodpy.LEFT)

            menu_width = 24
            for i, text in enumerate(["[C] Continue with last game", "[N] Play a new game", "[H] Play a new game in hardcore mode", "[O] Options", "[Q] Quit"]):
                if self.selected_item == i + 1:
                    console.print(0, console.height // 2 - 2 + i, text.ljust(menu_width), fg=colour.menu_text, bg=colour.selected_item, alignment=libtcodpy.LEFT, bg_blend=libtcodpy.BKGND_ALPHA(64))
                else:
                    console.print(0, console.height // 2 - 2 + i, text.ljust(menu_width), fg=colour.menu_text, bg=colour.black, alignment=libtcodpy.LEFT, bg_blend=libtcodpy.BKGND_ALPHA(64))
        elif self.menu_mode == 1:
            console.clear()
            console.draw_semigraphics(background_image, 0, 0)
            console.print(console.width // 2, console.height // 2 - 4, "Options", fg=colour.menu_title, alignment=libtcodpy.CENTER)
            music_volume = int(g.music_volume * 10)
            mixer_volume = int(g.mixer_volume * 10)
            graphic_settings = ""
            if g.isAscii == True:
                graphic_settings = "ASCII"
            else:
                graphic_settings = "Tiles"
            menu_width = 24
            txtFScreen = ""
            window = g.context.sdl_window
            if window.fullscreen:
                txtFScreen = "On" 
            else:
                txtFScreen = "Off" 
            for i, text in enumerate([f"[F] Fullscreen: {txtFScreen}", f"[<] Music: {music_volume} [>]", f"[-] Audio: {mixer_volume} [+]", f"[G] Graphics: {graphic_settings}", "[B] Back"]):
                if self.selected_item == i + 1:
                    console.print(console.width // 2, console.height // 2 - 2 + i, text.ljust(menu_width), fg=colour.menu_text, bg=colour.selected_item, alignment=libtcodpy.CENTER, bg_blend=libtcodpy.BKGND_ALPHA(64))
                else:
                    console.print(console.width // 2, console.height // 2 - 2 + i, text.ljust(menu_width), fg=colour.menu_text, bg=colour.black, alignment=libtcodpy.CENTER, bg_blend=libtcodpy.BKGND_ALPHA(64))
        elif self.menu_mode == 2:
            console.clear()
            console.draw_semigraphics(background_image, 0, 0)
            console.print(0, console.height // 2 - 4, "Select your Class: ", fg=colour.menu_title, alignment=libtcodpy.LEFT)

            menu_width = 24
            for i, text in enumerate([f"[1] Infantry", "[2] Medic", "[3] Sergeant", "[4] Bruiser", "[5] Officer", " ", "[B] Back"]):
                if self.selected_item == i + 1:
                    console.print(0, console.height // 2 - 2 + i, text.ljust(menu_width), fg=colour.menu_text, bg=colour.selected_item, alignment=libtcodpy.LEFT, bg_blend=libtcodpy.BKGND_ALPHA(64))
                else:
                    console.print(0, console.height // 2 - 2 + i, text.ljust(menu_width), fg=colour.menu_text, bg=colour.black, alignment=libtcodpy.LEFT, bg_blend=libtcodpy.BKGND_ALPHA(64))

            console.draw_frame(x=5, y=console.height-11, width=console.width-10, height=10, title="Controls: ", clear=True, fg=(255, 255, 255), bg=(0, 0, 0))
            console.print(6, console.height-10, "Keyboard - Move: Arrowkeys, Numpad, vi keys | Shoot: F | Pickup: g |", fg=colour.menu_text, alignment=libtcodpy.LEFT)
            console.print(17, console.height-9, "Exit Level: > | View history: v | Equip: i | Drop: d |", fg=colour.menu_text, alignment=libtcodpy.LEFT)
            console.print(17, console.height-8, "Character: c | Look: / | Cycle ranged: z or v", fg=colour.menu_text, alignment=libtcodpy.LEFT)

            console.print(6, console.height-6, "Mouse - Move select: left click on map | Move: left click on tile", fg=colour.menu_text, alignment=libtcodpy.LEFT)
            console.print(14, console.height-5, "Shoot select: right click on map | Shoot: left click on tile", fg=colour.menu_text, alignment=libtcodpy.LEFT)
            console.print(14, console.height-4, "Pickup or exit: click on player when standing on item", fg=colour.menu_text, alignment=libtcodpy.LEFT)
            console.print(14, console.height-3, "Equip: left click equipment | Drop: Right Click equipment", fg=colour.menu_text, alignment=libtcodpy.LEFT)

            if self.selected_item > 0 and self.selected_item < 6:
                console.draw_frame(x=25, y=5, width=console.width-26, height=30, title="Class:", clear=True, fg=(255, 255, 255), bg=(0, 0, 0))

                if self.selected_item == 1:
                    console.print(26, 7, "Class: Infantry", fg=colour.menu_text, alignment=libtcodpy.LEFT)
                    console.print(26, 9, "Attributes: ", fg=colour.menu_text, alignment=libtcodpy.LEFT)
                    console.print(37, 10, "Strength (physical attacks): 5", fg=colour.menu_text, alignment=libtcodpy.LEFT)
                    console.print(37, 11, "Dexterity (shooting, avoiding danger): 5", fg=colour.menu_text, alignment=libtcodpy.LEFT)
                    console.print(37, 12, "Vitality (health, using health packs): 4", fg=colour.menu_text, alignment=libtcodpy.LEFT)
                    console.print(37, 13, "Composure (handling stressful events): 4", fg=colour.menu_text, alignment=libtcodpy.LEFT)

                    console.print(26, 15, "Equipment: ", fg=colour.menu_text, alignment=libtcodpy.LEFT)
                    console.print(37, 16, "LE-Rifle with Bayonet", fg=colour.menu_text, alignment=libtcodpy.LEFT)
                    console.print(37, 17, "2x Rifle Ammo Packs", fg=colour.menu_text, alignment=libtcodpy.LEFT)
                    console.print(37, 18, "Health Pack", fg=colour.menu_text, alignment=libtcodpy.LEFT)
                
                if self.selected_item == 2:
                    console.print(26, 7, "Class: Medic", fg=colour.menu_text, alignment=libtcodpy.LEFT)
                    console.print(26, 9, "Attributes: ", fg=colour.menu_text, alignment=libtcodpy.LEFT)
                    console.print(37, 10, "Strength (physical attacks): 4", fg=colour.menu_text, alignment=libtcodpy.LEFT)
                    console.print(37, 11, "Dexterity (shooting, avoiding danger): 5", fg=colour.menu_text, alignment=libtcodpy.LEFT)
                    console.print(37, 12, "Vitality (health, using health packs): 5", fg=colour.menu_text, alignment=libtcodpy.LEFT)
                    console.print(37, 13, "Composure (handling stressful events): 4", fg=colour.menu_text, alignment=libtcodpy.LEFT)

                    console.print(26, 15, "Equipment: ", fg=colour.menu_text, alignment=libtcodpy.LEFT)
                    console.print(37, 16, "LE-Rifle", fg=colour.menu_text, alignment=libtcodpy.LEFT)
                    console.print(37, 17, "2x Health Packs", fg=colour.menu_text, alignment=libtcodpy.LEFT)
                    console.print(37, 18, "Rifle ammo Pack", fg=colour.menu_text, alignment=libtcodpy.LEFT)
                
                if self.selected_item == 3:
                    console.print(26, 7, "Class: Sergeant", fg=colour.menu_text, alignment=libtcodpy.LEFT)
                    console.print(26, 9, "Attributes: ", fg=colour.menu_text, alignment=libtcodpy.LEFT)
                    console.print(37, 10, "Strength (physical attacks): 5", fg=colour.menu_text, alignment=libtcodpy.LEFT)
                    console.print(37, 11, "Dexterity (shooting, avoiding danger): 4", fg=colour.menu_text, alignment=libtcodpy.LEFT)
                    console.print(37, 12, "Vitality (health, using health packs): 4", fg=colour.menu_text, alignment=libtcodpy.LEFT)
                    console.print(37, 13, "Composure (handling stressful events): 5", fg=colour.menu_text, alignment=libtcodpy.LEFT)

                    console.print(26, 15, "Equipment: ", fg=colour.menu_text, alignment=libtcodpy.LEFT)
                    console.print(37, 16, "LE-Rifle", fg=colour.menu_text, alignment=libtcodpy.LEFT)
                    console.print(37, 17, "WebRev", fg=colour.menu_text, alignment=libtcodpy.LEFT)
                    console.print(37, 18, "Health Pack", fg=colour.menu_text, alignment=libtcodpy.LEFT)
                    console.print(37, 19, "Rifle Ammo Pack", fg=colour.menu_text, alignment=libtcodpy.LEFT)
                    console.print(37, 20, "Pistol Ammo Pack", fg=colour.menu_text, alignment=libtcodpy.LEFT)
                if self.selected_item == 4:
                    console.print(26, 7, "Class: Bruiser", fg=colour.menu_text, alignment=libtcodpy.LEFT)
                    console.print(26, 9, "Attributes: ", fg=colour.menu_text, alignment=libtcodpy.LEFT)
                    console.print(37, 10, "Strength (physical attacks): 7", fg=colour.menu_text, alignment=libtcodpy.LEFT)
                    console.print(37, 11, "Dexterity (shooting, avoiding danger): 3", fg=colour.menu_text, alignment=libtcodpy.LEFT)
                    console.print(37, 12, "Vitality (health, using health packs): 4", fg=colour.menu_text, alignment=libtcodpy.LEFT)
                    console.print(37, 13, "Composure (handling stressful events): 4", fg=colour.menu_text, alignment=libtcodpy.LEFT)

                    console.print(26, 15, "Equipment: ", fg=colour.menu_text, alignment=libtcodpy.LEFT)
                    console.print(37, 16, "Spade", fg=colour.menu_text, alignment=libtcodpy.LEFT)
                    console.print(37, 17, "Health Pack", fg=colour.menu_text, alignment=libtcodpy.LEFT)
                if self.selected_item == 5:
                    console.print(26, 7, "Class: Officer", fg=colour.menu_text, alignment=libtcodpy.LEFT)
                    console.print(26, 9, "Attributes: ", fg=colour.menu_text, alignment=libtcodpy.LEFT)
                    console.print(37, 10, "Strength (physical attacks): 4", fg=colour.menu_text, alignment=libtcodpy.LEFT)
                    console.print(37, 11, "Dexterity (shooting, avoiding danger): 5", fg=colour.menu_text, alignment=libtcodpy.LEFT)
                    console.print(37, 12, "Vitality (health, using health packs): 4", fg=colour.menu_text, alignment=libtcodpy.LEFT)
                    console.print(37, 13, "Composure (handling stressful events): 5", fg=colour.menu_text, alignment=libtcodpy.LEFT)

                    console.print(26, 15, "Equipment: ", fg=colour.menu_text, alignment=libtcodpy.LEFT)
                    console.print(37, 16, "Sword", fg=colour.menu_text, alignment=libtcodpy.LEFT)
                    console.print(37, 17, "WebRev", fg=colour.menu_text, alignment=libtcodpy.LEFT)
                    console.print(37, 18, "Pistol Ammo Pack", fg=colour.menu_text, alignment=libtcodpy.LEFT)
        elif self.menu_mode == 3:
            console.clear()
            console.draw_semigraphics(background_image, 0, 0)
            console.draw_frame(x=15, y=5, width=console.width-26, height=30, title="Character Name:", clear=True, fg=(255, 255, 255), bg=(0, 0, 0))
            console.print(26, 7, f"Name: {self.character_name}◄", fg=colour.menu_text, alignment=libtcodpy.LEFT)
            console.print(26, 8, f"Length: {len(self.character_name)}/{self.namelength}", fg=colour.menu_text, alignment=libtcodpy.LEFT)
            if self.v_kb_open == False:
                console.draw_frame(x=15, y=15, width=console.width-26, height=20, title="Ctrl: Virtual Keyboard Off", clear=True, fg=(255, 255, 255), bg=(0, 0, 0))
            if self.v_kb_open == True:
                console.draw_frame(x=15, y=15, width=console.width-26, height=20, title="Ctrl: Virtual Keyboard On", clear=True, fg=(255, 255, 255), bg=(0, 0, 0))
                if self.namecursor > 0 and self.namecursor <= 52:
                    console.draw_rect(x=14+self.namecursor, y=16, width=1, height=1, ch=0, fg=(0, 0, 0), bg=(0, 0, 255))
                elif self.namecursor > 52 and self.namecursor <= 104:
                    console.draw_rect(x=14+(self.namecursor-52), y=18, width=1, height=1, ch=0, fg=(0, 0, 0), bg=(0, 0, 255))
                elif self.namecursor > 104 and self.namecursor <= 156:
                    console.draw_rect(x=14+(self.namecursor-104), y=20, width=1, height=1, ch=0, fg=(0, 0, 0), bg=(0, 0, 255))
                elif self.namecursor == 158:
                    console.draw_rect(x=16, y=22, width=4, height=1, ch=0, fg=(0, 0, 0), bg=(0, 0, 255))
                elif self.namecursor == 160:
                    console.draw_rect(x=32, y=22, width=5, height=1, ch=0, fg=(0, 0, 0), bg=(0, 0, 255))
                elif self.namecursor == 162:
                    console.draw_rect(x=48, y=22, width=9, height=1, ch=0, fg=(0, 0, 0), bg=(0, 0, 255))
                elif self.namecursor == 164:
                    console.draw_rect(x=63, y=22, width=5, height=1, ch=0, fg=(0, 0, 0), bg=(0, 0, 255))
                console.print(16, 16, f"A B C D E F G H I J K L M N O P Q R S T U V W X Y Z", fg=colour.menu_text, alignment=libtcodpy.LEFT)
                console.print(16, 18, f"a b c d e f g h i j k l m n o p q r s t u v w x y z", fg=colour.menu_text, alignment=libtcodpy.LEFT)
                console.print(16, 20, f"1 2 3 4 5 6 7 8 9 0 , . # ^ ; : ~ ! - + % & ( ) [ ]", fg=colour.menu_text, alignment=libtcodpy.LEFT)
                console.print(16, 22, f"BACK", fg=colour.menu_text, alignment=libtcodpy.LEFT)
                console.print(32, 22, f"SPACE", fg=colour.menu_text, alignment=libtcodpy.LEFT)
                console.print(48, 22, f"BACKSPACE", fg=colour.menu_text, alignment=libtcodpy.LEFT)
                console.print(63, 22, f"ENTER", fg=colour.menu_text, alignment=libtcodpy.LEFT)
        elif self.menu_mode == 4:
            console.clear()
            console.draw_semigraphics(background_image, 0, 0)
            console.draw_frame(x=15, y=5, width=console.width-26, height=30, title="Select a save:", clear=True, fg=(255, 255, 255), bg=(0, 0, 0))
            for i, text in enumerate(self.save_file_names):
                if len(self.save_file_names) > 10:
                    lowerEnd = 0 + self.adder
                    upperEnd = 9 + self.adder
                    self.current_files = self.save_file_names[lowerEnd:upperEnd]
                else:
                    self.current_files = self.save_file_names
                self.file_list_length = len(self.current_files)
                for i, text in enumerate(self.current_files):
                    console.print(16, i+7, f"{text}", fg=colour.menu_text, alignment=libtcodpy.LEFT)
            if self.selected_file > 0:
                console.print(60, 6+self.selected_file, f"<-", fg=colour.menu_text, alignment=libtcodpy.LEFT)
            if self.saveButtonSet == 1:
                console.draw_rect(x=16, y=20, width=1, height=1, ch=0, fg=(0, 0, 0), bg=(0, 0, 255))
            if self.saveButtonSet == 2:
                console.draw_rect(x=67, y=20, width=1, height=1, ch=0, fg=(0, 0, 0), bg=(0, 0, 255))
            if self.saveButtonSet == 3:
                console.draw_rect(x=console.width // 2, y=20, width=4, height=1, ch=0, fg=(0, 0, 0), bg=(0, 0, 255))
            console.print(16, 20, f"◄", fg=colour.menu_text, alignment=libtcodpy.LEFT)
            console.print(67, 20, f"►", fg=colour.menu_text, alignment=libtcodpy.LEFT)
            console.print(40, 20, "Back", fg=colour.menu_text, alignment=libtcodpy.LEFT)


    
    def ev_keydown(self, event: tcod.event.KeyDown) ->Optional[input_handlers.BaseEventHandler]:
        if self.menu_mode == 0:
            if event.sym in (tcod.event.KeySym.q, tcod.event.KeySym.ESCAPE):
                raise exceptions.QuitWithoutSaving()
            elif event.sym == tcod.event.KeySym.c:
                self.save_file_names = []
                mixer = g.mixer
                sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
                sound = mixer.device.convert(sound, sample_rate)
                channel = mixer.play(sound=sound, volume=g.mixer_volume)
                window = g.context.sdl_window
                if os.path.exists(path.join(game_dir, "saves")):
                    for file in os.listdir(game_dir+"/saves"):
                        if file.endswith(".sav"):
                            self.save_file_names.append(file)
                    if len(self.save_file_names) <= 0:
                        return input_handlers.PopupMessage(self, "No saved game found.")
                    else:
                        self.menu_mode = 4
                else:
                    return input_handlers.PopupMessage(self, "No saved game found.")
            elif event.sym == tcod.event.KeySym.n:
                mixer = g.mixer
                sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
                sound = mixer.device.convert(sound, sample_rate)
                channel = mixer.play(sound=sound, volume=g.mixer_volume)
                window = g.context.sdl_window
                self.selected_item = 0
                self.menu_mode = 2
                screen_width = 80
                screen_height = 50
                self.on_render(tcod.console.Console(screen_width, screen_height, order="F"))
                self.hard_mode = False
            elif event.sym == tcod.event.KeySym.h:
                mixer = g.mixer
                sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
                sound = mixer.device.convert(sound, sample_rate)
                channel = mixer.play(sound=sound, volume=g.mixer_volume)
                window = g.context.sdl_window
                self.selected_item = 0
                self.menu_mode = 2
                screen_width = 80
                screen_height = 50
                self.on_render(tcod.console.Console(screen_width, screen_height, order="F"))
                self.hard_mode = True
            elif event.sym == tcod.event.KeySym.o:
                mixer = g.mixer
                sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
                sound = mixer.device.convert(sound, sample_rate)
                channel = mixer.play(sound=sound, volume=g.mixer_volume)
                window = g.context.sdl_window
                self.selected_item = 0
                self.menu_mode = 1
                screen_width = 80
                screen_height = 50
                self.on_render(tcod.console.Console(screen_width, screen_height, order="F"))
            elif event.sym == tcod.event.KeySym.UP:
                mixer = g.mixer
                sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
                sound = mixer.device.convert(sound, sample_rate)
                channel = mixer.play(sound=sound, volume=g.mixer_volume)
                window = g.context.sdl_window
                if self.selected_item <= 0:
                    self.selected_item = 5
                else:
                    self.selected_item = self.selected_item - 1
            elif event.sym == tcod.event.KeySym.DOWN:
                mixer = g.mixer
                sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
                sound = mixer.device.convert(sound, sample_rate)
                channel = mixer.play(sound=sound, volume=g.mixer_volume)
                window = g.context.sdl_window
                if self.selected_item >= 5:
                    self.selected_item = 0
                else:
                    self.selected_item = self.selected_item + 1
            elif event.sym == tcod.event.KeySym.RETURN:
                self.save_file_names = []
                mixer = g.mixer
                sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
                sound = mixer.device.convert(sound, sample_rate)
                channel = mixer.play(sound=sound, volume=g.mixer_volume)
                window = g.context.sdl_window
                if self.selected_item == 1:
                    if os.path.exists(path.join(game_dir, "saves")):
                        for file in os.listdir(game_dir+"/saves"):
                            if file.endswith(".sav"):
                                self.save_file_names.append(file)
                        if len(self.save_file_names) <= 0:
                            return input_handlers.PopupMessage(self, "No saved game found.")
                        else:
                            self.menu_mode = 4
                    else:
                        return input_handlers.PopupMessage(self, "No saved game found.")
                if self.selected_item == 2:
                    self.selected_item = 0
                    self.menu_mode = 2
                    screen_width = 80
                    screen_height = 50
                    self.on_render(tcod.console.Console(screen_width, screen_height, order="F"))
                    self.hard_mode = False
                if self.selected_item == 3:
                    self.selected_item = 0
                    self.menu_mode = 2
                    screen_width = 80
                    screen_height = 50
                    self.on_render(tcod.console.Console(screen_width, screen_height, order="F"))
                    self.hard_mode = True
                if self.selected_item == 4:
                    self.selected_item = 0
                    self.menu_mode = 1
                    screen_width = 80
                    screen_height = 50
                    self.on_render(tcod.console.Console(screen_width, screen_height, order="F"))
                if self.selected_item == 5:
                    raise exceptions.QuitWithoutSaving()
            return None
        elif self.menu_mode == 1:
            if event.sym == tcod.event.KeySym.f:
                mixer = g.mixer
                sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
                sound = mixer.device.convert(sound, sample_rate)
                channel = mixer.play(sound=sound, volume=g.mixer_volume)
                window = g.context.sdl_window
                window = g.context.sdl_window
                if not window:
                    return
                if window.fullscreen:
                    window.fullscreen = False
                else:
                    window.fullscreen = tcod.sdl.video.WindowFlags.FULLSCREEN_DESKTOP
            elif event.sym == tcod.event.KeySym.PERIOD and event.mod & (tcod.event.KMOD_LSHIFT | tcod.event.KMOD_RSHIFT):
                mixer = g.mixer
                sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
                sound = mixer.device.convert(sound, sample_rate)
                channel = mixer.play(sound=sound, volume=g.mixer_volume)
                if g.music_volume < 1.0:
                    g.music_volume = float(int(g.music_volume * 10) + 1) / 10
                self.channel.volume = g.music_volume
            elif event.sym == tcod.event.KeySym.COMMA and event.mod & (tcod.event.KMOD_LSHIFT | tcod.event.KMOD_RSHIFT):
                mixer = g.mixer
                sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
                sound = mixer.device.convert(sound, sample_rate)
                channel = mixer.play(sound=sound, volume=g.mixer_volume)
                if g.music_volume > 0.0:
                    g.music_volume = float(int(g.music_volume * 10) - 1) / 10
                self.channel.volume = g.music_volume
            elif event.sym == tcod.event.KeySym.EQUALS and event.mod & (tcod.event.KMOD_LSHIFT | tcod.event.KMOD_RSHIFT):
                mixer = g.mixer
                sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
                sound = mixer.device.convert(sound, sample_rate)
                channel = mixer.play(sound=sound, volume=g.mixer_volume)
                if g.mixer_volume < 1.0:
                    g.mixer_volume = float(int(g.mixer_volume * 10) + 1) / 10
            elif event.sym == tcod.event.KeySym.MINUS:
                mixer = g.mixer
                sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
                sound = mixer.device.convert(sound, sample_rate)
                channel = mixer.play(sound=sound, volume=g.mixer_volume)
                if g.mixer_volume > 0.0:
                    g.mixer_volume = float(int(g.mixer_volume * 10) - 1) / 10
            elif event.sym == tcod.event.KeySym.g:
                mixer = g.mixer
                sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
                sound = mixer.device.convert(sound, sample_rate)
                channel = mixer.play(sound=sound, volume=g.mixer_volume)
                if g.isAscii == True:
                    g.isAscii = False
                else:
                    g.isAscii = True
            elif event.sym == tcod.event.KeySym.b:
                mixer = g.mixer
                sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
                sound = mixer.device.convert(sound, sample_rate)
                channel = mixer.play(sound=sound, volume=g.mixer_volume)
                window = g.context.sdl_window
                save_string = f"F:{window.fullscreen},M:{g.music_volume},A:{g.mixer_volume},G:{g.isAscii}"
                ini_file = open("settings.ini", "w")
                n = ini_file.write(save_string)
                ini_file.close()
                screen_width = 80
                screen_height = 50
                self.menu_mode = 0
                self.selected_item = 0
                self.on_render(tcod.console.Console(screen_width, screen_height, order="F"))
            elif event.sym == tcod.event.KeySym.UP:
                mixer = g.mixer
                sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
                sound = mixer.device.convert(sound, sample_rate)
                channel = mixer.play(sound=sound, volume=g.mixer_volume)
                window = g.context.sdl_window
                if self.selected_item <= 0:
                    self.selected_item = 5
                else:
                    self.selected_item = self.selected_item - 1
            elif event.sym == tcod.event.KeySym.DOWN:
                mixer = g.mixer
                sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
                sound = mixer.device.convert(sound, sample_rate)
                channel = mixer.play(sound=sound, volume=g.mixer_volume)
                window = g.context.sdl_window
                if self.selected_item >= 5:
                    self.selected_item = 0
                else:
                    self.selected_item = self.selected_item + 1
            elif event.sym == tcod.event.KeySym.RETURN:
                mixer = g.mixer
                sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
                sound = mixer.device.convert(sound, sample_rate)
                channel = mixer.play(sound=sound, volume=g.mixer_volume)
                window = g.context.sdl_window
                if self.selected_item == 1:
                    window = g.context.sdl_window
                    if not window:
                        return
                    if window.fullscreen:
                        window.fullscreen = False
                    else:
                        window.fullscreen = tcod.sdl.video.WindowFlags.FULLSCREEN_DESKTOP
                if self.selected_item == 4:
                    if g.isAscii == True:
                        g.isAscii = False
                    else:
                        g.isAscii = True
                if self.selected_item == 5:
                    screen_width = 80
                    screen_height = 50
                    self.menu_mode = 0
                    self.selected_item = 0
                    self.on_render(tcod.console.Console(screen_width, screen_height, order="F"))
            elif event.sym == tcod.event.KeySym.LEFT:
                if self.selected_item == 2:
                    mixer = g.mixer
                    sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
                    sound = mixer.device.convert(sound, sample_rate)
                    channel = mixer.play(sound=sound, volume=g.mixer_volume)
                    if g.music_volume > 0.0:
                        g.music_volume = float(int(g.music_volume * 10) - 1) / 10
                    self.channel.volume = g.music_volume
                if self.selected_item == 3:
                    mixer = g.mixer
                    sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
                    sound = mixer.device.convert(sound, sample_rate)
                    channel = mixer.play(sound=sound, volume=g.mixer_volume)
                    if g.mixer_volume > 0.0:
                        g.mixer_volume = float(int(g.mixer_volume * 10) - 1) / 10
                    self.channel.volume = g.music_volume
            elif event.sym == tcod.event.KeySym.RIGHT:
                if self.selected_item == 2:
                    mixer = g.mixer
                    sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
                    sound = mixer.device.convert(sound, sample_rate)
                    channel = mixer.play(sound=sound, volume=g.mixer_volume)
                    if g.music_volume < 1.0:
                        g.music_volume = float(int(g.music_volume * 10) + 1) / 10
                    self.channel.volume = g.music_volume
                if self.selected_item == 3:
                    mixer = g.mixer
                    sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
                    sound = mixer.device.convert(sound, sample_rate)
                    channel = mixer.play(sound=sound, volume=g.mixer_volume)
                    if g.mixer_volume < 1.0:
                        g.mixer_volume = float(int(g.mixer_volume * 10) + 1) / 10
                    self.channel.volume = g.music_volume
        elif self.menu_mode == 2:
            if event.sym == tcod.event.KeySym.N1:
                mixer = g.mixer
                sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
                sound = mixer.device.convert(sound, sample_rate)
                channel = mixer.play(sound=sound, volume=g.mixer_volume)
                window = g.context.sdl_window
                self.menu_mode = 3
                self.character_name = entity_factories.player_infantry.name
                self.class_numb = 1
            elif event.sym == tcod.event.KeySym.N2:
                mixer = g.mixer
                sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
                sound = mixer.device.convert(sound, sample_rate)
                channel = mixer.play(sound=sound, volume=g.mixer_volume)
                window = g.context.sdl_window
                self.menu_mode = 3
                self.character_name = entity_factories.player_medic.name
                self.class_numb = 2
            elif event.sym == tcod.event.KeySym.N3:
                mixer = g.mixer
                sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
                sound = mixer.device.convert(sound, sample_rate)
                channel = mixer.play(sound=sound, volume=g.mixer_volume)
                window = g.context.sdl_window
                self.menu_mode = 3
                self.character_name = entity_factories.player_sergeant.name
                self.class_numb = 3
            elif event.sym == tcod.event.KeySym.N4:
                mixer = g.mixer
                sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
                sound = mixer.device.convert(sound, sample_rate)
                channel = mixer.play(sound=sound, volume=g.mixer_volume)
                window = g.context.sdl_window
                self.menu_mode = 3
                self.character_name = entity_factories.player_bruiser.name
                self.class_numb = 4
            elif event.sym == tcod.event.KeySym.N5:
                mixer = g.mixer
                sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
                sound = mixer.device.convert(sound, sample_rate)
                channel = mixer.play(sound=sound, volume=g.mixer_volume)
                window = g.context.sdl_window
                self.menu_mode = 3
                self.character_name = entity_factories.player_officer.name
                self.class_numb = 5
            elif event.sym == tcod.event.KeySym.b:
                mixer = g.mixer
                sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
                sound = mixer.device.convert(sound, sample_rate)
                channel = mixer.play(sound=sound, volume=g.mixer_volume)
                window = g.context.sdl_window
                screen_width = 80
                screen_height = 50
                self.menu_mode = 0
                self.selected_item = 0
                self.on_render(tcod.console.Console(screen_width, screen_height, order="F"))
            elif event.sym == tcod.event.KeySym.UP:
                mixer = g.mixer
                sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
                sound = mixer.device.convert(sound, sample_rate)
                channel = mixer.play(sound=sound, volume=g.mixer_volume)
                window = g.context.sdl_window
                if self.selected_item <= 0:
                    self.selected_item = 7
                else:
                    self.selected_item = self.selected_item - 1
                    if self.selected_item == 6:
                        self.selected_item = 5
            elif event.sym == tcod.event.KeySym.DOWN:
                mixer = g.mixer
                sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
                sound = mixer.device.convert(sound, sample_rate)
                channel = mixer.play(sound=sound, volume=g.mixer_volume)
                window = g.context.sdl_window
                if self.selected_item >= 6:
                    self.selected_item = 0
                else:
                    self.selected_item = self.selected_item + 1
                    if self.selected_item == 6:
                        self.selected_item = 7
            elif event.sym == tcod.event.KeySym.RETURN:
                mixer = g.mixer
                sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
                sound = mixer.device.convert(sound, sample_rate)
                channel = mixer.play(sound=sound, volume=g.mixer_volume)
                window = g.context.sdl_window
                if self.selected_item == 1:
                    self.menu_mode = 3
                    self.character_name = entity_factories.player_infantry.name
                    self.class_numb = 1
                if self.selected_item == 2:
                    self.menu_mode = 3
                    self.character_name = entity_factories.player_medic.name
                    self.class_numb = 2
                if self.selected_item == 3:
                    self.menu_mode = 3
                    self.character_name = entity_factories.player_sergeant.name
                    self.class_numb = 3
                if self.selected_item == 4:
                    self.menu_mode = 3
                    self.character_name = entity_factories.player_bruiser.name
                    self.class_numb = 4
                if self.selected_item == 5:
                    self.menu_mode = 3
                    self.character_name = entity_factories.player_officer.name
                    self.class_numb = 5
                if self.selected_item == 7:
                    screen_width = 80
                    screen_height = 50
                    self.menu_mode = 0
                    self.selected_item = 0
                    self.on_render(tcod.console.Console(screen_width, screen_height, order="F"))
        elif self.menu_mode == 3:
            if event.sym == tcod.event.KeySym.LCTRL or event.sym == tcod.event.KeySym.RCTRL:
                mixer = g.mixer
                sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
                sound = mixer.device.convert(sound, sample_rate)
                channel = mixer.play(sound=sound, volume=g.mixer_volume)
                if self.v_kb_open == False:
                    self.v_kb_open = True
                else:
                    self.v_kb_open = False
            elif event.sym == tcod.event.KeySym.RETURN:
                mixer = g.mixer
                sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
                sound = mixer.device.convert(sound, sample_rate)
                channel = mixer.play(sound=sound, volume=g.mixer_volume)
                if self.v_kb_open == False:
                    self.music.close()
                    g.music.close()
                    return input_handlers.MainGameEventHandler(new_game(self.class_numb, self.character_name, self.hard_mode))
                else:
                    if self.namecursor > 0 and self.namecursor <= 156:
                        if len(self.character_name) < self.namelength:
                            self.character_name = self.character_name + self.character_map[((int(self.namecursor/2))-1)]
                    elif self.namecursor == 158:
                        self.menu_mode = 2
                    elif self.namecursor == 160:
                        if len(self.character_name) < self.namelength:
                            self.character_name = self.character_name + " "
                    elif self.namecursor == 162:
                        if len(self.character_name) > 0:
                            self.character_name = self.character_name[:-1]
                    elif self.namecursor == 164:
                        self.music.close()
                        g.music.close()
                        return input_handlers.MainGameEventHandler(new_game(self.class_numb, self.character_name, self.hard_mode))
                        
            elif event.sym == tcod.event.KeySym.BACKSPACE:
                if self.v_kb_open == False:
                    mixer = g.mixer
                    sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
                    sound = mixer.device.convert(sound, sample_rate)
                    channel = mixer.play(sound=sound, volume=g.mixer_volume)
                    if len(self.character_name) > 0:
                        self.character_name = self.character_name[:-1]
            elif event.sym == tcod.event.KeySym.ESCAPE:
                if self.v_kb_open == False:
                    mixer = g.mixer
                    sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
                    sound = mixer.device.convert(sound, sample_rate)
                    channel = mixer.play(sound=sound, volume=g.mixer_volume)
                    self.menu_mode = 2
            elif event.sym == tcod.event.KeySym.LEFT:
                if self.v_kb_open == True:
                    mixer = g.mixer
                    sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
                    sound = mixer.device.convert(sound, sample_rate)
                    channel = mixer.play(sound=sound, volume=g.mixer_volume)
                    window = g.context.sdl_window
                    if self.namecursor <= 0:
                        self.namecursor = 164
                    else:
                        self.namecursor = self.namecursor - 2
            elif event.sym == tcod.event.KeySym.RIGHT:
                if self.v_kb_open == True:
                    mixer = g.mixer
                    sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
                    sound = mixer.device.convert(sound, sample_rate)
                    channel = mixer.play(sound=sound, volume=g.mixer_volume)
                    window = g.context.sdl_window
                    if self.namecursor >= 164:
                        self.namecursor = 0
                    else:
                        self.namecursor = self.namecursor + 2
        elif self.menu_mode == 4:
            if event.sym == tcod.event.KeySym.LEFT:
                mixer = g.mixer
                sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
                sound = mixer.device.convert(sound, sample_rate)
                channel = mixer.play(sound=sound, volume=g.mixer_volume)
                window = g.context.sdl_window
                if self.adder <= 0:
                    self.adder = 0
                else:
                    self.adder = self.adder - 10
                self.selected_file = 0
            elif event.sym == tcod.event.KeySym.RIGHT:
                mixer = g.mixer
                sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
                sound = mixer.device.convert(sound, sample_rate)
                channel = mixer.play(sound=sound, volume=g.mixer_volume)
                window = g.context.sdl_window
                if self.adder >= len(self.save_file_names) - 10:
                    self.adder = self.adder
                else:
                    self.adder = self.adder + 10
                self.selected_file = 0
            elif event.sym == tcod.event.KeySym.UP:
                mixer = g.mixer
                sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
                sound = mixer.device.convert(sound, sample_rate)
                channel = mixer.play(sound=sound, volume=g.mixer_volume)
                window = g.context.sdl_window
                if self.selected_file <= 0:
                    self.selected_file = self.file_list_length
                else:
                    self.selected_file = self.selected_file - 1
            elif event.sym == tcod.event.KeySym.DOWN:
                mixer = g.mixer
                sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
                sound = mixer.device.convert(sound, sample_rate)
                channel = mixer.play(sound=sound, volume=g.mixer_volume)
                window = g.context.sdl_window
                if self.selected_file >= self.file_list_length:
                    self.selected_file = 0
                else:
                    self.selected_file = self.selected_file + 1
            elif event.sym == tcod.event.KeySym.RETURN:
                mixer = g.mixer
                sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
                sound = mixer.device.convert(sound, sample_rate)
                channel = mixer.play(sound=sound, volume=g.mixer_volume)
                window = g.context.sdl_window
                try:
                    self.music.close()
                    g.music.close()
                    return input_handlers.MainGameEventHandler(load_game(self.current_files[self.selected_file-1]))
                except FileNotFoundError:
                    return input_handlers.PopupMessage(self, "File could not be found.")
                except Exception as exc:
                    traceback.print_exc()
                    return input_handlers.PopupMessage(self, f"Failed to load save:\n{exc}")
            elif event.sym == tcod.event.KeySym.ESCAPE:
                mixer = g.mixer
                sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
                sound = mixer.device.convert(sound, sample_rate)
                channel = mixer.play(sound=sound, volume=g.mixer_volume)
                window = g.context.sdl_window
                self.selected_file = 0
                self.saveButtonSet = 0
                self.menu_mode = 0
                

    def ev_textinput(self, event: TextInput) -> Optional[input_handlers.BaseEventHandler]:
        if self.menu_mode == 3:
            if self.v_kb_open == False:
                mixer = g.mixer
                sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
                sound = mixer.device.convert(sound, sample_rate)
                channel = mixer.play(sound=sound, volume=g.mixer_volume)
                if len(self.character_name) < self.namelength:
                    self.character_name = self.character_name + event.text


    
    def ev_mousebuttondown(self, event: tcod.event.MouseButtonDown) -> Optional[input_handlers.BaseEventHandler]:      
        if self.menu_mode == 0:
            if event.tile.y == 23 and event.tile.x <= 26:
                self.save_file_names = []
                mixer = g.mixer
                sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
                sound = mixer.device.convert(sound, sample_rate)
                channel = mixer.play(sound=sound, volume=g.mixer_volume)
                window = g.context.sdl_window
                if os.path.exists(path.join(game_dir, "saves")):
                    for file in os.listdir(game_dir+"/saves"):
                        if file.endswith(".sav"):
                            self.save_file_names.append(file)
                    if len(self.save_file_names) <= 0:
                        return input_handlers.PopupMessage(self, "No saved game found.")
                    else:
                        self.menu_mode = 4
                else:
                    return input_handlers.PopupMessage(self, "No saved game found.")
            elif event.tile.y == 24 and event.tile.x <= 26:
                mixer = g.mixer
                sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
                sound = mixer.device.convert(sound, sample_rate)
                channel = mixer.play(sound=sound, volume=g.mixer_volume)
                window = g.context.sdl_window
                self.selected_item = 0
                self.menu_mode = 2
                screen_width = 80
                screen_height = 50
                self.on_render(tcod.console.Console(screen_width, screen_height, order="F"))
                self.hard_mode = False
            elif event.tile.y == 25 and event.tile.x <= 26:
                mixer = g.mixer
                sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
                sound = mixer.device.convert(sound, sample_rate)
                channel = mixer.play(sound=sound, volume=g.mixer_volume)
                window = g.context.sdl_window
                self.selected_item = 0
                self.menu_mode = 2
                screen_width = 80
                screen_height = 50
                self.on_render(tcod.console.Console(screen_width, screen_height, order="F"))
                self.hard_mode = True
            elif event.tile.y == 26 and event.tile.x <= 26:
                mixer = g.mixer
                sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
                sound = mixer.device.convert(sound, sample_rate)
                channel = mixer.play(sound=sound, volume=g.mixer_volume)
                window = g.context.sdl_window
                self.selected_item = 0
                self.menu_mode = 1
                screen_width = 80
                screen_height = 50
                self.on_render(tcod.console.Console(screen_width, screen_height, order="F"))
            elif event.tile.y == 27 and event.tile.x <= 26:
                mixer = g.mixer
                sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
                sound = mixer.device.convert(sound, sample_rate)
                channel = mixer.play(sound=sound, volume=g.mixer_volume)
                window = g.context.sdl_window
                raise exceptions.QuitWithoutSaving()
            return None
        elif self.menu_mode == 1:
            if event.tile.y == 23 and event.tile.x >= 28 and event.tile.x <= 51:
                mixer = g.mixer
                sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
                sound = mixer.device.convert(sound, sample_rate)
                channel = mixer.play(sound=sound, volume=g.mixer_volume)
                window = g.context.sdl_window
                if not window:
                    return
                if window.fullscreen:
                    window.fullscreen = False
                else:
                    window.fullscreen = tcod.sdl.video.WindowFlags.FULLSCREEN_DESKTOP
            elif event.tile.y == 24 and event.tile.x >= 28 and event.tile.x <= 39:
                mixer = g.mixer
                sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
                sound = mixer.device.convert(sound, sample_rate)
                channel = mixer.play(sound=sound, volume=g.mixer_volume)
                if g.music_volume > 0.0:
                    g.music_volume = float(int(g.music_volume * 10) - 1) / 10
                self.channel.volume = g.music_volume
            elif event.tile.y == 24 and event.tile.x >= 40 and event.tile.x <= 51:
                mixer = g.mixer
                sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
                sound = mixer.device.convert(sound, sample_rate)
                channel = mixer.play(sound=sound, volume=g.mixer_volume)
                if g.music_volume < 1.0:
                    g.music_volume = float(int(g.music_volume * 10) + 1) / 10
                self.channel.volume = g.music_volume
            elif event.tile.y == 25 and event.tile.x >= 28 and event.tile.x <= 39:
                mixer = g.mixer
                sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
                sound = mixer.device.convert(sound, sample_rate)
                channel = mixer.play(sound=sound, volume=g.mixer_volume)
                if g.mixer_volume > 0.0:
                    g.mixer_volume = float(int(g.mixer_volume * 10) - 1) / 10
            elif event.tile.y == 25 and event.tile.x >= 40 and event.tile.x <= 51:
                mixer = g.mixer
                sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
                sound = mixer.device.convert(sound, sample_rate)
                channel = mixer.play(sound=sound, volume=g.mixer_volume)
                if g.mixer_volume < 1.0:
                    g.mixer_volume = float(int(g.mixer_volume * 10) + 1) / 10
            elif event.tile.y == 26 and event.tile.x >= 28 and event.tile.x <= 51:
                mixer = g.mixer
                sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
                sound = mixer.device.convert(sound, sample_rate)
                channel = mixer.play(sound=sound, volume=g.mixer_volume)
                if g.isAscii == True:
                    g.isAscii = False
                else:
                    g.isAscii = True
            elif event.tile.y == 27 and event.tile.x >= 28 and event.tile.x <= 51:
                mixer = g.mixer
                sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
                sound = mixer.device.convert(sound, sample_rate)
                channel = mixer.play(sound=sound, volume=g.mixer_volume)
                window = g.context.sdl_window
                screen_width = 80
                screen_height = 50
                self.selected_item = 0
                self.menu_mode = 0
                self.on_render(tcod.console.Console(screen_width, screen_height, order="F"))
        elif self.menu_mode == 2:
            if event.tile.y == 23 and event.tile.x <= 23:
                mixer = g.mixer
                sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
                sound = mixer.device.convert(sound, sample_rate)
                channel = mixer.play(sound=sound, volume=g.mixer_volume)
                window = g.context.sdl_window
                self.menu_mode = 3
                self.character_name = entity_factories.player_infantry.name
                self.class_numb = 1
            elif event.tile.y == 24 and event.tile.x <= 23:
                mixer = g.mixer
                sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
                sound = mixer.device.convert(sound, sample_rate)
                channel = mixer.play(sound=sound, volume=g.mixer_volume)
                window = g.context.sdl_window
                self.menu_mode = 3
                self.character_name = entity_factories.player_medic.name
                self.class_numb = 2
            elif event.tile.y == 25 and event.tile.x <= 23:
                mixer = g.mixer
                sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
                sound = mixer.device.convert(sound, sample_rate)
                channel = mixer.play(sound=sound, volume=g.mixer_volume)
                window = g.context.sdl_window
                self.menu_mode = 3
                self.character_name = entity_factories.player_sergeant.name
                self.class_numb = 3
            elif event.tile.y == 26 and event.tile.x <= 23:
                mixer = g.mixer
                sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
                sound = mixer.device.convert(sound, sample_rate)
                channel = mixer.play(sound=sound, volume=g.mixer_volume)
                window = g.context.sdl_window
                self.menu_mode = 3
                self.character_name = entity_factories.player_bruiser.name
                self.class_numb = 4
            elif event.tile.y == 27 and event.tile.x <= 23:
                mixer = g.mixer
                sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
                sound = mixer.device.convert(sound, sample_rate)
                channel = mixer.play(sound=sound, volume=g.mixer_volume)
                window = g.context.sdl_window
                self.menu_mode = 3
                self.character_name = entity_factories.player_officer.name
                self.class_numb = 5
            elif event.tile.y == 29 and event.tile.x <= 23:
                mixer = g.mixer
                sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
                sound = mixer.device.convert(sound, sample_rate)
                channel = mixer.play(sound=sound, volume=g.mixer_volume)
                window = g.context.sdl_window
                screen_width = 80
                screen_height = 50
                self.selected_item = 0
                self.menu_mode = 0
                self.on_render(tcod.console.Console(screen_width, screen_height, order="F"))
        elif self.menu_mode == 3:
            print(f"x: {event.tile.x} - y: {event.tile.y}")
            if event.tile.y == 15 and event.tile.x >= 28 and event.tile.x <= 55:
                mixer = g.mixer
                sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
                sound = mixer.device.convert(sound, sample_rate)
                channel = mixer.play(sound=sound, volume=g.mixer_volume)
                if self.v_kb_open == False:
                    self.v_kb_open = True
                else:
                    self.v_kb_open = False
            if self.v_kb_open == True:
                if self.namecursor > 0 and self.namecursor <= 156:
                    mixer = g.mixer
                    sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
                    sound = mixer.device.convert(sound, sample_rate)
                    channel = mixer.play(sound=sound, volume=g.mixer_volume)
                    if len(self.character_name) < self.namelength:
                        self.character_name = self.character_name + self.character_map[((int(self.namecursor/2))-1)]          
                elif self.namecursor == 158:
                    mixer = g.mixer
                    sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
                    sound = mixer.device.convert(sound, sample_rate)
                    channel = mixer.play(sound=sound, volume=g.mixer_volume)
                    self.menu_mode = 2
                elif self.namecursor == 160:
                    mixer = g.mixer
                    sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
                    sound = mixer.device.convert(sound, sample_rate)
                    channel = mixer.play(sound=sound, volume=g.mixer_volume)
                    if len(self.character_name) < self.namelength:
                        self.character_name = self.character_name + " "
                elif self.namecursor == 162:
                    mixer = g.mixer
                    sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
                    sound = mixer.device.convert(sound, sample_rate)
                    channel = mixer.play(sound=sound, volume=g.mixer_volume)
                    if len(self.character_name) > 0:
                        self.character_name = self.character_name[:-1]
                elif self.namecursor == 164:
                    mixer = g.mixer
                    sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
                    sound = mixer.device.convert(sound, sample_rate)
                    channel = mixer.play(sound=sound, volume=g.mixer_volume)
                    self.music.close()
                    g.music.close()
                    return input_handlers.MainGameEventHandler(new_game(self.class_numb, self.character_name, self.hard_mode))
        elif self.menu_mode == 4:
            if event.tile.y >= 6 and event.tile.y <= 6+len(self.current_files):
                mixer = g.mixer
                sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
                sound = mixer.device.convert(sound, sample_rate)
                channel = mixer.play(sound=sound, volume=g.mixer_volume)
                window = g.context.sdl_window
                try:
                    self.music.close()
                    g.music.close()
                    return input_handlers.MainGameEventHandler(load_game(self.current_files[self.selected_file-1]))
                except FileNotFoundError:
                    return input_handlers.PopupMessage(self, "File could not be found.")
                except Exception as exc:
                    traceback.print_exc()
                    return input_handlers.PopupMessage(self, f"Failed to load save:\n{exc}")
            if event.tile.x == 16 and event.tile.y == 20:
                mixer = g.mixer
                sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
                sound = mixer.device.convert(sound, sample_rate)
                channel = mixer.play(sound=sound, volume=g.mixer_volume)
                window = g.context.sdl_window
                if self.adder <= 0:
                    self.adder = 0
                else:
                    self.adder = self.adder - 10
                self.selected_file = 0
            elif event.tile.x == 67 and event.tile.y == 20:
                mixer = g.mixer
                sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
                sound = mixer.device.convert(sound, sample_rate)
                channel = mixer.play(sound=sound, volume=g.mixer_volume)
                window = g.context.sdl_window
                if self.adder >= len(self.save_file_names) - 10:
                    self.adder = self.adder
                else:
                    self.adder = self.adder + 10
                self.selected_file = 0
            elif event.tile.x >= 40 and event.tile.x < 44 and event.tile.y == 20:
                mixer = g.mixer
                sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
                sound = mixer.device.convert(sound, sample_rate)
                channel = mixer.play(sound=sound, volume=g.mixer_volume)
                window = g.context.sdl_window
                self.selected_file = 0
                self.saveButtonSet = 0
                self.menu_mode = 0
                

    def ev_mousemotion(self, event: tcod.event.MouseMotion) -> None:
        if self.menu_mode == 0:
            if event.tile.y == 23 and event.tile.x <= 26:
                if self.selected_item != 1:
                    mixer = g.mixer
                    sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
                    sound = mixer.device.convert(sound, sample_rate)
                    channel = mixer.play(sound=sound, volume=g.mixer_volume)
                self.selected_item = 1
            elif event.tile.y == 24 and event.tile.x <= 26:
                if self.selected_item != 2:
                    mixer = g.mixer
                    sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
                    sound = mixer.device.convert(sound, sample_rate)
                    channel = mixer.play(sound=sound, volume=g.mixer_volume)
                self.selected_item = 2
            elif event.tile.y == 25 and event.tile.x <= 26:
                if self.selected_item != 3:
                    mixer = g.mixer
                    sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
                    sound = mixer.device.convert(sound, sample_rate)
                    channel = mixer.play(sound=sound, volume=g.mixer_volume)
                self.selected_item = 3
            elif event.tile.y == 26 and event.tile.x <= 26:
                if self.selected_item != 4:
                    mixer = g.mixer
                    sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
                    sound = mixer.device.convert(sound, sample_rate)
                    channel = mixer.play(sound=sound, volume=g.mixer_volume)
                self.selected_item = 4
            elif event.tile.y == 27 and event.tile.x <= 26:
                if self.selected_item != 4:
                    mixer = g.mixer
                    sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
                    sound = mixer.device.convert(sound, sample_rate)
                    channel = mixer.play(sound=sound, volume=g.mixer_volume)
                self.selected_item = 5
            else:
                if self.selected_item != 0:
                    mixer = g.mixer
                    sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
                    sound = mixer.device.convert(sound, sample_rate)
                    channel = mixer.play(sound=sound, volume=g.mixer_volume)
                self.selected_item = 0
            return None
        elif self.menu_mode == 1:
            if event.tile.y == 23 and event.tile.x >= 28 and event.tile.x <= 51:
                if self.selected_item != 1:
                    mixer = g.mixer
                    sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
                    sound = mixer.device.convert(sound, sample_rate)
                    channel = mixer.play(sound=sound, volume=g.mixer_volume)
                self.selected_item = 1
            elif event.tile.y == 24 and event.tile.x >= 28 and event.tile.x <= 51:
                if self.selected_item != 2:
                    mixer = g.mixer
                    sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
                    sound = mixer.device.convert(sound, sample_rate)
                    channel = mixer.play(sound=sound, volume=g.mixer_volume)
                self.selected_item = 2
            elif event.tile.y == 25 and event.tile.x >= 28 and event.tile.x <= 51:
                if self.selected_item != 3:
                    mixer = g.mixer
                    sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
                    sound = mixer.device.convert(sound, sample_rate)
                    channel = mixer.play(sound=sound, volume=g.mixer_volume)
                self.selected_item = 3
            elif event.tile.y == 26 and event.tile.x >= 28 and event.tile.x <= 51:
                if self.selected_item != 4:
                    mixer = g.mixer
                    sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
                    sound = mixer.device.convert(sound, sample_rate)
                    channel = mixer.play(sound=sound, volume=g.mixer_volume)
                self.selected_item = 4
            elif event.tile.y == 27 and event.tile.x >= 28 and event.tile.x <= 51:
                if self.selected_item != 5:
                    mixer = g.mixer
                    sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
                    sound = mixer.device.convert(sound, sample_rate)
                    channel = mixer.play(sound=sound, volume=g.mixer_volume)
                self.selected_item = 5
            else:
                if self.selected_item != 0:
                    mixer = g.mixer
                    sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
                    sound = mixer.device.convert(sound, sample_rate)
                    channel = mixer.play(sound=sound, volume=g.mixer_volume)
                self.selected_item = 0
        elif self.menu_mode == 2:
            if event.tile.y == 23 and event.tile.x <= 23:
                if self.selected_item != 1:
                    mixer = g.mixer
                    sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
                    sound = mixer.device.convert(sound, sample_rate)
                    channel = mixer.play(sound=sound, volume=g.mixer_volume)
                self.selected_item = 1
            elif event.tile.y == 24 and event.tile.x <= 23:
                if self.selected_item != 2:
                    mixer = g.mixer
                    sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
                    sound = mixer.device.convert(sound, sample_rate)
                    channel = mixer.play(sound=sound, volume=g.mixer_volume)
                self.selected_item = 2
            elif event.tile.y == 25 and event.tile.x <= 23:
                if self.selected_item != 3:
                    mixer = g.mixer
                    sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
                    sound = mixer.device.convert(sound, sample_rate)
                    channel = mixer.play(sound=sound, volume=g.mixer_volume)
                self.selected_item = 3
            elif event.tile.y == 26 and event.tile.x <= 23:
                if self.selected_item != 4:
                    mixer = g.mixer
                    sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
                    sound = mixer.device.convert(sound, sample_rate)
                    channel = mixer.play(sound=sound, volume=g.mixer_volume)
                self.selected_item = 4
            elif event.tile.y == 27 and event.tile.x <= 23:
                if self.selected_item != 5:
                    mixer = g.mixer
                    sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
                    sound = mixer.device.convert(sound, sample_rate)
                    channel = mixer.play(sound=sound, volume=g.mixer_volume)
                self.selected_item = 5
            elif event.tile.y == 29 and event.tile.x <= 23:
                if self.selected_item != 7:
                    mixer = g.mixer
                    sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
                    sound = mixer.device.convert(sound, sample_rate)
                    channel = mixer.play(sound=sound, volume=g.mixer_volume)
                self.selected_item = 7
            else:
                if self.selected_item != 0:
                    mixer = g.mixer
                    sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
                    sound = mixer.device.convert(sound, sample_rate)
                    channel = mixer.play(sound=sound, volume=g.mixer_volume)
                self.selected_item = 0
        elif self.menu_mode == 3:
            if self.v_kb_open == True:
                if event.tile.y >= 16 and event.tile.y <= 20 and event.tile.x >= 16 and event.tile.x <= 67:
                    if event.tile.y % 2 == 0 and event.tile.x % 2 == 0:
                        adder = 0
                        if event.tile.y == 18:
                            adder = 52
                        elif event.tile.y == 20:
                            adder = 104
                        if self.namecursor == 0:
                            mixer = g.mixer
                            sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
                            sound = mixer.device.convert(sound, sample_rate)
                            channel = mixer.play(sound=sound, volume=g.mixer_volume)
                        self.namecursor = (event.tile.x - 14) + adder 
                    else:
                        self.namecursor = 0
                elif event.tile.y == 22:
                    if event.tile.x >= 16 and event.tile.x <= 19:
                        if self.namecursor == 0:
                            mixer = g.mixer
                            sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
                            sound = mixer.device.convert(sound, sample_rate)
                            channel = mixer.play(sound=sound, volume=g.mixer_volume)
                            self.namecursor = 158
                    elif event.tile.x >= 32 and event.tile.x <= 36:
                        if self.namecursor == 0:
                            mixer = g.mixer
                            sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
                            sound = mixer.device.convert(sound, sample_rate)
                            channel = mixer.play(sound=sound, volume=g.mixer_volume)
                            self.namecursor = 158
                        self.namecursor = 160
                    elif event.tile.x >= 48 and event.tile.x <= 56:
                        if self.namecursor == 0:
                            mixer = g.mixer
                            sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
                            sound = mixer.device.convert(sound, sample_rate)
                            channel = mixer.play(sound=sound, volume=g.mixer_volume)
                            self.namecursor = 158
                        self.namecursor = 162
                    elif event.tile.x >= 63 and event.tile.x <= 67:
                        if self.namecursor == 0:
                            mixer = g.mixer
                            sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
                            sound = mixer.device.convert(sound, sample_rate)
                            channel = mixer.play(sound=sound, volume=g.mixer_volume)
                            self.namecursor = 158
                        self.namecursor = 164
                    else:
                        self.namecursor = 0
                else:
                    self.namecursor = 0
        elif self.menu_mode == 4:
            if event.tile.y >= 6 and event.tile.y <= 6+len(self.current_files):
                if self.selected_file != event.tile.y - 6:
                    mixer = g.mixer
                    sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
                    sound = mixer.device.convert(sound, sample_rate)
                    channel = mixer.play(sound=sound, volume=g.mixer_volume)
                    self.selected_file = event.tile.y - 6
            else:
                self.selected_file = 0
            if event.tile.x == 16 and event.tile.y == 20:
                if self.saveButtonSet != 1:
                    mixer = g.mixer
                    sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
                    sound = mixer.device.convert(sound, sample_rate)
                    channel = mixer.play(sound=sound, volume=g.mixer_volume)
                    self.saveButtonSet = 1
            elif event.tile.x == 67 and event.tile.y == 20:
                if self.saveButtonSet != 2:
                    mixer = g.mixer
                    sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
                    sound = mixer.device.convert(sound, sample_rate)
                    channel = mixer.play(sound=sound, volume=g.mixer_volume)
                    self.saveButtonSet = 2
            elif event.tile.x >= 40 and event.tile.x < 44:
                if self.saveButtonSet != 3:
                    mixer = g.mixer
                    sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
                    sound = mixer.device.convert(sound, sample_rate)
                    channel = mixer.play(sound=sound, volume=g.mixer_volume)
                    self.saveButtonSet = 3
            else:
                if self.saveButtonSet != 0:
                    mixer = g.mixer
                    sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
                    sound = mixer.device.convert(sound, sample_rate)
                    channel = mixer.play(sound=sound, volume=g.mixer_volume)
                    self.saveButtonSet = 0


