from __future__ import annotations
from typing import Optional, Tuple, TYPE_CHECKING

import colour
import exceptions
import random
import tile_types
from exceptions import Impossible

import numpy as np
import tcod

import time
import soundfile
import tcod.sdl.audio

import g

if TYPE_CHECKING:
    from engine import Engine
    from entity import Actor, Entity, Item

class Action:
    def __init__(self, entity: Actor) -> None:
        super().__init__()
        self.entity = entity

    @property
    def engine(self) -> Engine:
        return self.entity.gamemap.engine

    def perform(self) -> None:
        raise NotImplementedError()

class PickupAction(Action):
    def __init__(self, entity: Actor, item: Item):
        super().__init__(entity)
        self.item = item

    def perform(self) -> None:
        actor_location_x = self.entity.x
        actor_location_y = self.entity.y

        inventory = self.entity.inventory
        if len(inventory.items) >= inventory.capacity:
            raise exceptions.Impossible("Inventory is full!")
        

        self.engine.game_map.entities.remove(self.item)
        self.item.parent = self.entity.inventory
        inventory.items.append(self.item)

        self.engine.message_log.add_message(f"You picked up the {self.item.name}")
        #device = tcod.sdl.audio.open()
        #sound, sample_rate = soundfile.read("media/Audio/pickup.ogg", dtype="float32")
        #converted = device.convert(sound, sample_rate)
        #device.queue_audio(converted)
        
        #while device.queued_samples:
        #    time.sleep(0.00001)

        mixer = g.mixer
        sound, sample_rate = soundfile.read("media/Audio/pickup.ogg")
        sound = mixer.device.convert(sound, sample_rate)
        channel = mixer.play(sound=sound, volume=g.mixer_volume) 
        
        return

class ItemAction(Action):
    def __init__(self, entity: Actor, item: Item, target_xy: Optional[Tuple[int, int]] = None):
        super().__init__(entity)
        self.item = item
        if not target_xy:
            target_xy = entity.x, entity.y
        self.target_xy = target_xy

    @property
    def target_actor(self) -> Optional[Actor]:
        return self.engine.game_map.get_actor_at_location(*self.target_xy)

    def perform(self) -> None:
        if self.item.consumable:
            self.item.consumable.activate(self)

class DropItem(ItemAction):
    def perform(self) -> None:
        if self.entity.equipment.item_is_equipped(self.item):
            self.entity.equipment.toggle_equip(self.item)

        self.entity.inventory.drop(self.item)
        mixer = g.mixer
        sound, sample_rate = soundfile.read("media/Audio/drop.ogg")
        sound = mixer.device.convert(sound, sample_rate)
        channel = mixer.play(sound=sound, volume=g.mixer_volume) 

class EquipAction(Action):
    def __init__(self, entity: Actor, item: Item):
        super().__init__(entity)

        self.item = item

    def perform(self) -> None:
        device = tcod.sdl.audio.open()
        mixer = g.mixer
        sound, sample_rate = soundfile.read("media/Audio/equip.ogg")
        sound = mixer.device.convert(sound, sample_rate)
        channel = mixer.play(sound=sound, volume=g.mixer_volume) 
        self.entity.equipment.toggle_equip(self.item)

class PanicActionEquip(Action):
    def __init__(self, entity: Actor, item: Item):
        super().__init__(entity)

        self.item = item

    def perform(self) -> None:
        attack_colour = colour.enemy_attk
        self.engine.message_log.add_message(f"{self.entity.name}: is panicked", attack_colour)
        mixer = g.mixer
        sound, sample_rate = soundfile.read("media/Audio/shock.ogg")
        sound = mixer.device.convert(sound, sample_rate)
        channel = mixer.play(sound=sound, volume=g.mixer_volume)
        self.entity.move(0, 0)

class ShootAction(Action):
    def __init__(self, entity: Actor, target_xy: Optional[Tuple[int, int]] = None):
        super().__init__(entity)
        if not target_xy:
            target_xy = entity.x, entity.y
        self.target_xy = target_xy

    @property
    def target_actor(self) -> Optional[Actor]:
        return self.engine.game_map.get_actor_at_location(*self.target_xy)

    def perform(self) -> None:
        if(self.entity.equipment.machine_gun_equipped()):
            self.PlayerMachineGunShoot(self)
        else:
            self.PlayerShoot(self)

    def PlayerShoot(self, action: actions.ItemAction) -> None:
        target = action.target_actor

        

        if not target:
            raise exceptions.Impossible("Nothing to Attack.")

        mixer = g.mixer
        if self.entity.equipment.rifle_equipped() == True or self.entity.equipment.rifle_bayonet_equipped() == True or self.entity.equipment.sniper_equipped() == True:
            sound, sample_rate = soundfile.read("media/Audio/rifleshot.ogg")
        else:
            sound, sample_rate = soundfile.read("media/Audio/snd_pistol.ogg")
        sound = mixer.device.convert(sound, sample_rate)
        channel = mixer.play(sound=sound, volume=g.mixer_volume) 

        if self.entity is self.engine.player:
            attack_colour = colour.player_attk
        else:
            attack_colour = colour.enemy_attk
        
        dice = random.randint(1, 12)
        addNumb = 0

        if self.entity.soldier.dexterity <= 1:
            addNumb = -3
        elif self.entity.soldier.dexterity == 2 or self.entity.soldier.dexterity == 3:
            addNumb = -2
        elif self.entity.soldier.dexterity == 4:
            addNumb = -1
        elif self.entity.soldier.dexterity == 5:
            addNumb = 0
        elif self.entity.soldier.dexterity == 6 or self.entity.soldier.dexterity == 7:
            addNumb = 1
        elif self.entity.soldier.dexterity == 8 or self.entity.soldier.dexterity == 9:
            addNumb = 2
        else:
            addNumb = 3
        total = dice + addNumb + self.entity.soldier.aim_bonus

        if total <= 3:
            self.engine.message_log.add_message(f"Target missed:", attack_colour)
            damage = 0
        elif total > 3 and total <= 5:
            damage = (self.entity.soldier.shot_damage/4) - target.soldier.defense
        elif total > 5 and total <= 8:
            damage = (self.entity.soldier.shot_damage/2) - target.soldier.defense
        elif total > 8 and total <= 11:
            damage = ((self.entity.soldier.shot_damage/4) + (self.entity.soldier.shot_damage/2)) - target.soldier.defense
        else:
            damage = self.entity.soldier.shot_damage
            self.engine.message_log.add_message(f"CRITICAL HIT:", attack_colour)
            if target.equipment.mask_equiped() == True:
                target.equipment.destroy_mask()
                mixer = g.mixer
                sound, sample_rate = soundfile.read("media/Audio/mask_break.ogg")
                sound = mixer.device.convert(sound, sample_rate)
                channel = mixer.play(sound=sound, volume=g.mixer_volume)
                self.engine.message_log.add_message(f"{target.name} mask was destroyed", attack_colour)
            if target.panicNumber == 0:
                target.panicNumber = 3

        damage = round(damage)

        attack_desc = f"{self.entity.name.capitalize()} shoots {target.name}"

        self.entity.equipment.take_from_ammo(1)
        
        if damage > 0:
            sound, sample_rate = soundfile.read("media/Audio/hit.ogg")
            sound = mixer.device.convert(sound, sample_rate)
            channel = mixer.play(sound=sound, volume=g.mixer_volume) 
            self.engine.message_log.add_message(f"{attack_desc} for {damage} hit points", attack_colour)
            target.soldier.hp -= damage
        else:
            sound, sample_rate = soundfile.read("media/Audio/miss.ogg")
            sound = mixer.device.convert(sound, sample_rate)
            channel = mixer.play(sound=sound, volume=g.mixer_volume) 
            self.engine.message_log.add_message(f"{attack_desc} but does no dammage.", attack_colour)

    def PlayerMachineGunShoot(self, action: actions.ItemAction) -> None:
        for x in range(3):
            if self.entity.equipment.current_ammo() > 0:
                target = action.target_actor

                if not target:
                    raise exceptions.Impossible("Nothing to Attack.")

                if self.entity is self.engine.player:
                    attack_colour = colour.player_attk
                else:
                    attack_colour = colour.enemy_attk

                mixer = g.mixer
                sound, sample_rate = soundfile.read("media/Audio/mg_shot.ogg")
                sound = mixer.device.convert(sound, sample_rate)
                channel = mixer.play(sound=sound, volume=g.mixer_volume) 

                dice = random.randint(1, 12)
                addNumb = 0

                if self.entity.soldier.dexterity <= 1:
                    addNumb = -3
                elif self.entity.soldier.dexterity == 2 or self.entity.soldier.dexterity == 3:
                    addNumb = -2
                elif self.entity.soldier.dexterity == 4:
                    addNumb = -1
                elif self.entity.soldier.dexterity == 5:
                    addNumb = 0
                elif self.entity.soldier.dexterity == 6 or self.entity.soldier.dexterity == 7:
                    addNumb = 1
                elif self.entity.soldier.dexterity == 8 or self.entity.soldier.dexterity == 9:
                    addNumb = 2
                else:
                    addNumb = 3
                total = dice + addNumb + self.entity.soldier.aim_bonus

                if total <= 6:
                    self.engine.message_log.add_message(f"Target missed:", attack_colour)
                    damage = 0
                elif total > 6 and total <= 9:
                    damage = (self.entity.soldier.shot_damage/4) - target.soldier.defense
                elif total > 9 and total <= 11:
                    damage = (self.entity.soldier.shot_damage/2) - target.soldier.defense
                else:
                    damage = self.entity.soldier.shot_damage
                    self.engine.message_log.add_message(f"CRITICAL HIT:", attack_colour)
                    if target.equipment.mask_equiped() == True:
                        target.equipment.destroy_mask()
                        mixer = g.mixer
                        sound, sample_rate = soundfile.read("media/Audio/mask_break.ogg")
                        sound = mixer.device.convert(sound, sample_rate)
                        channel = mixer.play(sound=sound, volume=g.mixer_volume)
                        self.engine.message_log.add_message(f"{target.name} mask was destroyed", attack_colour)
                    if target.panicNumber == 0:
                        target.panicNumber = 3

                damage = round(damage)

                attack_desc = f"{self.entity.name.capitalize()} shoots {target.name}"

                self.entity.equipment.take_from_ammo(1)
                
                if damage > 0:
                    mixer = g.mixer
                    sound, sample_rate = soundfile.read("media/Audio/hit.ogg")
                    sound = mixer.device.convert(sound, sample_rate)
                    channel = mixer.play(sound=sound, volume=g.mixer_volume) 
                    self.engine.message_log.add_message(f"{attack_desc} for {damage} hit points", attack_colour)
                    target.soldier.hp -= damage
                else:
                    mixer = g.mixer
                    sound, sample_rate = soundfile.read("media/Audio/miss.ogg")
                    sound = mixer.device.convert(sound, sample_rate)
                    channel = mixer.play(sound=sound, volume=g.mixer_volume) 
                    self.engine.message_log.add_message(f"{attack_desc} but does no damage.", attack_colour)
            else:
                attack_colour = colour.player_attk
                self.engine.message_log.add_message(f"No shots fired. All ammo depleted", attack_colour)

class WaitAction(Action):
    def perform(self) -> None:
        pass

class ChangeRoomAction(Action):
    def perform(self) -> None:
        if (self.entity.x, self.entity.y) == self.engine.game_map.downstairs_location:
            g.music.close()
            mixer = g.mixer
            sound, sample_rate = soundfile.read("media/Audio/exit_level.ogg")
            sound = mixer.device.convert(sound, sample_rate)
            channel = mixer.play(sound=sound, volume=g.mixer_volume) 
            self.engine.game_world.generate_floor()
            #self.engine.message_log.add_message("You moved to another location", colour.descend)
        else:
            raise exceptions.Impossible("There is no exit here.")

class ActionWithDirection(Action):
    def __init__(self, entity: Actor, dx: int, dy: int):
        super().__init__(entity)

        self.dx = dx
        self.dy = dy
    @property
    def dest_xy(self) -> Tuple[int, int]:
        return self.entity.x + self.dx, self.entity.y + self.dy

    @property
    def blocking_entity(self) -> Optional[Entity]:
        return self.engine.game_map.get_blocking_entity_at_location(*self.dest_xy)

    @property
    def target_actor(self) -> Optional[Actor]:
        return self.engine.game_map.get_actor_at_location(*self.dest_xy)

    def perform(self) -> None:
        raise NotImplementedError()

class MeleeAction(ActionWithDirection):
    def perform(self) -> None:
        target = self.target_actor
        if not target:
            raise exceptions.Impossible("Nothing to Attack.")
        
        if self.entity is self.engine.player:
            attack_colour = colour.player_attk
        else:
            attack_colour = colour.enemy_attk
        mixer = g.mixer
        sound, sample_rate = soundfile.read("media/Audio/Meleeattack.ogg")
        sound = mixer.device.convert(sound, sample_rate)
        channel = mixer.play(sound=sound, volume=g.mixer_volume) 
        
        dice = random.randint(1, 12)
        addNumb = 0

        if self.entity.soldier.strength <= 1:
            addNumb = -3
        elif self.entity.soldier.strength == 2 or self.entity.soldier.strength == 3:
            addNumb = -2
        elif self.entity.soldier.strength == 4:
            addNumb = -1
        elif self.entity.soldier.strength == 5:
            addNumb = 0
        elif self.entity.soldier.strength == 6 or self.entity.soldier.strength == 7:
            addNumb = 1
        elif self.entity.soldier.strength == 8 or self.entity.soldier.strength == 9:
            addNumb = 2
        else:
            addNumb = 3
        total = dice + addNumb

        if total <= 4:
            damage = self.entity.soldier.strength - target.soldier.defense
        elif total > 4 and total <= 8:
            damage = (self.entity.soldier.strength + 1) - target.soldier.defense
        elif total > 8 and total <= 11:
            damage = (self.entity.soldier.strength + 2) - target.soldier.defense
        else:
            damage = (self.entity.soldier.strength * 2) - target.soldier.defense
            self.engine.message_log.add_message(f"CRITICAL HIT:", attack_colour)
            if target.equipment.mask_equiped() == True:
                target.equipment.destroy_mask()
                mixer = g.mixer
                sound, sample_rate = soundfile.read("media/Audio/mask_break.ogg")
                sound = mixer.device.convert(sound, sample_rate)
                channel = mixer.play(sound=sound, volume=g.mixer_volume)
                self.engine.message_log.add_message(f"{target.name} mask was destroyed", attack_colour)
            if target.panicNumber == 0:
                target.panicNumber = 3

        attack_desc = f"{self.entity.name.capitalize()} attacks {target.name}"
        

        if damage > 0:
            mixer = g.mixer
            sound, sample_rate = soundfile.read("media/Audio/hit.ogg")
            sound = mixer.device.convert(sound, sample_rate)
            channel = mixer.play(sound=sound, volume=g.mixer_volume) 
            self.engine.message_log.add_message(f"{attack_desc} for {damage} hit points", attack_colour)
            target.soldier.hp -= damage
        else:
            mixer = g.mixer
            sound, sample_rate = soundfile.read("media/Audio/miss.ogg")
            sound = mixer.device.convert(sound, sample_rate)
            channel = mixer.play(sound=sound, volume=g.mixer_volume) 
            self.engine.message_log.add_message(f"{attack_desc} but does no damage.", attack_colour)

class EnemyShootAction(ActionWithDirection):
    def perform(self) -> None:
        target = self.target_actor
        if not target:
            raise exceptions.Impossible("Nothing to Attack.")

        if self.entity is self.engine.player:
            attack_colour = colour.player_attk
        else:
            attack_colour = colour.enemy_attk

        mixer = g.mixer
        sound, sample_rate = soundfile.read("media/Audio/rifleshot.ogg")
        sound = mixer.device.convert(sound, sample_rate)
        channel = mixer.play(sound=sound, volume=g.mixer_volume) 
        dice = random.randint(1, 12)
        addNumb = 0

        if self.entity.soldier.dexterity <= 1:
            addNumb = -3
        elif self.entity.soldier.dexterity == 2 or self.entity.soldier.dexterity == 3:
            addNumb = -2
        elif self.entity.soldier.dexterity == 4:
            addNumb = -1
        elif self.entity.soldier.dexterity == 5:
            addNumb = 0
        elif self.entity.soldier.dexterity == 6 or self.entity.soldier.dexterity == 7:
            addNumb = 1
        elif self.entity.soldier.dexterity == 8 or self.entity.soldier.dexterity == 9:
            addNumb = 2
        else:
            addNumb = 3
        total = dice + addNumb + self.entity.soldier.aim_bonus

        
        if total <= 3:
            self.engine.message_log.add_message(f"Target missed:", attack_colour)
            damage = 0
        elif total > 3 and total <= 5:
            damage = (self.entity.soldier.shot_damage/4) - target.soldier.defense
        elif total > 5 and total <= 8:
            damage = (self.entity.soldier.shot_damage/2) - target.soldier.defense
        elif total > 8 and total <= 11:
            damage = ((self.entity.soldier.shot_damage/4) + (self.entity.soldier.shot_damage/2)) - target.soldier.defense
        else:
            damage = self.entity.soldier.shot_damage
            self.engine.message_log.add_message(f"CRITICAL HIT:", attack_colour)
            if target.equipment.mask_equiped() == True:
                target.equipment.destroy_mask()
                mixer = g.mixer
                sound, sample_rate = soundfile.read("media/Audio/mask_break.ogg")
                sound = mixer.device.convert(sound, sample_rate)
                channel = mixer.play(sound=sound, volume=g.mixer_volume)
                self.engine.message_log.add_message(f"{target.name} mask was destroyed", attack_colour)
            if target.panicNumber == 0:
                target.panicNumber = 3

        damage = round(damage)

        attack_desc = f"{self.entity.name.capitalize()} shoots {target.name}"

        if damage > 0:
            sound, sample_rate = soundfile.read("media/Audio/hit.ogg")
            sound = mixer.device.convert(sound, sample_rate)
            channel = mixer.play(sound=sound, volume=g.mixer_volume)
            self.engine.message_log.add_message(f"{attack_desc} for {damage} hit points", attack_colour)
            target.soldier.hp -= damage
        else:
            sound, sample_rate = soundfile.read("media/Audio/miss.ogg")
            sound = mixer.device.convert(sound, sample_rate)
            channel = mixer.play(sound=sound, volume=g.mixer_volume)
            self.engine.message_log.add_message(f"{attack_desc} but does no damage.", attack_colour)

class EnemyMachineGunShootAction(ActionWithDirection):
    def perform(self) -> None:
        for x in range(3):
            target = self.target_actor
            if not target:
                raise exceptions.Impossible("Nothing to Attack.")

            if self.entity is self.engine.player:
                attack_colour = colour.player_attk
            else:
                attack_colour = colour.enemy_attk

            mixer = g.mixer
            sound, sample_rate = soundfile.read("media/Audio/mg_shot.ogg")
            sound = mixer.device.convert(sound, sample_rate)
            channel = mixer.play(sound=sound, volume=g.mixer_volume) 
            dice = random.randint(1, 12)
            addNumb = 0

            if self.entity.soldier.dexterity <= 1:
                addNumb = -3
            elif self.entity.soldier.dexterity == 2 or self.entity.soldier.dexterity == 3:
                addNumb = -2
            elif self.entity.soldier.dexterity == 4:
                addNumb = -1
            elif self.entity.soldier.dexterity == 5:
                addNumb = 0
            elif self.entity.soldier.dexterity == 6 or self.entity.soldier.dexterity == 7:
                addNumb = 1
            elif self.entity.soldier.dexterity == 8 or self.entity.soldier.dexterity == 9:
                addNumb = 2
            else:
                addNumb = 3
            total = dice + addNumb + self.entity.soldier.aim_bonus

            
            if total <= 6:
                self.engine.message_log.add_message(f"Target missed:", attack_colour)
                damage = 0
            elif total > 6 and total <= 9:
                damage = (self.entity.soldier.shot_damage/4) - target.soldier.defense
            elif total > 9 and total <= 11:
                damage = (self.entity.soldier.shot_damage/2) - target.soldier.defense
            else:
                damage = self.entity.soldier.shot_damage
                self.engine.message_log.add_message(f"CRITICAL HIT:", attack_colour)
                if target.equipment.mask_equiped() == True:
                    target.equipment.destroy_mask()
                    mixer = g.mixer
                    sound, sample_rate = soundfile.read("media/Audio/mask_break.ogg")
                    sound = mixer.device.convert(sound, sample_rate)
                    channel = mixer.play(sound=sound, volume=g.mixer_volume)
                    self.engine.message_log.add_message(f"{target.name} mask was destroyed", attack_colour)
                if target.panicNumber == 0:
                    target.panicNumber = 3

            damage = round(damage)

            attack_desc = f"{self.entity.name.capitalize()} shoots {target.name}"

            if damage > 0:
                mixer = g.mixer
                sound, sample_rate = soundfile.read("media/Audio/hit.ogg")
                sound = mixer.device.convert(sound, sample_rate)
                channel = mixer.play(sound=sound, volume=g.mixer_volume) 
                self.engine.message_log.add_message(f"{attack_desc} for {damage} hit points", attack_colour)
                target.soldier.hp -= damage
            else:
                mixer = g.mixer
                sound, sample_rate = soundfile.read("media/Audio/miss.ogg")
                sound = mixer.device.convert(sound, sample_rate)
                channel = mixer.play(sound=sound, volume=g.mixer_volume) 
                self.engine.message_log.add_message(f"{attack_desc} but does no damage.", attack_colour)

class EnemyHealAction(ActionWithDirection):
    def perform(self) -> None:
        target = self.target_actor
        if not target:
            raise exceptions.Impossible("Nothing to Heal.")

        heal_colour = colour.enemy_attk

        healAmount = 1

        
        dice = random.randint(1, 12)
        addNumb = 0

        if self.entity.soldier.vitality <= 1:
            addNumb = -3
        elif self.entity.soldier.vitality == 2 or self.entity.soldier.vitality == 3:
            addNumb = -2
        elif self.entity.soldier.vitality == 4:
            addNumb = -1
        elif self.entity.soldier.vitality == 5:
            addNumb = 0
        elif self.entity.soldier.vitality == 6 or self.entity.soldier.vitality == 7:
            addNumb = 1
        elif self.entity.soldier.vitality == 8 or self.entity.soldier.vitality == 9:
            addNumb = 2
        else:
            addNumb = 3
        total = dice + addNumb

        
        if total <= 3:
            healAmount = healAmount
        elif total > 3 and total <= 5:
            healAmount = healAmount + 2
        elif total > 5 and total <= 8:
            healAmount = healAmount + 4
        elif total > 8 and total <= 11:
            healAmount = healAmount + 6
        else:
            healAmount = target.soldier.maxHp

        amount_recovered = target.soldier.heal(healAmount)


        self.engine.message_log.add_message(f"{self.entity.name.capitalize()} heals {target.name} for {healAmount}", heal_colour)

class EnemyExplosionAction(ActionWithDirection):
    def perform(self) -> None:
        radius = 3
        damage = 12
        for actor in list(self.engine.game_map.actors).copy():
            if actor.distance(self.dx, self.dy) <= radius:
                self.engine.message_log.add_message(f"The {actor.name} is hit by the explosion of the grenade, taking {damage} damage!")
                actor.soldier.take_damage(damage)
                actor.panicNumber = 5
        mixer = g.mixer
        sound, sample_rate = soundfile.read("media/Audio/explode.ogg")
        sound = mixer.device.convert(sound, sample_rate)
        channel = mixer.play(sound=sound, volume=g.mixer_volume) 
class FlameAction(ActionWithDirection):
    def perform(self) -> None:
        target = self.target_actor
        if not target:
            raise exceptions.Impossible("Nothing to Attack.")
        mixer = g.mixer
        sound, sample_rate = soundfile.read("media/Audio/flame.ogg")
        sound = mixer.device.convert(sound, sample_rate)
        channel = mixer.play(sound=sound, volume=g.mixer_volume) 
        
        if self.entity is self.engine.player:
            attack_colour = colour.player_attk
        else:
            attack_colour = colour.enemy_attk

        if self.engine.game_map.tiles["walkable"][target.x, target.y]:
            self.engine.game_map.tiles[target.x, target.y] = tile_types.fire
        

class MovementAction(ActionWithDirection):
    

    def perform(self) -> None:
        dest_x, dest_y = self.dest_xy

        if not self.engine.game_map.in_bounds(dest_x, dest_y):
            raise exceptions.Impossible("That area is blocked1")
        if not self.engine.game_map.tiles["walkable"][dest_x, dest_y]:
            raise exceptions.Impossible("That area is blocked2")
        if self.engine.game_map.get_blocking_entity_at_location(dest_x, dest_y):
            raise exceptions.Impossible("That area is blocked3")
        if self.entity.equipment.machine_gun_equipped():
            raise exceptions.Impossible("You can't move with a Machine gun")
        

        if self.engine.game_map.tiles[dest_x, dest_y] == tile_types.door_closed or self.engine.game_map.tiles[dest_x, dest_y] == tile_types.gas_door_closed:
            mixer = g.mixer
            sound, sample_rate = soundfile.read("media/Audio/door_open.ogg")
            sound = mixer.device.convert(sound, sample_rate)
            channel = mixer.play(sound=sound, volume=g.mixer_volume)
            self.entity.open(self.dx, self.dy)
        else:
            self.entity.move(self.dx, self.dy)

        if self.engine.game_map.tiles[dest_x, dest_y] == tile_types.mine_active:
            dice = random.randint(1, 12)
            addNumb = 0

            if self.entity.soldier.dexterity <= 1:
                addNumb = -3
            elif self.entity.soldier.dexterity  == 2 or self.entity.soldier.dexterity == 3:
                addNumb = -2
            elif self.entity.soldier.dexterity  == 4:
                addNumb = -1
            elif self.entity.soldier.dexterity  == 5:
                addNumb = 0
            elif self.entity.soldier.dexterity  == 6 or self.entity.soldier.dexterity  == 7:
                addNumb = 1
            elif self.entity.soldier.dexterity  == 8 or self.entity.soldier.dexterity  == 9:
                addNumb = 2
            else:
                addNumb = 3
            total = dice + addNumb

            if total <= 7:
                mixer = g.mixer
                sound, sample_rate = soundfile.read("media/Audio/explode.ogg")
                sound = mixer.device.convert(sound, sample_rate)
                channel = mixer.play(sound=sound, volume=g.mixer_volume)
                fail_colour = colour.enemy_attk
                self.engine.message_log.add_message(f"{self.entity.name} detonated a mine, taking 12 damage")
                self.entity.soldier.hp -= 12
                self.engine.game_map.tiles[dest_x, dest_y] = tile_types.floor
                #future for loop for entities in the area surrounding it
                target_xy = self.entity.x, self.entity.y
                for actor in list(self.engine.game_map.actors).copy():
                    if actor.distance(*target_xy) <= 3:
                        self.engine.message_log.add_message(f"The {actor.name} is hit by the explosion of the mine, taking {5} damage!")
                        actor.soldier.take_damage(5)
                        actor.panicNumber = 5
            else:
                success_colour = colour.white
                self.engine.message_log.add_message(f"{self.entity.name} avoided setting off the mine.", success_colour)


class PanicAction(ActionWithDirection):
    def getPanic(self, entity: Entity) -> bool:

        if entity.panicNumber > 0:
            entity.panicNumber = entity.panicNumber - 1
            dice = random.randint(1, 12)
            addNumb = 0

            if entity.soldier.composure <= 1:
                addNumb = -3
            elif entity.soldier.composure == 2 or entity.soldier.composure == 3:
                addNumb = -2
            elif entity.soldier.composure == 4:
                addNumb = -1
            elif entity.soldier.composure == 5:
                addNumb = 0
            elif entity.soldier.composure == 6 or entity.soldier.composure == 7:
                addNumb = 1
            elif entity.soldier.composure == 8 or entity.soldier.composure == 9:
                addNumb = 2
            else:
                addNumb = 3
            total = dice + addNumb

            if total <= 4:
                fail_colour = colour.enemy_attk
                self.engine.message_log.add_message(f"{entity.name} is panicked", fail_colour)
                mixer = g.mixer
                sound, sample_rate = soundfile.read("media/Audio/shock.ogg")
                sound = mixer.device.convert(sound, sample_rate)
                channel = mixer.play(sound=sound, volume=g.mixer_volume)
                return True

            return False

        hp_check =  entity.soldier.maxHp / 10
        if entity.soldier.hp < hp_check:
            dice = random.randint(1, 12)
            addNumb = 0

            if entity.soldier.composure <= 1:
                addNumb = -3
            elif entity.soldier.composure == 2 or entity.soldier.composure == 3:
                addNumb = -2
            elif entity.soldier.composure == 4:
                addNumb = -1
            elif entity.soldier.composure == 5:
                addNumb = 0
            elif entity.soldier.composure == 6 or entity.soldier.composure == 7:
                addNumb = 1
            elif entity.soldier.composure == 8 or entity.soldier.composure == 9:
                addNumb = 2
            else:
                addNumb = 3
            total = dice + addNumb

            if total <= 4:
                fail_colour = colour.enemy_attk
                self.engine.message_log.add_message(f"{entity.name} is panicked", fail_colour)
                mixer = g.mixer
                sound, sample_rate = soundfile.read("media/Audio/shock.ogg")
                sound = mixer.device.convert(sound, sample_rate)
                channel = mixer.play(sound=sound, volume=g.mixer_volume)
                return True


            return False
        if self.engine.game_map.getGasActive and entity.equipment.mask_equiped() == False:
            dice = random.randint(1, 12)
            addNumb = 0

            if entity.soldier.composure <= 1:
                addNumb = -3
            elif entity.soldier.composure == 2 or entity.soldier.composure == 3:
                addNumb = -2
            elif entity.soldier.composure == 4:
                addNumb = -1
            elif entity.soldier.composure == 5:
                addNumb = 0
            elif entity.soldier.composure == 6 or entity.soldier.composure == 7:
                addNumb = 1
            elif entity.soldier.composure == 8 or entity.soldier.composure == 9:
                addNumb = 2
            else:
                addNumb = 3
            total = dice + addNumb

            if total <= 4:
                fail_colour = colour.enemy_attk
                mixer = g.mixer
                sound, sample_rate = soundfile.read("media/Audio/shock.ogg")
                sound = mixer.device.convert(sound, sample_rate)
                channel = mixer.play(sound=sound, volume=g.mixer_volume)
                self.engine.message_log.add_message(f"{entity.name} is panicked", fail_colour)
                return True

            if entity is not self.engine.player:
                success_colour = colour.white
                self.engine.message_log.add_message(f"{entity.name} Equipped Gas Mask", success_colour)
                entity.soldier.equipMask()
                return True


            return False

    def perform(self) -> None:
        self.entity.move(0, 0)

class MouseMovementAction(ActionWithDirection):
    def get_safe_path_to(self, dest_x: int, dest_y: int) -> List[Tuple[int, int]]:

        #gets path to target position

        cost = np.array(self.entity.gamemap.tiles["safe"], dtype=np.int8)

        for entity in self.entity.gamemap.entities:
            if entity.blocks_movement and cost[entity.x, entity.y]:

                cost[entity.x, entity.y] += 10

        graph = tcod.path.SimpleGraph(cost=cost, cardinal=2, diagonal=3)
        pathfinder = tcod.path.Pathfinder(graph)

        pathfinder.add_root((self.entity.x, self.entity.y))

        path: List[List[int]] = pathfinder.path_to((dest_x, dest_y))[1:].tolist()

        return [(index[0], index[1]) for index in path]

    def perform(self) -> None:
        dest_x, dest_y = self.dest_xy
        camera = self.engine.game_map.get_camera()
        dx = dest_x - self.entity.x
        dy = dest_y - self.entity.y
        distance = max(abs(dx), abs(dy))
        if(PanicAction.getPanic(self, self.entity)):
            return PanicAction(self.entity, self.entity.x, self.entity.y).perform()

        if self.engine.game_map.visible[self.entity.x, self.entity.y]:
            
            self.path = self.get_safe_path_to(dx, dy)
        else:
            self.path = None

        if self.path:
            dest_x, dest_y = self.path.pop(0)
            return BumpAction(self.entity, dest_x - self.entity.x, dest_y - self.entity.y).perform()
class StuckAction(ActionWithDirection):
    def getMove(self, entity: Entity) -> bool:
        dice = random.randint(1, 12)
        addNumb = 0

        if entity.soldier.dexterity <= 1:
            addNumb = -3
        elif entity.soldier.dexterity  == 2 or entity.soldier.dexterity == 3:
            addNumb = -2
        elif entity.soldier.dexterity  == 4:
            addNumb = -1
        elif entity.soldier.dexterity  == 5:
            addNumb = 0
        elif entity.soldier.dexterity  == 6 or entity.soldier.dexterity  == 7:
            addNumb = 1
        elif entity.soldier.dexterity  == 8 or entity.soldier.dexterity  == 9:
            addNumb = 2
        else:
            addNumb = 3
        total = dice + addNumb

        if total <= 4:
            fail_colour = colour.enemy_attk
            self.engine.message_log.add_message(f"{entity.name} is stuck", fail_colour)
            return True


        return False

    def perform(self) -> None:
        self.entity.move(0, 0)

        

class SpawnAction(ActionWithDirection):
    

    def perform(self) -> None:
        dest_x, dest_y = self.dest_xy

        if not self.engine.game_map.in_bounds(dest_x, dest_y):
            raise exceptions.Impossible("That area is blocked")
        if not self.engine.game_map.tiles["walkable"][dest_x, dest_y]:
            raise exceptions.Impossible("That area is blocked")
        if self.engine.game_map.get_blocking_entity_at_location(dest_x, dest_y):
            raise exceptions.Impossible("That area is blocked")
            

        self.entity.spawn(self.engine.game_map, self.dx, self.dy)

        

class BumpAction(ActionWithDirection):
    def perform(self) -> None:
        
        
        if(PanicAction.getPanic(self, self.entity)):
            return PanicAction(self.entity, self.entity.x, self.entity.y).perform()

        
        
        if self.target_actor:
            return MeleeAction(self.entity, self.dx, self.dy).perform()
        else:
            if self.engine.game_map.tiles["mud"][self.entity.x, self.entity.y]:
                if(StuckAction.getMove(self, self.entity)):
                    return StuckAction(self.entity, self.entity.x, self.entity.y).perform()
            return MovementAction(self.entity, self.dx, self.dy).perform()