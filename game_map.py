from __future__ import annotations

from typing import Iterable, Iterator, Optional, TYPE_CHECKING

import numpy as np
from tcod.console import Console

from entity import Actor, Item, GasGrenade
import tile_types
import random

import g

import time
import soundfile
import tcod.sdl.audio
import colour
if TYPE_CHECKING:
    from engine import Engine
    from entity import Entity



class GameMap:
    def __init__(self, engine: Engine, width: int, height: int, gasActive: bool, entities: Iterable[Entity] = ()):
        self.engine = engine
        self.width, self.height = width, height
        self.entities = set(entities)
        self.tiles = np.full((width, height), fill_value=tile_types.wall, order="F")
        self.gasActive = gasActive

        self.visible = np.full((width, height), fill_value=False, order="F")
        self.light_levels = np.full((width, height), fill_value=1.0, order="F")
        self.explored = np.full((width, height), fill_value=False, order="F")
        self.downstairs_location = (0, 0)
        self.map_name = ""

    @property
    def gamemap(self) -> GameMap:
        return self
    
    @property
    def actors(self) -> Iterator[Actor]:
        yield from (entity for entity in self.entities if isinstance(entity, Actor) and entity.is_alive)

    @property
    def items(self) -> Iterator[Item]:
        yield from (entity for entity in self.entities if isinstance(entity, Item))

    @property
    def gasActors(self) -> Iterator[Actor]:
        yield from (entity for entity in self.entities if isinstance(entity, GasGrenade))

    @property
    def getGasActive(self) -> bool:
        return self.gasActive
    @property
    def setGasActive(self) -> bool:
        if self.gasActive:
            self.gasActive = False
        else:
            self.gasActive = True    

    def get_blocking_entity_at_location(self, location_x: int, location_y: int) -> Optional[Entity]:
        for entity in self.entities:
            if entity.blocks_movement and entity.x == location_x and entity.y == location_y:
                return entity
        
        return None

    def get_actor_at_location(self, x: int, y: int) -> Optional[Actor]:
        for actor in self.actors:
            if actor.x == x and actor.y == y:
                return actor
        return None
    
    def get_entity_name_at_location(self, location_x: int, location_y: int) -> Optional[str]:
        rtnStr = ""
        for entity in self.entities:
            if entity.x == location_x and entity.y == location_y:
                if rtnStr == "":
                    rtnStr = entity.name
                else:
                    rtnStr = rtnStr + ", " + entity.name
        if rtnStr == "":
            return "No entity found"
        else:
            return rtnStr

    def in_bounds(self, x: int, y: int) -> bool:
        return 0 <= x < self.width and 0 <= y < self.height

    def get_camera(self):
        width = self.engine.game_world.viewport_width
        height = self.engine.game_world.viewport_height
        x_1 = self.engine.player.x - int(width/2)
        y_1 = self.engine.player.y - int(height/2)
        if x_1 < 0:
            x_1 = 0
        if y_1 < 0:
            y_1 = 0

        x_2 = x_1 + width
        y_2 = y_1 + height
        if x_2 > self.width:
            x_dist = x_2 - self.width
            x_1 -= x_dist
            x_2 -= x_dist

        if y_2 > self.height:
            y_dist = y_2 - self.height
            y_1 -= y_dist
            y_2 -= y_dist

        return((x_1, y_1, x_2-1, y_2-1))

    def open(self, x: int, y: int) -> None:
        if self.tiles[x, y] == tile_types.door_closed:
            self.tiles[x, y] = tile_types.door_open
        if self.tiles[x, y] == tile_types.gas_door_closed:
            self.tiles[x, y] = tile_types.gas_door_open


    def render(self, console: Console) -> None:
        x_1, y_1, x_2, y_2 = self.get_camera()
        x_view = slice(x_1, x_2+1)
        y_view = slice(y_1,y_2+1)
        view_tiles = self.tiles[x_view, y_view]
        view_visable = self.visible[x_view, y_view]
        view_explored = self.explored[x_view,y_view]

        if g.isAscii == True:
            console.rgb[0:self.engine.game_world.viewport_width, 0:self.engine.game_world.viewport_height] = np.select(condlist=[view_visable, view_explored], choicelist=[view_tiles["light"], view_tiles["dark"]], default=tile_types.SHROUD)

            player = self.engine.player
            entities_sorted_for_rendering = sorted(
                self.entities, key=lambda x: x.render_order.value
            )

            for entity in entities_sorted_for_rendering:
                if self.visible[entity.x, entity.y]:
                    if entity in self.actors:
                        console.print(x=entity.x - x_1,
                            y=entity.y - y_1,
                            string=entity.char,
                            fg=entity.colour)
                    else:
                        console.print(x=entity.x - x_1,
                        y=entity.y - y_1,
                        string=entity.char,
                        fg=entity.colour)
        else:
            console.rgb[0:self.engine.game_world.viewport_width, 0:self.engine.game_world.viewport_height] = np.select(condlist=[view_visable, view_explored], choicelist=[view_tiles["light_sprite"], view_tiles["dark_sprite"]], default=tile_types.SHROUD)

            player = self.engine.player
            entities_sorted_for_rendering = sorted(
                self.entities, key=lambda x: x.render_order.value
            )

            for entity in entities_sorted_for_rendering:
                if self.visible[entity.x, entity.y]:
                    if entity in self.actors:
                        console.print(x=entity.x - x_1,
                            y=entity.y - y_1,
                            string=entity.sprite,
                            fg=(255, 255, 255))
                    else:
                        console.print(x=entity.x - x_1,
                        y=entity.y - y_1,
                        string=entity.sprite,
                        fg=entity.colour)


class GameWorld:
    #hold setttings for the gamemap and generates new maps
    def __init__(self, *, engine: Engine, map_width: int, map_height: int, viewport_width: int, viewport_height: int, max_rooms: int, room_min_size: int, room_max_size: int, current_floor: int = 0):
        self.engine = engine
        self.map_width = map_width
        self.map_height = map_height
        self.max_rooms = max_rooms
        self.room_min_size = room_min_size
        self.room_max_size = room_max_size
        self.current_floor = current_floor
        self.viewport_width = viewport_width
        self.viewport_height = viewport_height

    def generate_floor(self) -> None:
        from procgen_trench import generate_trench
        from procgen_tunnel import generate_tunnel
        from procgen_mud import generate_mud
        from procgen_mine import generate_minefield
        from procgen_village import generate_village
        from procgen_fire import generate_fire
        from procgen_boss_1 import generate_dungeon_boss_1
        from procgen_boss_2 import generate_dungeon_boss_2
        from procgen_boss_3 import generate_dungeon_boss_3
        from procgen_enemy_showcase import generate_dungeon_showcase
        from procgen_epilogue import generate_epilogue
        g.music = tcod.sdl.audio.BasicMixer(tcod.sdl.audio.open())
        music = g.music
        self.current_floor += 1
        if self.current_floor == 1 or self.current_floor == 11 or self.current_floor == 21 or self.current_floor == 31:
            self.engine.story_shown = False
        if(self.current_floor == 1):
            self.engine.game_map = generate_trench(max_rooms=self.max_rooms, room_min_size=self.room_min_size, room_max_size=self.room_max_size, map_width=self.map_width, map_height=self.map_height, engine=self.engine)
            self.engine.set_fov_radius(10)
            self.engine.set_original_fov(20)
            sound, sample_rate = soundfile.read("media/Music/track_3.ogg")
            sound = music.device.convert(sound, sample_rate)
            channel = music.play(sound=sound, loops=-1,  volume=g.music_volume)
            g.global_channel = channel
            self.engine.music_name="track_3.ogg"
        if(self.current_floor > 1 and self.current_floor < 10):
            room_numb = random.randint(0, 1)
            if room_numb == 0:
                self.engine.message_log.add_message("You enter another Trench", colour.descend)
                self.engine.game_map = generate_trench(max_rooms=self.max_rooms, room_min_size=self.room_min_size, room_max_size=self.room_max_size, map_width=self.map_width, map_height=self.map_height, engine=self.engine)
                self.engine.set_fov_radius(10)
                sound, sample_rate = soundfile.read("media/Music/track_3.ogg")
                sound = music.device.convert(sound, sample_rate)
                channel = music.play(sound=sound, loops=-1,  volume=g.music_volume)
                g.global_channel = channel
                self.engine.music_name="track_3.ogg"
            else:
                self.engine.message_log.add_message("You descend into the tunnels", colour.descend)
                self.engine.game_map = generate_tunnel(max_rooms=self.max_rooms, room_min_size=self.room_min_size, room_max_size=self.room_max_size, map_width=self.map_width, map_height=self.map_height, engine=self.engine)
                self.engine.set_fov_radius(10)
                self.engine.set_original_fov(10)
                sound, sample_rate = soundfile.read("media/Music/track_12.ogg")
                sound = music.device.convert(sound, sample_rate)
                channel = music.play(sound=sound, loops=-1,  volume=g.music_volume)
                g.global_channel = channel
                self.engine.music_name="track_12.ogg"
        if(self.current_floor == 10):
            self.engine.message_log.add_message("You enter an abnormal trench", colour.descend)
            self.engine.game_map = generate_dungeon_boss_1(max_rooms=3, room_min_size=self.room_min_size, room_max_size=self.room_max_size, map_width=80, map_height=50, engine=self.engine)
            sound, sample_rate = soundfile.read("media/Music/track_4.ogg")
            sound = music.device.convert(sound, sample_rate)
            channel = music.play(sound=sound, loops=-1,  volume=g.music_volume)
            g.global_channel = channel
            self.engine.music_name="track_4.ogg"
        if(self.current_floor > 10 and self.current_floor < 20):
            room_numb = random.randint(0, 3)
            if room_numb == 0:
                self.engine.message_log.add_message("You enter another Trench", colour.descend)
                self.engine.game_map = generate_trench(max_rooms=self.max_rooms, room_min_size=self.room_min_size, room_max_size=self.room_max_size, map_width=self.map_width, map_height=self.map_height, engine=self.engine)
                self.engine.set_fov_radius(10)
                sound, sample_rate = soundfile.read("media/Music/track_3.ogg")
                sound = music.device.convert(sound, sample_rate)
                channel = music.play(sound=sound, loops=-1,  volume=g.music_volume)
                g.global_channel = channel
                self.engine.music_name="track_3.ogg"
            elif room_numb == 1:
                self.engine.message_log.add_message("You descend into the tunnels", colour.descend)
                self.engine.game_map = generate_tunnel(max_rooms=self.max_rooms, room_min_size=self.room_min_size, room_max_size=self.room_max_size, map_width=self.map_width, map_height=self.map_height, engine=self.engine)
                self.engine.set_fov_radius(10)
                self.engine.set_original_fov(20)
                sound, sample_rate = soundfile.read("media/Music/track_12.ogg")
                sound = music.device.convert(sound, sample_rate)
                channel = music.play(sound=sound, loops=-1,  volume=g.music_volume)
                g.global_channel = channel
                self.engine.music_name="track_12.ogg"
            elif room_numb == 2:
                self.engine.message_log.add_message("You enter a muddy forest", colour.descend)
                self.engine.game_map = generate_mud(max_rooms=90, room_min_size=self.room_min_size, room_max_size=self.room_max_size, map_width=self.map_width, map_height=self.map_height, engine=self.engine)
                self.engine.set_fov_radius(10)
                self.engine.set_original_fov(20)
                sound, sample_rate = soundfile.read("media/Music/track_2.ogg")
                sound = music.device.convert(sound, sample_rate)
                channel = music.play(sound=sound, loops=-1,  volume=g.music_volume)
                g.global_channel = channel
                self.engine.music_name="track_2.ogg"
            else:
                self.engine.message_log.add_message("You enter a dangerous minefield", colour.descend)
                self.engine.game_map = generate_minefield(max_rooms=self.max_rooms, room_min_size=self.room_min_size, room_max_size=self.room_max_size, map_width=self.map_width, map_height=self.map_height, engine=self.engine)
                self.engine.set_fov_radius(10)
                self.engine.set_original_fov(20)
                sound, sample_rate = soundfile.read("media/Music/track_1.ogg")
                sound = music.device.convert(sound, sample_rate)
                channel = music.play(sound=sound, loops=-1,  volume=g.music_volume)
                g.global_channel = channel
                self.engine.music_name="track_1.ogg"
        elif(self.current_floor == 20):
            self.engine.message_log.add_message("You appear in a spine-chilling field of mud", colour.descend)
            self.engine.game_map = generate_dungeon_boss_2(max_rooms=2, room_min_size=self.room_min_size, room_max_size=self.room_max_size, map_width=80, map_height=50, engine=self.engine)
            sound, sample_rate = soundfile.read("media/Music/track_7.ogg")
            sound = music.device.convert(sound, sample_rate)
            channel = music.play(sound=sound, loops=-1,  volume=g.music_volume)
            g.global_channel = channel
            self.engine.music_name="track_7.ogg"
        if(self.current_floor > 20 and self.current_floor < 30):
            room_numb = random.randint(0, 5)
            if room_numb == 0:
                self.engine.message_log.add_message("You enter another Trench", colour.descend)
                self.engine.game_map = generate_trench(max_rooms=self.max_rooms, room_min_size=self.room_min_size, room_max_size=self.room_max_size, map_width=self.map_width, map_height=self.map_height, engine=self.engine)
                self.engine.set_fov_radius(10)
                sound, sample_rate = soundfile.read("media/Music/track_3.ogg")
                sound = music.device.convert(sound, sample_rate)
                channel = music.play(sound=sound, loops=-1,  volume=g.music_volume)
                g.global_channel = channel
                self.engine.music_name="track_3.ogg"
            elif room_numb == 1:
                self.engine.message_log.add_message("You descend into the tunnels", colour.descend)
                self.engine.game_map = generate_tunnel(max_rooms=self.max_rooms, room_min_size=self.room_min_size, room_max_size=self.room_max_size, map_width=self.map_width, map_height=self.map_height, engine=self.engine)
                self.engine.set_fov_radius(10)
                self.engine.set_original_fov(20)
                sound, sample_rate = soundfile.read("media/Music/track_12.ogg")
                sound = music.device.convert(sound, sample_rate)
                channel = music.play(sound=sound, loops=-1,  volume=g.music_volume)
                g.global_channel = channel
                self.engine.music_name="track_12.ogg"
            elif room_numb == 2:
                self.engine.message_log.add_message("You enter a muddy forest", colour.descend)
                self.engine.game_map = generate_mud(max_rooms=90, room_min_size=self.room_min_size, room_max_size=self.room_max_size, map_width=self.map_width, map_height=self.map_height, engine=self.engine)
                self.engine.set_fov_radius(10)
                self.engine.set_original_fov(20)
                sound, sample_rate = soundfile.read("media/Music/track_2.ogg")
                sound = music.device.convert(sound, sample_rate)
                channel = music.play(sound=sound, loops=-1,  volume=g.music_volume)
                g.global_channel = channel
                self.engine.music_name="track_2.ogg"
            elif room_numb == 3:
                self.engine.message_log.add_message("You enter a dangerous minefield", colour.descend)
                self.engine.game_map = generate_minefield(max_rooms=self.max_rooms, room_min_size=self.room_min_size, room_max_size=self.room_max_size, map_width=self.map_width, map_height=self.map_height, engine=self.engine)
                self.engine.set_fov_radius(10)
                self.engine.set_original_fov(20)
                sound, sample_rate = soundfile.read("media/Music/track_1.ogg")
                sound = music.device.convert(sound, sample_rate)
                channel = music.play(sound=sound, loops=-1,  volume=g.music_volume)
                g.global_channel = channel
                self.engine.music_name="track_1.ogg"
            elif room_numb == 4:
                self.engine.message_log.add_message("You enter an eerie village", colour.descend)
                self.engine.game_map = generate_village(max_rooms=self.max_rooms, room_min_size=self.room_min_size, room_max_size=self.room_max_size, map_width=self.map_width, map_height=self.map_height, engine=self.engine)
                self.engine.set_fov_radius(10)
                self.engine.set_original_fov(20)
                sound, sample_rate = soundfile.read("media/Music/track_9.ogg")
                sound = music.device.convert(sound, sample_rate)
                channel = music.play(sound=sound, loops=-1,  volume=g.music_volume)
                g.global_channel = channel
                self.engine.music_name="track_9.ogg"
            else:
                self.engine.message_log.add_message("You march into a blazing village", colour.descend)
                self.engine.game_map = generate_fire(max_rooms=self.max_rooms, room_min_size=self.room_min_size, room_max_size=self.room_max_size, map_width=self.map_width, map_height=self.map_height, engine=self.engine)
                self.engine.set_fov_radius(10)
                self.engine.set_original_fov(20)
                sound, sample_rate = soundfile.read("media/Music/track_10.ogg")
                sound = music.device.convert(sound, sample_rate)
                channel = music.play(sound=sound, loops=-1,  volume=g.music_volume)
                g.global_channel = channel
                self.engine.music_name="track_10.ogg"
        elif(self.current_floor == 30):
            self.engine.message_log.add_message("You approach your final nightmare", colour.descend)
            self.engine.game_map = generate_dungeon_boss_3(max_rooms=2, room_min_size=self.room_min_size, room_max_size=self.room_max_size, map_width=80, map_height=50, engine=self.engine)
            sound, sample_rate = soundfile.read("media/Music/track_11.ogg")
            sound = music.device.convert(sound, sample_rate)
            channel = music.play(sound=sound, loops=-1,  volume=g.music_volume)
            g.global_channel = channel
            self.engine.music_name="track_11.ogg"
        elif(self.current_floor >= 31):
            self.engine.message_log.add_message("You walk through the remains", colour.descend)
            self.engine.game_map = generate_epilogue(max_rooms=self.max_rooms, room_min_size=self.room_min_size, room_max_size=self.room_max_size, map_width=self.map_width, map_height=self.map_height, engine=self.engine)
            self.engine.set_fov_radius(10)
            self.engine.set_original_fov(20)
            sound, sample_rate = soundfile.read("media/Music/track_6.ogg")
            sound = music.device.convert(sound, sample_rate)
            channel = music.play(sound=sound, loops=-1,  volume=g.music_volume)
            g.global_channel = channel
            self.engine.music_name="track_6.ogg"