from __future__ import annotations

import os

from typing import Callable, Optional, Tuple, TYPE_CHECKING, Union
import tcod.event

import actions

from actions import (Action, BumpAction, PickupAction, WaitAction, ShootAction, PanicAction, MouseMovementAction)

from components.ai import PlayerMouseMovemnt

import colour
import exceptions

import g

import time
import soundfile
import tcod.sdl.audio

import sys
from os import path
from tcod import libtcodpy


if getattr(sys, 'frozen', False):
    game_dir = path.dirname(sys.executable)
else:
    game_dir=path.dirname(__file__)

if TYPE_CHECKING:
    from engine import Engine
    from entity import Item, Actor

MOVE_KEYS = {
    #arrows
    tcod.event.KeySym.UP: (0, -1),
    tcod.event.KeySym.DOWN: (0, 1),
    tcod.event.KeySym.LEFT: (-1, 0),
    tcod.event.KeySym.RIGHT: (1, 0),
    tcod.event.KeySym.HOME: (-1, -1),
    tcod.event.KeySym.END: (-1, 1),
    tcod.event.KeySym.PAGEUP: (1, -1),
    tcod.event.KeySym.PAGEDOWN: (1, 1),
    #numpad
    tcod.event.KeySym.KP_1: (-1, 1),
    tcod.event.KeySym.KP_2: (0, 1),
    tcod.event.KeySym.KP_3: (1, 1),
    tcod.event.KeySym.KP_4: (-1, 0),
    tcod.event.KeySym.KP_6: (1, 0),
    tcod.event.KeySym.KP_7: (-1, -1),
    tcod.event.KeySym.KP_8: (0, -1),
    tcod.event.KeySym.KP_9: (1, -1),
    #vi keys
    tcod.event.KeySym.h: (-1, 0),
    tcod.event.KeySym.j: (0, 1),
    tcod.event.KeySym.k: (0, -1),
    tcod.event.KeySym.l: (1, 0),
    tcod.event.KeySym.y: (-1, -1),
    tcod.event.KeySym.u: (1, -1),
    tcod.event.KeySym.b: (-1, 1),
    tcod.event.KeySym.n: (1, 1),

}

WAIT_KEYS = {
    tcod.event.KeySym.PERIOD,
    tcod.event.KeySym.KP_5,
    tcod.event.KeySym.CLEAR,
}

CONFIRM_KEYS = {
    tcod.event.KeySym.RETURN,
    tcod.event.KeySym.KP_ENTER,
}

ActionOrHandler = Union[Action, "BaseEventHandler"]

class BaseEventHandler(tcod.event.EventDispatch[ActionOrHandler]):
    def handle_events(self, event: tcod.event.Event) -> BaseEventHandler:
        state = self.dispatch(event)
        if isinstance(state, BaseEventHandler):
            return state
        assert not isinstance(state, Action), f"{self!r} can not handle actions."
        return self

    def on_render(self, console: tcod.Console) -> None:
        raise NotImplementedError()

    def ev_quit(self, event: tcod.event.Quit) -> Optional[Action]:
        raise SystemExit()

class PopupMessage(BaseEventHandler):

    def __init__(self, parent_handler: BaseEventHandler, text: str):
        self.parent = parent_handler
        self.text = text

    def on_render(self, console: tcod.Console) -> None:
        self.parent.on_render(console)
        console.rgb["fg"] //= 8
        console.rgb["bg"] //= 8
        console.print(console.width // 2, console.height // 2, self.text, fg=colour.white, bg=colour.black, alignment=libtcodpy.CENTER)

    def ev_keydown(self, event: tcod.event.KeyDown) -> Optional[BaseEventHandler]:
        return self.parent


class EventHandler(BaseEventHandler):
    def __init__(self, engine: Engine):
        self.engine = engine

    def handle_events(self, event: tcod.event.Event) -> BaseEventHandler:
        action_or_state = self.dispatch(event)
        if isinstance(action_or_state, BaseEventHandler):
            return action_or_state
        if self.handle_action(action_or_state):
            if not self.engine.player.is_alive:
                return GameOverEventHandler(self.engine)
            elif self.engine.player.level.requires_level_up:
                mixer = g.mixer
                sound, sample_rate = soundfile.read("media/Audio/level_up.ogg")
                sound = mixer.device.convert(sound, sample_rate)
                channel = mixer.play(sound=sound, volume=g.mixer_volume)
                return LevelUpEventHandler(self.engine)
            return MainGameEventHandler(self.engine)
        return self

    def handle_action(self, action: Optional[Action]) -> bool:

        if action is None:
            return False

        try:
            action.perform()
        except exceptions.Impossible as exc:
            self.engine.message_log.add_message(exc.args[0], colour.impossible)
            return False

        self.engine.handle_enemy_turns()

        self.engine.handle_gas_turns()

        self.engine.update_fov()
        
        self.engine.handle_entity_toxic_turns()

        return True
    
    def ev_mousemotion(self, event: tcod.event.MouseMotion) -> None:
        if self.engine.game_map.in_bounds(event.tile.x, event.tile.y):
            self.engine.mouse_location = event.tile.x, event.tile.y

    

    def on_render(self, console: tcod.Console) -> None:
        self.engine.render(console)


class MainGameEventHandler(EventHandler):
    def __init__(self, engine: Engine):
        self.engine = engine

    def ev_keydown(self, event: tcod.event.KeyDown) -> Optional[ActionOrHandler]:
        action: Optional[Action] = None
        #get key
        key = event.sym
        modifier = event.mod

        player = self.engine.player
        itemList: List[Item] = []

        if self.engine.story_shown == False:
            self.engine.story_shown = True
            return None

        if self.engine.game_world.current_floor == 30 and self.engine.game_completed:
            g.music.close()
            mixer = g.mixer
            sound, sample_rate = soundfile.read("media/Audio/exit_level.ogg")
            sound = mixer.device.convert(sound, sample_rate)
            channel = mixer.play(sound=sound, volume=g.mixer_volume) 
            self.engine.game_world.generate_floor()
            #self.engine.message_log.add_message("You moved to another location", colour.descend)

        if key == tcod.event.KeySym.PERIOD and modifier & (tcod.event.KMOD_LSHIFT | tcod.event.KMOD_RSHIFT):
            if self.engine.game_world.current_floor <= 30:
                return actions.ChangeRoomAction(player)
            else:
                self.engine.save_as(self.engine.player.name+".sav")
                g.music.close()
                g.music = tcod.sdl.audio.BasicMixer(tcod.sdl.audio.open())
                music = g.music
                sound, sample_rate = soundfile.read("media/Music/track_6.ogg")
                sound = music.device.convert(sound, sample_rate)
                channel = music.play(sound=sound, loops=-1,  volume=g.music_volume)
                g.MainMenu.menu_mode = 0
                return g.MainMenu

        #handle movement
        if key in MOVE_KEYS:
            dx, dy = MOVE_KEYS[key]
            action = BumpAction(player, dx, dy)
        elif key in WAIT_KEYS:
            action = WaitAction(player)
        elif key == tcod.event.KeySym.ESCAPE:
            mixer = g.mixer
            sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
            sound = mixer.device.convert(sound, sample_rate)
            channel = mixer.play(sound=sound, volume=g.mixer_volume)
            return MenuEventHandler(self.engine)
            #raise SystemExit()
        elif key == tcod.event.KeySym.v:
            mixer = g.mixer
            sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
            sound = mixer.device.convert(sound, sample_rate)
            channel = mixer.play(sound=sound, volume=g.mixer_volume)
            return HistoryViewer(self.engine)
        elif key == tcod.event.KeySym.g:
            actor_location_x = player.x
            actor_location_y = player.y
            inventory = player.inventory
            for item in self.engine.game_map.items:
                if actor_location_x == item.x and actor_location_y ==  item.y:
                    itemList.append(item)
            if len(itemList) == 1:
                return PickupAction(self.engine.player, itemList[0])
            if len(itemList) > 1:
                mixer = g.mixer
                sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
                sound = mixer.device.convert(sound, sample_rate)
                channel = mixer.play(sound=sound, volume=g.mixer_volume)
                action = SelectItemEventHandler(self.engine, itemList)
        elif key == tcod.event.KeySym.i:
            mixer = g.mixer
            sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
            sound = mixer.device.convert(sound, sample_rate)
            channel = mixer.play(sound=sound, volume=g.mixer_volume)
            return InventoryActivateHandler(self.engine)
        elif key == tcod.event.KeySym.d:
            mixer = g.mixer
            sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
            sound = mixer.device.convert(sound, sample_rate)
            channel = mixer.play(sound=sound, volume=g.mixer_volume)
            return InventoryDropHandler(self.engine)
        elif key == tcod.event.KeySym.c:
            mixer = g.mixer
            sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
            sound = mixer.device.convert(sound, sample_rate)
            channel = mixer.play(sound=sound, volume=g.mixer_volume)
            return CharacterScreenEventHandler(self.engine)
        elif key == tcod.event.KeySym.SLASH:
            return LookHandler(self.engine)
        elif key == tcod.event.KeySym.f:
            if(self.engine.player.equipment.ranged_equiped() == False):
                self.engine.message_log.add_message("No ranged weapon is equipped", colour.impossible)
            elif(self.engine.player.equipment.current_ammo() <= 0):
                self.engine.message_log.add_message("Not enough ammo", colour.impossible)
            else:
                mixer = g.mixer
                sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
                sound = mixer.device.convert(sound, sample_rate)
                channel = mixer.play(sound=sound, volume=g.mixer_volume)
                return ShotRangedAttackHandler(self.engine, player, callback=lambda xy: actions.ShootAction(player, xy))
        elif key == tcod.event.KeySym.TAB:
            if g.isAscii == True:
                g.isAscii = False
            else:
                g.isAscii = True


        return action

    def ev_mousebuttondown(self, event: tcod.event.MouseButtonDown) -> Optional[ActionOrHandler]:
        action: Optional[Action] = None
        player = self.engine.player
        itemList: List[Item] = []
        if self.engine.story_shown == False:
            self.engine.story_shown = True
            return None
        if event.tile.y >= 45:
            if event.tile.x >= 61:
                if event.button == 1:
                    return InventoryActivateHandler(self.engine)
                if event.button == 3:
                    return InventoryDropHandler(self.engine)
            if event.tile.x >= 1 and event.tile.x <= 5:
                return CharacterScreenEventHandler(self.engine)
            if event.tile.x >= 6 and event.tile.x <= 10:
                return LookHandler(self.engine)
            if event.tile.x >= 21 and event.tile.x < 61:
                return HistoryViewer(self.engine)
        else:
            
            camera = self.engine.game_map.get_camera()
            e_tile_x = event.tile.x + camera[0]
            e_tile_y = event.tile.y + camera[1]
            if e_tile_x == player.x and e_tile_y == player.y:
                if (player.x, player.y) == self.engine.game_map.downstairs_location:
                    if self.engine.game_world.current_floor <= 30:
                        return actions.ChangeRoomAction(player)
                    else:
                        self.engine.save_as(self.engine.player.name+".sav")
                        g.music.close()
                        g.music = tcod.sdl.audio.BasicMixer(tcod.sdl.audio.open())
                        music = g.music
                        sound, sample_rate = soundfile.read("media/Music/track_6.ogg")
                        sound = music.device.convert(sound, sample_rate)
                        channel = music.play(sound=sound, loops=-1,  volume=g.music_volume)
                        g.MainMenu.menu_mode = 0
                        return g.MainMenu
                else:
                    actor_location_x = player.x
                    actor_location_y = player.y
                    inventory = player.inventory
                    for item in self.engine.game_map.items:
                        if actor_location_x == item.x and actor_location_y ==  item.y:
                            itemList.append(item)
                    if len(itemList) == 1:
                        return PickupAction(player, itemList[0])
                    if len(itemList) > 1:
                        return SelectItemEventHandler(self.engine, itemList)
            
            else:
                if event.button == 1:
                    return mouseMoveHandler(self.engine, player, callback=lambda xy: MouseMovementAction(player, xy[0], xy[1]))
                if event.button == 3:
                    if(player.equipment.ranged_equiped() == False):
                        self.engine.message_log.add_message("No ranged weapon is equipped", colour.impossible)
                    elif(player.equipment.current_ammo() <= 0):
                        self.engine.message_log.add_message("Not enough ammo", colour.impossible)
                    else:
                        return ShotRangedAttackHandler(self.engine, player, callback=lambda xy: actions.ShootAction(player, xy))
        return action
    def ev_mousemotion(self, event: tcod.event.MouseMotion) -> None:
        if event.tile.y >= 45 and event.tile.x >= 61:
            self.engine.hover_set(True, 1)
        else:
            self.engine.hover_set(False, 1)

        if event.tile.y == 48: 
            if event.tile.x >= 1 and event.tile.x <= 5:
                self.engine.hover_set(True, 2)
            else:
                self.engine.hover_set(False, 2)
        
            if event.tile.x >= 6 and event.tile.x <= 10:
                self.engine.hover_set(True, 3)
            else:
                self.engine.hover_set(False, 3)
    def ev_controlleraxismotion(event: ControllerAxis) -> None:
        print(f"event: {event}")
class GameOverEventHandler(EventHandler):
    def on_quit(self) -> Optional[ActionOrHandler]:
        if os.path.exists(path.join(game_dir, "saves/"+self.engine.player.name+".sav")):
            os.remove(path.join(game_dir, "saves/"+self.engine.player.name+".sav"))
        raise exceptions.QuitWithoutSaving()

    def on_menu(self) -> Optional[ActionOrHandler]:
        if os.path.exists(path.join(game_dir, "saves/"+self.engine.player.name+".sav")):
            os.remove(path.join(game_dir, "saves/"+self.engine.player.name+".sav"))
        self.engine.quit_called = True
        g.music.close()
        g.music = tcod.sdl.audio.BasicMixer(tcod.sdl.audio.open())
        music = g.music
        sound, sample_rate = soundfile.read("media/Music/track_6.ogg")
        sound = music.device.convert(sound, sample_rate)
        channel = music.play(sound=sound, loops=-1,  volume=g.music_volume)
        g.MainMenu.menu_mode = 0
        return g.MainMenu

    def ev_quit(self, event: tcod.event.Quit) -> Optional[ActionOrHandler]:
        return self.on_quit()

    def ev_keydown(self, event: tcod.event.KeyDown) -> Optional[ActionOrHandler]:
        if event.sym == tcod.event.KeySym.ESCAPE:
            return self.on_menu()

CURSOR_Y_KEYS = {
    tcod.event.KeySym.UP: -1,
    tcod.event.KeySym.DOWN: 1,
    tcod.event.KeySym.PAGEUP: -10,
    tcod.event.KeySym.PAGEDOWN: 10,
}

class HistoryViewer(EventHandler):
    def __init__(self, engine: Engine):
        super().__init__(engine)
        self.log_length = len(engine.message_log.messages)
        self.cursor = self.log_length - 1

    def on_render(self, console: tcod.Console) -> None:
        super().on_render(console)

        log_console = tcod.console.Console(console.width - 6, console.height - 6)

        log_console.draw_frame(0, 0, log_console.width, log_console.height)
        log_console.print_box(0, 0, log_console.width, 1, "Message History", alignment=libtcodpy.CENTER)

        self.engine.message_log.render_messages(log_console, 1, 1, log_console.width - 2, log_console.height - 2, self.engine.message_log.messages[: self.cursor +1])
        log_console.blit(console, 3, 3)

    def ev_keydown(self, event: tcod.event.KeyDown) -> Optional[MainGameEventHandler]:
        if event.sym in CURSOR_Y_KEYS:
            mixer = g.mixer
            sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
            sound = mixer.device.convert(sound, sample_rate)
            channel = mixer.play(sound=sound, volume=g.mixer_volume)
            adjust = CURSOR_Y_KEYS[event.sym]
            if adjust < 0 and self.cursor == 0:
                self.cursor = self.log_length - 1
            elif adjust > 0 and self.cursor == self.log_length - 1:
                self.cursor = 0
            else:
                self.cursor = max(0, min(self.cursor + adjust, self.log_length - 1))
        elif event.sym == tcod.event.KeySym.HOME:
            self.cursor = 0
        elif event.sym == tcod.event.KeySym.END:
            self.cursor = self.log_length - 1
        else:
            return MainGameEventHandler(self.engine)
        return None
    def ev_mousebuttondown(self, event: tcod.event.MouseButtonDown) -> Optional[ActionOrHandler]:
        return MainGameEventHandler(self.engine)

class AskUserEventHandler(EventHandler):
    def ev_keydown(self, event: tcod.event.KeyDown) -> Optional[ActionOrHandler]:
        if event.sym in {tcod.event.KeySym.LSHIFT, tcod.event.KeySym.RSHIFT, tcod.event.KeySym.LCTRL, tcod.event.KeySym.RCTRL, tcod.event.KeySym.LALT, tcod.event.KeySym.RALT}:
            return None

            return self.on_exit()

    def ev_mousebuttondown(self, event: tcod.event.MouseButtonDown) -> Optional[ActionOrHandler]:
        return self.on_exit()

    def on_exit(self) -> Optional[ActionOrHandler]:
        return MainGameEventHandler(self.engine)

class SelectIndexHandler(AskUserEventHandler):
    enemies_in_area = []
    selected_enemy = 0
    def _init_(self, engine: Engine):
        super().__init__(engine)
        player = self.engine.player
        self.callback = callback

    def on_render(self, console: tcod.Console) -> None:
        super().on_render(console)
        x, y = self.engine.mouse_location
        console.rgb["bg"][x, y] = colour.white
        console.rgb["fg"][x, y] = colour.black

    def ev_keydown(self, event: tcod.event.KeyDown) -> Optional[ActionOrHandler]:
        key = event.sym
        camera = self.engine.game_map.get_camera()
        if len(self.enemies_in_area) > 0:
            if key == tcod.event.KeySym.z:
                self.selected_enemy = self.selected_enemy - 1
                if self.selected_enemy < 0:
                    self.selected_enemy = len(self.enemies_in_area) -1
                self.engine.mouse_location = self.enemies_in_area[self.selected_enemy].x - camera[0], self.enemies_in_area[self.selected_enemy].y - camera[1]
                return None
            if key == tcod.event.KeySym.x:
                self.selected_enemy = self.selected_enemy + 1
                if self.selected_enemy >= len(self.enemies_in_area):
                    self.selected_enemy = 0
                self.engine.mouse_location = self.enemies_in_area[self.selected_enemy].x - camera[0], self.enemies_in_area[self.selected_enemy].y - camera[1]
                return None
        if key == tcod.event.KeySym.ESCAPE:
            return self.on_exit()
        if key in MOVE_KEYS:
            mixer = g.mixer
            sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
            sound = mixer.device.convert(sound, sample_rate)
            channel = mixer.play(sound=sound, volume=g.mixer_volume)
            modifier = 1
            if event.mod & (tcod.event.KMOD_LSHIFT | tcod.event.KMOD_RSHIFT):
                modifier *= 5
            if event.mod & (tcod.event.KMOD_LCTRL | tcod.event.KMOD_RCTRL):
                modifier *= 10
            if event.mod & (tcod.event.KMOD_LALT | tcod.event.KMOD_RALT):
                modifier *= 20

            x, y = self.engine.mouse_location
            dx, dy = MOVE_KEYS[key]
            x += dx * modifier
            y += dy * modifier

            x = max(0, min(x, self.engine.game_map.width - 1))
            y = max(0, min(y, self.engine.game_map.height - 1))
            self.engine.mouse_location = x, y
            return None

        elif key in CONFIRM_KEYS:
            mixer = g.mixer
            sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
            sound = mixer.device.convert(sound, sample_rate)
            channel = mixer.play(sound=sound, volume=g.mixer_volume)
            camera = self.engine.game_map.get_camera()
            x = self.engine.mouse_location[0] + camera[0]
            y = self.engine.mouse_location[1] + camera[1]
            return self.on_index_selected(x,y)
        return super().ev_keydown(event)

    def ev_mousebuttondown(self, event: tcod.event.MouseButtonDown) -> Optional[ActionOrHandler]:
        if self.engine.game_map.in_bounds(*event.tile):
            if event.button == 1:
                mixer = g.mixer
                sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
                sound = mixer.device.convert(sound, sample_rate)
                channel = mixer.play(sound=sound, volume=g.mixer_volume)
                camera = self.engine.game_map.get_camera()
                x = event.tile.x + camera[0]
                y = event.tile.y + camera[1]
                return self.on_index_selected(x,y)
        return super().ev_mousebuttondown(event)

    def on_index_selected(self, x: int, y: int) -> Optional[ActionOrHandler]:
        raise NotImplementedError()

class LookHandler(SelectIndexHandler):
    def __init__(self, engine: Engine):
        super().__init__(engine)
        player = self.engine.player
        self.enemies_in_area = []
        camera = self.engine.game_map.get_camera()
        for entity in self.engine.game_map.entities:
            if self.engine.game_map.visible[entity.x, entity.y]:
                self.enemies_in_area.append(entity)
        if len(self.enemies_in_area) == 0:
            self.engine.mouse_location = player.x - camera[0], player.y - camera[1]
        else:
            self.enemies_in_area.sort(key=lambda x: player.distance(x.x, x.y))
            self.engine.mouse_location = self.enemies_in_area[0].x - camera[0], self.enemies_in_area[0].y - camera[1]

    def on_index_selected(self, x: int, y: int) -> MainGameEventHandler:
        self.engine.message_log.add_message(f"{self.engine.game_map.get_entity_name_at_location(x, y)}")
        return MainGameEventHandler(self.engine)

class SingleRangedAttackHandler(SelectIndexHandler):
    def __init__(self, engine: Engine, callback: Callable[[Tuple[int, int], Optional[Action]]]):
        super().__init__(engine)
        player = self.engine.player
        self.callback = callback
        self.enemies_in_area = []
        camera = self.engine.game_map.get_camera()
        for actor in self.engine.game_map.actors:
            if actor is not player and self.engine.game_map.visible[actor.x, actor.y] and actor.is_alive:
                self.enemies_in_area.append(actor)
        if len(self.enemies_in_area) == 0:
            self.engine.mouse_location = player.x - camera[0], player.y - camera[1]
        else:
            self.enemies_in_area.sort(key=lambda x: player.distance(x.x, x.y))
            self.engine.mouse_location = self.enemies_in_area[0].x - camera[0], self.enemies_in_area[0].y - camera[1]

    def on_index_selected(self, x: int, y: int) -> Optional[Action]:
        return self.callback((x, y))


class ShotRangedAttackHandler(SelectIndexHandler):
    def __init__(self, engine: Engine, player: entity, callback: Callable[[Tuple[int, int], Optional[Action]]]):
        super().__init__(engine)
        self.player = player
        self.callback = callback
        self.enemies_in_area = []
        camera = self.engine.game_map.get_camera()
        for actor in self.engine.game_map.actors:
            if actor is not player and self.engine.game_map.visible[actor.x, actor.y] and actor.is_alive:
                self.enemies_in_area.append(actor)
        if len(self.enemies_in_area) == 0:
            self.engine.mouse_location = player.x - camera[0], player.y - camera[1]
        else:
            self.enemies_in_area.sort(key=lambda x: player.distance(x.x, x.y))
            self.engine.mouse_location = self.enemies_in_area[0].x - camera[0], self.enemies_in_area[0].y - camera[1]

        
        


    def on_index_selected(self, x: int, y: int) -> Optional[Action]:
        return self.callback((x, y))

class AreaRangedAttackHandler(SelectIndexHandler):
    def __init__(self, engine: Engine, radius: int, callback: Callable[[Tuple[int, int]], Optional[Action]]):
        super().__init__(engine)
        self.radius = radius
        player = self.engine.player
        self.callback = callback
        self.enemies_in_area = []
        camera = self.engine.game_map.get_camera()
        for actor in self.engine.game_map.actors:
            if actor is not player and self.engine.game_map.visible[actor.x, actor.y] and actor.is_alive:
                self.enemies_in_area.append(actor)
        if len(self.enemies_in_area) == 0:
            self.engine.mouse_location = player.x - camera[0], player.y - camera[1]
        else:
            self.enemies_in_area.sort(key=lambda x: player.distance(x.x, x.y))
            self.engine.mouse_location = self.enemies_in_area[0].x - camera[0], self.enemies_in_area[0].y - camera[1]

    def on_render(self, console: tcod.Console) -> None:
        super().on_render(console)

        x, y = self.engine.mouse_location

        console.draw_frame(x=x - self.radius - 1, y=y - self.radius - 1, width=self.radius ** 2, height=self.radius ** 2, fg=colour.red, clear=False)

    def on_index_selected(self, x: int, y: int) -> Optional[Action]:
        return self.callback((x, y))

class mouseMoveHandler(SelectIndexHandler):
    def __init__(self, engine: Engine, player: entity, callback: Callable[[Tuple[int, int], Optional[Action]]]):
        super().__init__(engine)
        self.player = player
        self.callback = callback

    def on_index_selected(self, x: int, y: int) -> MouseMovementAction:
        return self.callback((x, y))

class CharacterScreenEventHandler(AskUserEventHandler):
    TITLE = "Character Information"

    def on_render(self, console: tcod.Console) -> None:
        super().on_render(console)

        
        x = 0

        y = 0

        width = 80

        console.draw_frame(x=x, y=y, width=width, height=9, title=self.TITLE, clear=True, fg=(255, 255, 255), bg=(0, 0, 0))

        console.print(x=x + 1, y=y + 1, string=f"Level: {self.engine.player.level.current_level}")

        console.print(x=x + 1, y=y + 2, string=f"XP: {self.engine.player.level.current_xp}/{self.engine.player.level.experience_to_next_level}")

        console.print(x=x + 1, y=y + 3, string=f"Strength: {self.engine.player.soldier.baseStrength}")

        console.print(x=x + 1, y=y + 4, string=f"Dexterity: {self.engine.player.soldier.baseDexterity}")

        console.print(x=x + 1, y=y + 5, string=f"Composure: {self.engine.player.soldier.composure}")

        console.print(x=x + 1, y=y + 6, string=f"Vitality: {self.engine.player.soldier.vitality}")

    def ev_keydown(self, event: tcod.event.KeyDown) -> Optional[ActionOrHandler]:
        player = self.engine.player
        key = event.sym
        index = key - tcod.event.KeySym.a
        mixer = g.mixer
        sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
        sound = mixer.device.convert(sound, sample_rate)
        channel = mixer.play(sound=sound, volume=g.mixer_volume)
        
        if event.sym == tcod.event.KeySym.ESCAPE:
            return self.on_exit()

class LevelUpEventHandler(AskUserEventHandler):
    TITLE = "Level Up"
    selected_item = 0
    def on_render(self, console: tcod.Console) -> None:
        super().on_render(console)

        
        x = 0

        console.draw_frame(x=x, y=0, width=80, height=9, title=self.TITLE, clear=True, fg=(255, 255, 255), bg=(0, 0, 0))

        console.print(x=x + 1, y=1, string="You just leveled up!")

        console.print(x=x + 1, y=2, string="Select an attribute to increase.")

        if self.selected_item > 0 and self.selected_item <= 4:
            console.draw_rect(x=x+1, y=3+self.selected_item, width=40, height=1, ch=0, fg=(0, 0, 0), bg=(0, 0, 255))

        console.print(x=x + 1, y=4, string=f"S) +1 to Str (Str: {self.engine.player.soldier.base_strength})")

        console.print(x=x + 1, y=5, string=f"D) +1 to Dex by 1 (Dex: {self.engine.player.soldier.base_dexterity})")

        console.print(x=x + 1, y=6, string=f"V) +1 to Vit (Vit: {self.engine.player.soldier.vitality})")

        console.print(x=x + 1, y=7, string=f"C) +1 to Com (Com: {self.engine.player.soldier.composure})")

        

    def ev_keydown(self, event: tcod.event.KeyDown) -> Optional[ActionOrHandler]:
        player = self.engine.player
        key = event.sym
        index = key - tcod.event.KeySym.a
        mixer = g.mixer
        sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
        sound = mixer.device.convert(sound, sample_rate)
        channel = mixer.play(sound=sound, volume=g.mixer_volume)
        
        if event.sym == tcod.event.KeySym.s:
            if player.soldier.base_strength < 10:
                player.level.increase_strength()
                return self.on_exit()
            else:
                self.engine.message_log.add_message("Stat at max level.", colour.invalid)
                return None
        elif event.sym == tcod.event.KeySym.d:
            if player.soldier.base_dexterity < 10:
                player.level.increase_dexterity()
                return self.on_exit()
            else:
                self.engine.message_log.add_message("Stat at max level.", colour.invalid)
                return None
        elif event.sym == tcod.event.KeySym.v:
            if player.soldier.base_vitality < 10:
                player.level.increase_vitality()
                return self.on_exit()
            else:
                self.engine.message_log.add_message("Stat at max level.", colour.invalid)
                return None
        elif event.sym == tcod.event.KeySym.c:
            if player.soldier.base_composure < 10:
                player.level.increase_composure()
                return self.on_exit()
            else:
                self.engine.message_log.add_message("Stat at max level.", colour.invalid)
                return None
        elif event.sym == tcod.event.KeySym.UP:
            if self.selected_item <= 0:
                self.selected_item = 4
            else:
                self.selected_item = self.selected_item - 1
        elif event.sym == tcod.event.KeySym.DOWN:
            if self.selected_item >= 4:
                self.selected_item = 0
            else:
                self.selected_item = self.selected_item + 1
        elif event.sym == tcod.event.KeySym.RETURN:
            if self.selected_item == 1:
                if player.soldier.base_strength < 10:
                    player.level.increase_strength()
                    return self.on_exit()
                else:
                    self.engine.message_log.add_message("Stat at max level.", colour.invalid)
                    return None
            elif self.selected_item == 2:
                if player.soldier.base_dexterity < 10:
                    player.level.increase_dexterity()
                    return self.on_exit()
                else:
                    self.engine.message_log.add_message("Stat at max level.", colour.invalid)
                    return None
            elif self.selected_item == 3:
                if player.soldier.base_vitality < 10:
                    player.level.increase_vitality()
                    return self.on_exit()
                else:
                    self.engine.message_log.add_message("Stat at max level.", colour.invalid)
                    return None
            elif self.selected_item == 4:
                if player.soldier.base_composure < 10:
                    player.level.increase_composure()
                    return self.on_exit()
                else:
                    self.engine.message_log.add_message("Stat at max level.", colour.invalid)
                    return None
            else:
                self.engine.message_log.add_message("Invalid entry", colour.invalid)
                return None
        else:
            self.engine.message_log.add_message("Invalid entry", colour.invalid)
            return None

    def ev_mousemotion(self, event: tcod.event.MouseMotion) -> None:
        if event.tile.y >= 4 and event.tile.y <= 7:
            if self.selected_item != event.tile.y-3:
                mixer = g.mixer
                sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
                sound = mixer.device.convert(sound, sample_rate)
                channel = mixer.play(sound=sound, volume=g.mixer_volume)
                self.selected_item = event.tile.y-3
        else:
            self.selected_item = 0
        
        

    def ev_mousebuttondown(self, event: tcod.event.MouseButtonDown) -> Optional[ActionOrHandler]:
        player = self.engine.player
        if self.selected_item > 0:
            mixer = g.mixer
            sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
            sound = mixer.device.convert(sound, sample_rate)
            channel = mixer.play(sound=sound, volume=g.mixer_volume)
        if self.selected_item == 1:
            if player.soldier.base_strength < 10:
                player.level.increase_strength()
                return self.on_exit()
            else:
                self.engine.message_log.add_message("Stat at max level.", colour.invalid)
                return None
        elif self.selected_item == 2:
            if player.soldier.base_dexterity < 10:
                player.level.increase_dexterity()
                return self.on_exit()
            else:
                self.engine.message_log.add_message("Stat at max level.", colour.invalid)
                return None
        elif self.selected_item == 3:
            if player.soldier.base_vitality < 10:
                player.level.increase_vitality()
                return self.on_exit()
            else:
                self.engine.message_log.add_message("Stat at max level.", colour.invalid)
                return None
        elif self.selected_item == 4:
            if player.soldier.base_composure < 10:
                player.level.increase_composure()
                return self.on_exit()
            else:
                self.engine.message_log.add_message("Stat at max level.", colour.invalid)
                return None
        else:
            return None

class MenuEventHandler(AskUserEventHandler):
    TITLE = "Menu"
    selected_item = 0
    def on_render(self, console: tcod.Console) -> None:
        super().on_render(console)
        graphic_settings = ""
        if g.isAscii == True:
            graphic_settings = "ASCII"
        else:
            graphic_settings = "Tiles"
        txtFScreen = ""
        window = g.context.sdl_window
        if window.fullscreen:
            txtFScreen = "On" 
        else:
            txtFScreen = "Off"
        menu_width = 24
        music_volume = int(g.music_volume * 10)
        mixer_volume = int(g.mixer_volume * 10)
        
        x = 0
        console.draw_frame(x=x, y=0, width=80, height=50, title=self.TITLE, clear=True, fg=(255, 255, 255), bg=(0, 0, 0))

        console.print(x=x + 1, y=1, string="Options:")
        for i, text in enumerate([f"[F] Fullscreen: {txtFScreen}", f"[<] Music: {music_volume} [>]", f"[-] Audio: {mixer_volume} [+]", f"[G] Graphics: {graphic_settings}"]):
            if self.selected_item == i + 1:
                console.print(console.width // 2, 1 + i, text.ljust(menu_width), fg=colour.menu_text, bg=colour.selected_item, alignment=libtcodpy.CENTER, bg_blend=libtcodpy.BKGND_ALPHA(64))
            else:
                console.print(console.width // 2, 1 + i, text.ljust(menu_width), fg=colour.menu_text, bg=colour.black, alignment=libtcodpy.CENTER, bg_blend=libtcodpy.BKGND_ALPHA(64))

        if self.selected_item == 5:
            console.print(x=x + 1, y=6, string="[M]enu", bg=colour.selected_item)
        else:
            console.print(x=x + 1, y=6, string="[M]enu")
        if self.selected_item == 6:
            console.print(x=x + 1, y=7, string=f"[Q]uit", bg=colour.selected_item)
        else:
            console.print(x=x + 1, y=7, string=f"[Q]uit")
        if self.selected_item == 7:
            console.print(x=x + 1, y=8, string=f"[B]ack", bg=colour.selected_item)
        else:
            console.print(x=x + 1, y=8, string=f"[B]ack")

        console.draw_frame(x=5, y=console.height-11, width=console.width-10, height=10, title="Controls: ", clear=True, fg=(255, 255, 255), bg=(0, 0, 0))
        console.print(6, console.height-10, "Keyboard - Move: Arrowkeys, Numpad, vi keys | Shoot: F | Pickup: g |", fg=colour.menu_text, alignment=libtcodpy.LEFT)
        console.print(17, console.height-9, "Exit Level: > | View history: v | Equip: i | Drop: d |", fg=colour.menu_text, alignment=libtcodpy.LEFT)
        console.print(17, console.height-8, "Character: c | Look: / | Cycle ranged: z or v", fg=colour.menu_text, alignment=libtcodpy.LEFT)

        console.print(6, console.height-6, "Mouse - Move select: left click on map | Move: left click on tile", fg=colour.menu_text, alignment=libtcodpy.LEFT)
        console.print(14, console.height-5, "Shoot select: right click on map | Shoot: left click on tile", fg=colour.menu_text, alignment=libtcodpy.LEFT)
        console.print(14, console.height-4, "Pickup or exit: click on player when standing on item", fg=colour.menu_text, alignment=libtcodpy.LEFT)
        console.print(14, console.height-3, "Equip: left click equipment | Drop: Right Click equipment", fg=colour.menu_text, alignment=libtcodpy.LEFT)

    def ev_keydown(self, event: tcod.event.KeyDown) -> Optional[ActionOrHandler]:
        player = self.engine.player
        key = event.sym
        if event.sym == tcod.event.KeySym.f:
            mixer = g.mixer
            sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
            sound = mixer.device.convert(sound, sample_rate)
            channel = mixer.play(sound=sound, volume=g.mixer_volume)
            window = g.context.sdl_window
            window = g.context.sdl_window
            if not window:
                return
            if window.fullscreen:
                window.fullscreen = False
            else:
                window.fullscreen = tcod.sdl.video.WindowFlags.FULLSCREEN_DESKTOP
        elif event.sym == tcod.event.KeySym.PERIOD and event.mod & (tcod.event.KMOD_LSHIFT | tcod.event.KMOD_RSHIFT):
            mixer = g.mixer
            sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
            sound = mixer.device.convert(sound, sample_rate)
            channel = mixer.play(sound=sound, volume=g.mixer_volume)
            if g.music_volume < 1.0:
                g.music_volume = float(int(g.music_volume * 10) + 1) / 10
            g.global_channel.volume = g.music_volume
        elif event.sym == tcod.event.KeySym.COMMA and event.mod & (tcod.event.KMOD_LSHIFT | tcod.event.KMOD_RSHIFT):
            mixer = g.mixer
            sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
            sound = mixer.device.convert(sound, sample_rate)
            channel = mixer.play(sound=sound, volume=g.mixer_volume)
            if g.music_volume > 0.0:
                g.music_volume = float(int(g.music_volume * 10) - 1) / 10
            g.global_channel.volume = g.music_volume
        elif event.sym == tcod.event.KeySym.EQUALS and event.mod & (tcod.event.KMOD_LSHIFT | tcod.event.KMOD_RSHIFT):
            mixer = g.mixer
            sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
            sound = mixer.device.convert(sound, sample_rate)
            channel = mixer.play(sound=sound, volume=g.mixer_volume)
            if g.mixer_volume < 1.0:
                g.mixer_volume = float(int(g.mixer_volume * 10) + 1) / 10
        elif event.sym == tcod.event.KeySym.MINUS:
            mixer = g.mixer
            sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
            sound = mixer.device.convert(sound, sample_rate)
            channel = mixer.play(sound=sound, volume=g.mixer_volume)
            if g.mixer_volume > 0.0:
                g.mixer_volume = float(int(g.mixer_volume * 10) - 1) / 10
        elif event.sym == tcod.event.KeySym.g:
            mixer = g.mixer
            sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
            sound = mixer.device.convert(sound, sample_rate)
            channel = mixer.play(sound=sound, volume=g.mixer_volume)
            if g.isAscii == True:
                g.isAscii = False
            else:
                g.isAscii = True
        elif event.sym == tcod.event.KeySym.b:
            mixer = g.mixer
            sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
            sound = mixer.device.convert(sound, sample_rate)
            channel = mixer.play(sound=sound, volume=g.mixer_volume)
            window = g.context.sdl_window
            save_string = f"F:{window.fullscreen},M:{g.music_volume},A:{g.mixer_volume},G:{g.isAscii}"
            ini_file = open("settings.ini", "w")
            n = ini_file.write(save_string)
            ini_file.close()
            return self.on_exit()
        elif event.sym == tcod.event.KeySym.q:
            mixer = g.mixer
            sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
            sound = mixer.device.convert(sound, sample_rate)
            channel = mixer.play(sound=sound, volume=g.mixer_volume)
            window = g.context.sdl_window
            save_string = f"F:{window.fullscreen},M:{g.music_volume},A:{g.mixer_volume},G:{g.isAscii}"
            ini_file = open("settings.ini", "w")
            n = ini_file.write(save_string)
            ini_file.close()
            raise SystemExit()
        elif event.sym == tcod.event.KeySym.m:
            mixer = g.mixer
            sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
            sound = mixer.device.convert(sound, sample_rate)
            channel = mixer.play(sound=sound, volume=g.mixer_volume)
            window = g.context.sdl_window
            save_string = f"F:{window.fullscreen},M:{g.music_volume},A:{g.mixer_volume},G:{g.isAscii}"
            ini_file = open("settings.ini", "w")
            n = ini_file.write(save_string)
            ini_file.close()
            self.engine.save_as(self.engine.player.name+".sav")
            g.music.close()
            g.music = tcod.sdl.audio.BasicMixer(tcod.sdl.audio.open())
            music = g.music
            sound, sample_rate = soundfile.read("media/Music/track_6.ogg")
            sound = music.device.convert(sound, sample_rate)
            channel = music.play(sound=sound, loops=-1,  volume=g.music_volume)
            g.MainMenu.menu_mode = 0
            return g.MainMenu
        elif event.sym == tcod.event.KeySym.UP:
            mixer = g.mixer
            sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
            sound = mixer.device.convert(sound, sample_rate)
            channel = mixer.play(sound=sound, volume=g.mixer_volume)
            window = g.context.sdl_window
            if self.selected_item <= 0:
                self.selected_item = 7
            else:
                self.selected_item = self.selected_item - 1
        elif event.sym == tcod.event.KeySym.DOWN:
            mixer = g.mixer
            sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
            sound = mixer.device.convert(sound, sample_rate)
            channel = mixer.play(sound=sound, volume=g.mixer_volume)
            window = g.context.sdl_window
            if self.selected_item >= 7:
                self.selected_item = 0
            else:
                self.selected_item = self.selected_item + 1
        elif event.sym == tcod.event.KeySym.RETURN:
            mixer = g.mixer
            sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
            sound = mixer.device.convert(sound, sample_rate)
            channel = mixer.play(sound=sound, volume=g.mixer_volume)
            window = g.context.sdl_window
            if self.selected_item == 1:
                window = g.context.sdl_window
                if not window:
                    return
                if window.fullscreen:
                    window.fullscreen = False
                else:
                    window.fullscreen = tcod.sdl.video.WindowFlags.FULLSCREEN_DESKTOP
            if self.selected_item == 4:
                if g.isAscii == True:
                    g.isAscii = False
                else:
                    g.isAscii = True
            if self.selected_item == 5:
                mixer = g.mixer
                sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
                sound = mixer.device.convert(sound, sample_rate)
                channel = mixer.play(sound=sound, volume=g.mixer_volume)
                window = g.context.sdl_window
                save_string = f"F:{window.fullscreen},M:{g.music_volume},A:{g.mixer_volume},G:{g.isAscii}"
                ini_file = open("settings.ini", "w")
                n = ini_file.write(save_string)
                ini_file.close()
                self.engine.save_as(self.engine.player.name+".sav")
                g.music.close()
                g.music = tcod.sdl.audio.BasicMixer(tcod.sdl.audio.open())
                music = g.music
                sound, sample_rate = soundfile.read("media/Music/track_6.ogg")
                sound = music.device.convert(sound, sample_rate)
                channel = music.play(sound=sound, loops=-1,  volume=g.music_volume)
                g.MainMenu.menu_mode = 0
                return g.MainMenu
            if self.selected_item == 6:
                window = g.context.sdl_window
                save_string = f"F:{window.fullscreen},M:{g.music_volume},A:{g.mixer_volume},G:{g.isAscii}"
                ini_file = open("settings.ini", "w")
                n = ini_file.write(save_string)
                ini_file.close()
                raise SystemExit()
            if self.selected_item == 7:
                window = g.context.sdl_window
                save_string = f"F:{window.fullscreen},M:{g.music_volume},A:{g.mixer_volume},G:{g.isAscii}"
                ini_file = open("settings.ini", "w")
                n = ini_file.write(save_string)
                ini_file.close()
                return self.on_exit()
        elif event.sym == tcod.event.KeySym.LEFT:
            if self.selected_item == 2:
                mixer = g.mixer
                sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
                sound = mixer.device.convert(sound, sample_rate)
                channel = mixer.play(sound=sound, volume=g.mixer_volume)
                if g.music_volume > 0.0:
                    g.music_volume = float(int(g.music_volume * 10) - 1) / 10
                g.global_channel.volume = g.music_volume
            if self.selected_item == 3:
                mixer = g.mixer
                sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
                sound = mixer.device.convert(sound, sample_rate)
                channel = mixer.play(sound=sound, volume=g.mixer_volume)
                if g.mixer_volume > 0.0:
                    g.mixer_volume = float(int(g.mixer_volume * 10) - 1) / 10
        elif event.sym == tcod.event.KeySym.RIGHT:
            if self.selected_item == 2:
                mixer = g.mixer
                sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
                sound = mixer.device.convert(sound, sample_rate)
                channel = mixer.play(sound=sound, volume=g.mixer_volume)
                if g.music_volume < 1.0:
                    g.music_volume = float(int(g.music_volume * 10) + 1) / 10
                g.global_channel.volume = g.music_volume
            if self.selected_item == 3:
                mixer = g.mixer
                sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
                sound = mixer.device.convert(sound, sample_rate)
                channel = mixer.play(sound=sound, volume=g.mixer_volume)
                if g.mixer_volume < 1.0:
                    g.mixer_volume = float(int(g.mixer_volume * 10) + 1) / 10
    
    def ev_mousemotion(self, event: tcod.event.MouseMotion) -> None:
        if event.tile.y >= 1 and event.tile.y <= 4:
            if self.selected_item != event.tile.y:
                mixer = g.mixer
                sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
                sound = mixer.device.convert(sound, sample_rate)
                channel = mixer.play(sound=sound, volume=g.mixer_volume)
                self.selected_item = event.tile.y
        elif event.tile.y >= 6 and event.tile.y <= 8:
            if self.selected_item != event.tile.y - 1:
                mixer = g.mixer
                sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
                sound = mixer.device.convert(sound, sample_rate)
                channel = mixer.play(sound=sound, volume=g.mixer_volume)
                self.selected_item = event.tile.y - 1
        else:
            self.selected_item = 0

    def ev_mousebuttondown(self, event: tcod.event.MouseButtonDown):
        if event.tile.y >= 1 and event.tile.y <= 4 or event.tile.y >= 6 and event.tile.y <= 8:
            mixer = g.mixer
            sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
            sound = mixer.device.convert(sound, sample_rate)
            channel = mixer.play(sound=sound, volume=g.mixer_volume)
            window = g.context.sdl_window
        if self.selected_item == 1:
            window = g.context.sdl_window
            if not window:
                return
            if window.fullscreen:
                window.fullscreen = False
            else:
                window.fullscreen = tcod.sdl.video.WindowFlags.FULLSCREEN_DESKTOP
        if self.selected_item == 2:
            if event.tile.x <= 39:
                if g.music_volume > 0.0:
                    g.music_volume = float(int(g.music_volume * 10) - 1) / 10
                g.global_channel.volume = g.music_volume
            else:
                if g.music_volume < 1.0:
                    g.music_volume = float(int(g.music_volume * 10) + 1) / 10
                g.global_channel.volume = g.music_volume
        if self.selected_item == 3:
            if event.tile.x <= 39:
                if g.mixer_volume > 0.0:
                    g.mixer_volume = float(int(g.mixer_volume * 10) - 1) / 10
            else:
                if g.mixer_volume < 1.0:
                    g.mixer_volume = float(int(g.mixer_volume * 10) + 1) / 10
        if self.selected_item == 4:
            if g.isAscii == True:
                g.isAscii = False
            else:
                g.isAscii = True
        if self.selected_item == 5:
            mixer = g.mixer
            sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
            sound = mixer.device.convert(sound, sample_rate)
            channel = mixer.play(sound=sound, volume=g.mixer_volume)
            window = g.context.sdl_window
            save_string = f"F:{window.fullscreen},M:{g.music_volume},A:{g.mixer_volume},G:{g.isAscii}"
            ini_file = open("settings.ini", "w")
            n = ini_file.write(save_string)
            ini_file.close()
            self.engine.save_as(self.engine.player.name+".sav")
            g.music.close()
            g.music = tcod.sdl.audio.BasicMixer(tcod.sdl.audio.open())
            music = g.music
            sound, sample_rate = soundfile.read("media/Music/track_6.ogg")
            sound = music.device.convert(sound, sample_rate)
            channel = music.play(sound=sound, loops=-1,  volume=g.music_volume)
            g.MainMenu.menu_mode = 0
            return g.MainMenu
        if self.selected_item == 6:
            window = g.context.sdl_window
            save_string = f"F:{window.fullscreen},M:{g.music_volume},A:{g.mixer_volume},G:{g.isAscii}"
            ini_file = open("settings.ini", "w")
            n = ini_file.write(save_string)
            ini_file.close()
            raise SystemExit()
        if self.selected_item == 7:
            window = g.context.sdl_window
            save_string = f"F:{window.fullscreen},M:{g.music_volume},A:{g.mixer_volume},G:{g.isAscii}"
            ini_file = open("settings.ini", "w")
            n = ini_file.write(save_string)
            ini_file.close()
            return self.on_exit()
        

class InventoryEventHandler(AskUserEventHandler):

    TITLE = "<missing title>"
    item_order = []
    select_y = 0
    def on_render(self, console: tcod.Console) -> None:

        super().on_render(console)
        number_of_items_in_inventory = len(self.engine.player.inventory.items)

        height = 35 #number_of_items_in_inventory + 2

        if height <= 3:
            height = 3

        
        x = 0
        y = 0

        width = 80
        yoff = 0
        WepList = []
        HedList = []
        BodList = []
        MskList = []
        EtcList = []

        itemOrderStr = []

        console.draw_frame(x=x, y=y, width=width, height=height, title=self.TITLE, clear=True, fg=(255, 255, 255), bg=(0, 0, 0))

        if number_of_items_in_inventory > 0:
            for i, item in enumerate(self.engine.player.inventory.items):
                item_key = chr(ord("a") + i)

                is_equipped = self.engine.player.equipment.item_is_equipped(item)
                
                
                item_string = f"({item_key}) {item.name}"


                if item.equippable:
                    if item.equippable.is_headarmour():
                        if self.engine.player.equipment.item_in_use("HEAD") is not None and is_equipped == False:
                            currentHeadArmour = self.engine.player.equipment.item_in_use("HEAD")
                            item_string =  f"{item_string} {self.compare_armour_stats(currentHeadArmour, item)}"
                        else:
                            item_string = f"{item_string} Def_bonus: {item.equippable.def_bonus()}"
                        if is_equipped:
                            item_string = f"{item_string} (E)"
                        itemOrderStr.append(item_string)
                        HedList.append(item_string)
                    elif item.equippable.is_bodyarmour():
                        if self.engine.player.equipment.item_in_use("BODY") is not None and is_equipped == False:
                            currentBodyArmour = self.engine.player.equipment.item_in_use("BODY")
                            item_string =  f"{item_string} {self.compare_armour_stats(currentBodyArmour, item)}"
                        else:
                            item_string = f"{item_string} Def_bonus: {item.equippable.def_bonus()} Dex_bonus: {item.equippable.dex_bonus()}"
                        if is_equipped:
                            item_string = f"{item_string} (E)"
                        itemOrderStr.append(item_string)
                        BodList.append(item_string)
                    elif item.equippable.is_weapon():
                        if self.engine.player.equipment.item_in_use("WEAPON") is not None and is_equipped == False:
                            currentWeapon = self.engine.player.equipment.item_in_use("WEAPON")
                            item_string =  f"{item_string} {self.compare_weapon_stats(currentWeapon, item)}"
                        else:
                            item_string = f"{item_string} Str_bonus: {item.equippable.str_bonus()} Aim_bonus: {item.equippable.am_bonus()} ShotDmg: {item.equippable.shot_dam()}"
                        if item.equippable.max_ammo > 0:
                            item_string = f"{item_string} ammo: {item.equippable.current_ammo}/{item.equippable.max_ammo}"
                        if is_equipped:
                            item_string = f"{item_string} (E)"
                        itemOrderStr.append(item_string)
                        WepList.append(item_string)
                    elif item.equippable.is_mask():
                        item_string = f"{item_string} Dex_bonus: {item.equippable.dex_bonus()}"
                        if is_equipped:
                            item_string = f"{item_string} (E)"
                        itemOrderStr.append(item_string)
                        MskList.append(item_string)
                    else:
                        itemOrderStr.append(item_string)
                        EtcList.append(item_string)
                else:
                    itemOrderStr.append(item_string)
                    EtcList.append(item_string)
                #console.print(x + 1, y + i + 1, item_string)
            if len(HedList) > 0:
                console.print_box(x=x, y=y + yoff + 1, width=width, height=1, string="HEAD", fg=(0, 0, 0), bg=(255, 255, 255), alignment=libtcodpy.CENTER)
                yoff = yoff + 1
                for i in HedList:
                    YInt = y + yoff + 1
                    itemInt = itemOrderStr.index(i)
                    self.item_order.append([itemInt, YInt])

                    console.print(x + 1, y + yoff + 1, i)
                    yoff = yoff + 1
            if len(BodList) > 0:
                console.print_box(x=x, y=y + yoff + 1, width=width, height=1, string="BODY", fg=(0, 0, 0), bg=(255, 255, 255), alignment=libtcodpy.CENTER)
                yoff = yoff + 1
                for i in BodList:
                    YInt = y + yoff + 1
                    itemInt = itemOrderStr.index(i)
                    self.item_order.append([itemInt, YInt])
                    
                    console.print(x + 1, y + yoff + 1, i)
                    yoff = yoff + 1
            if len(WepList) > 0:
                console.print_box(x=x, y=y + yoff + 1, width=width, height=1, string="WEAPON", fg=(0, 0, 0), bg=(255, 255, 255), alignment=libtcodpy.CENTER)
                yoff = yoff + 1
                for i in WepList:
                    YInt = y + yoff + 1
                    itemInt = itemOrderStr.index(i)
                    self.item_order.append([itemInt, YInt])

                    console.print(x + 1, y + yoff + 1, i)
                    yoff = yoff + 1
            if len(MskList) > 0:
                console.print_box(x=x, y=y + yoff + 1, width=width, height=1, string="MASK", fg=(0, 0, 0), bg=(255, 255, 255), alignment=libtcodpy.CENTER)
                yoff = yoff + 1
                for i in MskList:
                    YInt = y + yoff + 1
                    itemInt = itemOrderStr.index(i)
                    self.item_order.append([itemInt, YInt])

                    console.print(x + 1, y + yoff + 1, i)
                    yoff = yoff + 1
            if len(EtcList) > 0:
                console.print_box(x=x, y=y + yoff + 1, width=width, height=1, string="Misc", fg=(0, 0, 0), bg=(255, 255, 255), alignment=libtcodpy.CENTER)
                yoff = yoff + 1
                for i in EtcList:
                    YInt = y + yoff + 1
                    itemInt = itemOrderStr.index(i)
                    self.item_order.append([itemInt, YInt])

                    console.print(x + 1, y + yoff + 1, i)
                    yoff = yoff + 1

        else:
            console.print(x + 1, y + 1, "(Empty)")

        for i, j in self.item_order:
            if self.select_y == j:
                console.print(x=75, y=self.select_y, string="<-")
            else:
                console.print(x=75, y=j, string="  ")
            

    def ev_keydown(self, event: tcod.event.KeyDown) -> Optional[ActionOrHandler]:
        player = self.engine.player
        prevItem = self.select_y
        if event.sym == tcod.event.KeySym.ESCAPE:
            return self.on_exit()
        if event.sym == tcod.event.KeySym.DOWN or event.sym == tcod.event.KeySym.KP_2:
            mixer = g.mixer
            sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
            sound = mixer.device.convert(sound, sample_rate)
            channel = mixer.play(sound=sound, volume=g.mixer_volume)
            for i, j in self.item_order:
                if prevItem == 0:
                    self.select_y = j
                    break
                elif prevItem < j:
                    self.select_y = j
                    break
                else:
                    self.select_y = 0
        elif event.sym == tcod.event.KeySym.UP or event.sym == tcod.event.KeySym.KP_8:
            mixer = g.mixer
            sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
            sound = mixer.device.convert(sound, sample_rate)
            channel = mixer.play(sound=sound, volume=g.mixer_volume)
            for i, j in reversed(self.item_order):
                if prevItem == 0:
                    self.select_y = j
                    break
                elif prevItem > j:
                    self.select_y = j
                    break
                else:
                    self.select_y = 0
        elif event.sym == tcod.event.KeySym.KP_ENTER or event.sym == tcod.event.KeySym.RETURN:
            if self.select_y != 0:
                index = 0
                for i, j in self.item_order:
                    if j == self.select_y:
                        index = i

                try:
                    selected_item = player.inventory.items[index]
                except IndexError:
                    self.engine.message_log.add_message("Invalid entry.", colour.invalid)
                    return None
                return self.on_item_selected(selected_item)
        else:
            
            key = event.sym
            index = key - tcod.event.KeySym.a

            if 0 <= index <= 26:
                try:
                    selected_item = player.inventory.items[index]
                except IndexError:
                    self.engine.message_log.add_message("Invalid entry.", colour.invalid)
                    return None
                return self.on_item_selected(selected_item)
            return super().ev_keydown(event)

    def ev_mousebuttondown(self, event: tcod.event.MouseButtonDown) -> Optional[input_handlers.BaseEventHandler]: 
        player = self.engine.player
        if self.select_y != 0: 
            index = 0
            for i, j in self.item_order:
                if j == self.select_y:
                    index = i

            try:
                selected_item = player.inventory.items[index]
            except IndexError:
                self.engine.message_log.add_message("Invalid entry.", colour.invalid)
                return None
            return self.on_item_selected(selected_item)
        else:
            return self.on_exit()

    def ev_mousemotion(self, event: tcod.event.MouseMotion) -> None:
        for i, j in self.item_order:
            if event.tile.y == j:
                if self.select_y != j:
                    mixer = g.mixer
                    sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
                    sound = mixer.device.convert(sound, sample_rate)
                    channel = mixer.play(sound=sound, volume=g.mixer_volume)
                self.select_y = j
        if event.tile.y != self.select_y:
            if self.select_y != 0:
                mixer = g.mixer
                sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
                sound = mixer.device.convert(sound, sample_rate)
                channel = mixer.play(sound=sound, volume=g.mixer_volume)
            self.select_y = 0

    def on_item_selected(self, item: Item) -> Optional[ActionOrHandler]:
        raise NotImplementedError()
    def compare_item_stats(self, equippedItem: Item, itemDisplayed: Item) -> str:
        colourNeu = (255, 255, 255)
        colourStr = (0, 255, 0)
        colourWek = (255, 0, 0)

        item_string = ""

        if equippedItem.equippable.str_bonus() < itemDisplayed.equippable.str_bonus():
            item_string = f"{item_string} Str: {libtcodpy.COLCTRL_FORE_RGB:c}{colourStr[0]:c}{colourStr[1]:c}{colourStr[2]:c}{itemDisplayed.equippable.str_bonus()}{libtcodpy.COLCTRL_STOP:c}"
        if equippedItem.equippable.str_bonus() > itemDisplayed.equippable.str_bonus():
            item_string = f"{item_string} Str: {libtcodpy.COLCTRL_FORE_RGB:c}{colourWek[0]:c}{colourWek[1]:c}{colourWek[2]:c}{itemDisplayed.equippable.str_bonus()}{libtcodpy.COLCTRL_STOP:c}"
        if equippedItem.equippable.str_bonus() == itemDisplayed.equippable.str_bonus():
            item_string = f"{item_string} Str: {libtcodpy.COLCTRL_FORE_RGB:c}{colourNeu[0]:c}{colourNeu[1]:c}{colourNeu[2]:c}{itemDisplayed.equippable.str_bonus()}{libtcodpy.COLCTRL_STOP:c}"

        if equippedItem.equippable.def_bonus() < itemDisplayed.equippable.def_bonus():
            item_string = f"{item_string} Def: {libtcodpy.COLCTRL_FORE_RGB:c}{colourStr[0]:c}{colourStr[1]:c}{colourStr[2]:c}{itemDisplayed.equippable.def_bonus()}{libtcodpy.COLCTRL_STOP:c}"
        if equippedItem.equippable.def_bonus() > itemDisplayed.equippable.def_bonus():
            item_string = f"{item_string} Def: {libtcodpy.COLCTRL_FORE_RGB:c}{colourWek[0]:c}{colourWek[1]:c}{colourWek[2]:c}{itemDisplayed.equippable.def_bonus()}{libtcodpy.COLCTRL_STOP:c}"
        if equippedItem.equippable.def_bonus() == itemDisplayed.equippable.def_bonus():
            item_string = f"{item_string} Def: {libtcodpy.COLCTRL_FORE_RGB:c}{colourNeu[0]:c}{colourNeu[1]:c}{colourNeu[2]:c}{itemDisplayed.equippable.def_bonus()}{libtcodpy.COLCTRL_STOP:c}"

        if equippedItem.equippable.dex_bonus() < itemDisplayed.equippable.dex_bonus():
            item_string = f"{item_string} Dex: {libtcodpy.COLCTRL_FORE_RGB:c}{colourStr[0]:c}{colourStr[1]:c}{colourStr[2]:c}{itemDisplayed.equippable.dex_bonus()}{libtcodpy.COLCTRL_STOP:c}"
        if equippedItem.equippable.dex_bonus() > itemDisplayed.equippable.dex_bonus():
            item_string = f"{item_string} Dex: {libtcodpy.COLCTRL_FORE_RGB:c}{colourWek[0]:c}{colourWek[1]:c}{colourWek[2]:c}{itemDisplayed.equippable.dex_bonus()}{libtcodpy.COLCTRL_STOP:c}"
        if equippedItem.equippable.dex_bonus() == itemDisplayed.equippable.dex_bonus():
            item_string = f"{item_string} Dex: {libtcodpy.COLCTRL_FORE_RGB:c}{colourNeu[0]:c}{colourNeu[1]:c}{colourNeu[2]:c}{itemDisplayed.equippable.dex_bonus()}{libtcodpy.COLCTRL_STOP:c}"

        if equippedItem.equippable.vit_bonus() < itemDisplayed.equippable.vit_bonus():
            item_string = f"{item_string} Vit: {libtcodpy.COLCTRL_FORE_RGB:c}{colourStr[0]:c}{colourStr[1]:c}{colourStr[2]:c}{itemDisplayed.equippable.vit_bonus()}{libtcodpy.COLCTRL_STOP:c}"
        if equippedItem.equippable.vit_bonus() > itemDisplayed.equippable.vit_bonus():
            item_string = f"{item_string} Vit: {libtcodpy.COLCTRL_FORE_RGB:c}{colourWek[0]:c}{colourWek[1]:c}{colourWek[2]:c}{itemDisplayed.equippable.vit_bonus()}{libtcodpy.COLCTRL_STOP:c}"
        if equippedItem.equippable.vit_bonus() == itemDisplayed.equippable.vit_bonus():
            item_string = f"{item_string} Vit: {libtcodpy.COLCTRL_FORE_RGB:c}{colourNeu[0]:c}{colourNeu[1]:c}{colourNeu[2]:c}{itemDisplayed.equippable.vit_bonus()}{libtcodpy.COLCTRL_STOP:c}"

        if equippedItem.equippable.com_bonus() < itemDisplayed.equippable.com_bonus():
            item_string = f"{item_string} Com: {libtcodpy.COLCTRL_FORE_RGB:c}{colourStr[0]:c}{colourStr[1]:c}{colourStr[2]:c}{itemDisplayed.equippable.com_bonus()}{libtcodpy.COLCTRL_STOP:c}"
        if equippedItem.equippable.com_bonus() > itemDisplayed.equippable.com_bonus():
            item_string = f"{item_string} Com: {libtcodpy.COLCTRL_FORE_RGB:c}{colourWek[0]:c}{colourWek[1]:c}{colourWek[2]:c}{itemDisplayed.equippable.com_bonus()}{libtcodpy.COLCTRL_STOP:c}"
        if equippedItem.equippable.com_bonus() == itemDisplayed.equippable.com_bonus():
            item_string = f"{item_string} Com: {libtcodpy.COLCTRL_FORE_RGB:c}{colourNeu[0]:c}{colourNeu[1]:c}{colourNeu[2]:c}{itemDisplayed.equippable.com_bonus()}{libtcodpy.COLCTRL_STOP:c}"

        if equippedItem.equippable.am_bonus() < itemDisplayed.equippable.am_bonus():
            item_string = f"{item_string} Aim: {libtcodpy.COLCTRL_FORE_RGB:c}{colourStr[0]:c}{colourStr[1]:c}{colourStr[2]:c}{itemDisplayed.equippable.am_bonus()}{libtcodpy.COLCTRL_STOP:c}"
        if equippedItem.equippable.am_bonus() > itemDisplayed.equippable.am_bonus():
            item_string = f"{item_string} Aim: {libtcodpy.COLCTRL_FORE_RGB:c}{colourWek[0]:c}{colourWek[1]:c}{colourWek[2]:c}{itemDisplayed.equippable.am_bonus()}{libtcodpy.COLCTRL_STOP:c}"
        if equippedItem.equippable.am_bonus() == itemDisplayed.equippable.am_bonus():
            item_string = f"{item_string} Aim: {libtcodpy.COLCTRL_FORE_RGB:c}{colourNeu[0]:c}{colourNeu[1]:c}{colourNeu[2]:c}{itemDisplayed.equippable.am_bonus()}{libtcodpy.COLCTRL_STOP:c}"

        if equippedItem.equippable.shot_dam() < itemDisplayed.equippable.shot_dam():
            item_string = f"{item_string} ShotDmg: {libtcodpy.COLCTRL_FORE_RGB:c}{colourStr[0]:c}{colourStr[1]:c}{colourStr[2]:c}{itemDisplayed.equippable.shot_dam()}{libtcodpy.COLCTRL_STOP:c}"
        if equippedItem.equippable.shot_dam() > itemDisplayed.equippable.shot_dam():
            item_string = f"{item_string} ShotDmg: {libtcodpy.COLCTRL_FORE_RGB:c}{colourWek[0]:c}{colourWek[1]:c}{colourWek[2]:c}{itemDisplayed.equippable.shot_dam()}{libtcodpy.COLCTRL_STOP:c}"
        if equippedItem.equippable.shot_dam() == itemDisplayed.equippable.shot_dam():
            item_string = f"{item_string} ShotDmg: {libtcodpy.COLCTRL_FORE_RGB:c}{colourNeu[0]:c}{colourNeu[1]:c}{colourNeu[2]:c}{itemDisplayed.equippable.shot_dam()}{libtcodpy.COLCTRL_STOP:c}"
        
        return item_string

    def compare_armour_stats(self, equippedItem: Item, itemDisplayed: Item) -> str:
        colourNeu = (255, 255, 255)
        colourStr = (0, 255, 0)
        colourWek = (255, 0, 0)

        item_string = ""

        if equippedItem.equippable.def_bonus() < itemDisplayed.equippable.def_bonus():
            item_string = f"{item_string} Def_bonus: {libtcodpy.COLCTRL_FORE_RGB:c}{colourStr[0]:c}{colourStr[1]:c}{colourStr[2]:c}{itemDisplayed.equippable.def_bonus()}{libtcodpy.COLCTRL_STOP:c}"
        if equippedItem.equippable.def_bonus() > itemDisplayed.equippable.def_bonus():
            item_string = f"{item_string} Def_bonus: {libtcodpy.COLCTRL_FORE_RGB:c}{colourWek[0]:c}{colourWek[1]:c}{colourWek[2]:c}{itemDisplayed.equippable.def_bonus()}{libtcodpy.COLCTRL_STOP:c}"
        if equippedItem.equippable.def_bonus() == itemDisplayed.equippable.def_bonus():
            item_string = f"{item_string} Def_bonus: {libtcodpy.COLCTRL_FORE_RGB:c}{colourNeu[0]:c}{colourNeu[1]:c}{colourNeu[2]:c}{itemDisplayed.equippable.def_bonus()}{libtcodpy.COLCTRL_STOP:c}"

        if equippedItem.equippable.dex_bonus() < itemDisplayed.equippable.dex_bonus():
            item_string = f"{item_string} Dex_bonus: {libtcodpy.COLCTRL_FORE_RGB:c}{colourStr[0]:c}{colourStr[1]:c}{colourStr[2]:c}{itemDisplayed.equippable.dex_bonus()}{libtcodpy.COLCTRL_STOP:c}"
        if equippedItem.equippable.dex_bonus() > itemDisplayed.equippable.dex_bonus():
            item_string = f"{item_string} Dex_bonus: {libtcodpy.COLCTRL_FORE_RGB:c}{colourWek[0]:c}{colourWek[1]:c}{colourWek[2]:c}{itemDisplayed.equippable.dex_bonus()}{libtcodpy.COLCTRL_STOP:c}"
        if equippedItem.equippable.dex_bonus() == itemDisplayed.equippable.dex_bonus():
            item_string = f"{item_string} Dex_bonus: {libtcodpy.COLCTRL_FORE_RGB:c}{colourNeu[0]:c}{colourNeu[1]:c}{colourNeu[2]:c}{itemDisplayed.equippable.dex_bonus()}{libtcodpy.COLCTRL_STOP:c}"

        
        return item_string
    def compare_weapon_stats(self, equippedItem: Item, itemDisplayed: Item) -> str:
        colourNeu = (255, 255, 255)
        colourStr = (0, 255, 0)
        colourWek = (255, 0, 0)

        item_string = ""

        if equippedItem.equippable.str_bonus() < itemDisplayed.equippable.str_bonus():
            item_string = f"{item_string} Str_bonus: {libtcodpy.COLCTRL_FORE_RGB:c}{colourStr[0]:c}{colourStr[1]:c}{colourStr[2]:c}{itemDisplayed.equippable.str_bonus()}{libtcodpy.COLCTRL_STOP:c}"
        if equippedItem.equippable.str_bonus() > itemDisplayed.equippable.str_bonus():
            item_string = f"{item_string} Str_bonus: {libtcodpy.COLCTRL_FORE_RGB:c}{colourWek[0]:c}{colourWek[1]:c}{colourWek[2]:c}{itemDisplayed.equippable.str_bonus()}{libtcodpy.COLCTRL_STOP:c}"
        if equippedItem.equippable.str_bonus() == itemDisplayed.equippable.str_bonus():
            item_string = f"{item_string} Str_bonus: {libtcodpy.COLCTRL_FORE_RGB:c}{colourNeu[0]:c}{colourNeu[1]:c}{colourNeu[2]:c}{itemDisplayed.equippable.str_bonus()}{libtcodpy.COLCTRL_STOP:c}"

        if itemDisplayed.equippable.max_ammo > 0:
            if equippedItem.equippable.am_bonus() < itemDisplayed.equippable.am_bonus():
                item_string = f"{item_string} Aim_bonus: {libtcodpy.COLCTRL_FORE_RGB:c}{colourStr[0]:c}{colourStr[1]:c}{colourStr[2]:c}{itemDisplayed.equippable.am_bonus()}{libtcodpy.COLCTRL_STOP:c}"
            if equippedItem.equippable.am_bonus() > itemDisplayed.equippable.am_bonus():
                item_string = f"{item_string} Aim_bonus: {libtcodpy.COLCTRL_FORE_RGB:c}{colourWek[0]:c}{colourWek[1]:c}{colourWek[2]:c}{itemDisplayed.equippable.am_bonus()}{libtcodpy.COLCTRL_STOP:c}"
            if equippedItem.equippable.am_bonus() == itemDisplayed.equippable.am_bonus():
                item_string = f"{item_string} Aim_bonus: {libtcodpy.COLCTRL_FORE_RGB:c}{colourNeu[0]:c}{colourNeu[1]:c}{colourNeu[2]:c}{itemDisplayed.equippable.am_bonus()}{libtcodpy.COLCTRL_STOP:c}"

            if equippedItem.equippable.shot_dam() < itemDisplayed.equippable.shot_dam():
                item_string = f"{item_string} ShotDmg: {libtcodpy.COLCTRL_FORE_RGB:c}{colourStr[0]:c}{colourStr[1]:c}{colourStr[2]:c}{itemDisplayed.equippable.shot_dam()}{libtcodpy.COLCTRL_STOP:c}"
            if equippedItem.equippable.shot_dam() > itemDisplayed.equippable.shot_dam():
                item_string = f"{item_string} ShotDmg: {libtcodpy.COLCTRL_FORE_RGB:c}{colourWek[0]:c}{colourWek[1]:c}{colourWek[2]:c}{itemDisplayed.equippable.shot_dam()}{libtcodpy.COLCTRL_STOP:c}"
            if equippedItem.equippable.shot_dam() == itemDisplayed.equippable.shot_dam():
                item_string = f"{item_string} ShotDmg: {libtcodpy.COLCTRL_FORE_RGB:c}{colourNeu[0]:c}{colourNeu[1]:c}{colourNeu[2]:c}{itemDisplayed.equippable.shot_dam()}{libtcodpy.COLCTRL_STOP:c}"
        
        return item_string

        

class InventoryActivateHandler(InventoryEventHandler):

    TITLE = "Select an item to use"

    def on_item_selected(self, item: Item) -> Optional[ActionOrHandler]:
        if(actions.PanicAction.getPanic(self, self.engine.player)):
            return actions.PanicActionEquip(self.engine.player, item)


        if item.consumable:
            return item.consumable.get_action(self.engine.player)
        elif item.equippable:
            #needs updating for all types of items
            if item.name == "Torch":
                if self.engine.player.equipment.torch_equiped():
                    self.engine.set_fov_radius(self.engine.get_original_fov())
                else:
                    self.engine.set_fov_radius(20)
            return actions.EquipAction(self.engine.player, item)
        else:
            return None

class InventoryDropHandler(InventoryEventHandler):
    TITLE = "Select an item to drop"

    def on_item_selected(self, item: Item) -> Optional[ActionOrHandler]:
        return actions.DropItem(self.engine.player, item)

class SelectItemEventHandler(AskUserEventHandler):
    TITLE = "Select an Item to take"
    select_y = 0
    def __init__(self, engine: Engine, itemList: List[Item]):
        super().__init__(engine)
        self.itemList = itemList
    def on_render(self, console: tcod.Console) -> None:
        
        super().on_render(console)
        number_of_items = len(self.itemList)

        height = number_of_items + 2

        if height <= 3:
            height = 3

        
        x = 0

        y = 0

        width = 80

        console.draw_frame(x=x, y=y, width=width, height=height, title=self.TITLE, clear=True, fg=(255, 255, 255), bg=(0, 0, 0))

        if number_of_items > 0:
            for i, item in enumerate(self.itemList):
                item_key = chr(ord("a") + i)

                item_string = f"({item_key}) {item.name}"

                console.print(x + 1, y + i + 1, item_string)
                if self.select_y == y+i+1:
                    console.print(x=75, y=self.select_y, string="<-")
                else:
                    console.print(x=75, y=y+i+1, string="  ")


    def ev_keydown(self, event: tcod.event.KeyDown) -> Optional[ActionOrHandler]:
        player = self.engine.player
        prevItem = self.select_y
        if event.sym == tcod.event.KeySym.ESCAPE:
            return self.on_exit()
        if event.sym == tcod.event.KeySym.DOWN or event.sym == tcod.event.KeySym.KP_2:
            mixer = g.mixer
            sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
            sound = mixer.device.convert(sound, sample_rate)
            channel = mixer.play(sound=sound, volume=g.mixer_volume)
            for i, item in enumerate(self.itemList):
                if prevItem == 0:
                    self.select_y = i+1
                    break
                elif prevItem < i+1:
                    self.select_y = i+1
                    break
                else:
                    self.select_y = 0
        elif event.sym == tcod.event.KeySym.UP or event.sym == tcod.event.KeySym.KP_8:
            mixer = g.mixer
            sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
            sound = mixer.device.convert(sound, sample_rate)
            channel = mixer.play(sound=sound, volume=g.mixer_volume)
            for i, item in enumerate(self.itemList):
                if prevItem == 0:
                    self.select_y = i+1
                    break
                elif prevItem > i+1:
                    self.select_y = i+1
                    break
                else:
                    self.select_y = 0
        elif event.sym == tcod.event.KeySym.KP_ENTER or event.sym == tcod.event.KeySym.RETURN:
            if self.select_y != 0:
                index = 0
                for i, item in enumerate(self.itemList):
                    if i+1 == self.select_y:
                        index = i

                try:
                    selected_item = self.itemList[index]
                except IndexError:
                    self.engine.message_log.add_message("Invalid entry.", colour.invalid)
                    return None
                return PickupAction(self.engine.player, selected_item)
                self.itemList = []
        else:
            key = event.sym
            index = key - tcod.event.KeySym.a

            if 0 <= index <= 26:
                try:
                    selected_item = self.itemList[index]
                except IndexError:
                    self.engine.message_log.add_message("Invalid entry.", colour.invalid)
                    return None
                return PickupAction(self.engine.player, selected_item)
                self.itemList = []
            return super().ev_keydown(event)

    def ev_mousemotion(self, event: tcod.event.MouseMotion) -> None:
        for i, item in enumerate(self.itemList):
            if event.tile.y == i+1:
                if self.select_y != i+1:
                    mixer = g.mixer
                    sound, sample_rate = soundfile.read("media/Audio/menu_tick.ogg")
                    sound = mixer.device.convert(sound, sample_rate)
                    channel = mixer.play(sound=sound, volume=g.mixer_volume)
                self.select_y = i+1
        if event.tile.y != self.select_y:
            self.select_y = 0

    def ev_mousebuttondown(self, event: tcod.event.MouseButtonDown) -> Optional[input_handlers.BaseEventHandler]: 
        player = self.engine.player
        if self.select_y != 0: 
            index = 0
            for i, item in enumerate(self.itemList):
                if i+1 == self.select_y:
                    index = i

            try:
                selected_item = self.itemList[index]
            except IndexError:
                self.engine.message_log.add_message("Invalid entry.", colour.invalid)
                return None
            return PickupAction(self.engine.player, selected_item)
            self.itemList = []
        else:
            return self.on_exit()

    def on_item_selected(self, item: Item) -> Optional[ActionOrHandler]:
        raise NotImplementedError()