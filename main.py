#!/usr/bin/env python3
import traceback
import tcod
import colour

import exceptions
import input_handlers
import setup_game
import numpy

import time

import g

import sys
from os import path

if getattr(sys, 'frozen', False):
    game_dir = path.dirname(sys.executable)
else:
    game_dir=path.dirname(__file__)

def save_game(handler: input_handlers.BaseEventHandler, filename: str) -> None:
    if isinstance(handler, input_handlers.EventHandler):
        handler.engine.save_as(filename)
def ini_video():
    if path.exists(path.join(game_dir, "settings.ini")):
        try:
            ini_file=open("settings.ini","r")
            file_data = ini_file.read()
            ini_file.close()
            split_data = file_data.split(",")
            f_res = split_data[0].split(":")
            window = g.context.sdl_window
            if f_res[1] == "0":
                window.fullscreen = False
            else:
                window.fullscreen = tcod.sdl.video.WindowFlags.FULLSCREEN_DESKTOP
            g_res = split_data[3].split(":")
            if g_res[1] == "False":
                g.isAscii = False
            else:
                g.isAscii = True
        except Exception as exc:
            traceback.print_exc()

def main() -> None:
    #set screen dimensions
    screen_width = 80
    screen_height = 50
    #load tilesets
    tileset = tcod.tileset.load_tilesheet(path.join(game_dir, "media/Images/tiles.png"), 16, 16, tcod.tileset.CHARMAP_CP437)
    
    g.MainMenu = setup_game.MainMenu()
    handler: input_handlers.BaseEventHandler = setup_game.MainMenu()

    #load new console with tileset
    with tcod.context.new_terminal(screen_width, screen_height, tileset=tileset, title="Enemy Trench", vsync = True) as g.context:
        root_console = tcod.console.Console(screen_width, screen_height, order="F")
        ini_video()
        #game loop
        try:
            while True:
                root_console.clear()
                handler.on_render(console=root_console)
                g.context.present(root_console)

                try:
                    for event in tcod.event.get():
                        g.context.convert_event(event)
                        handler = handler.handle_events(event)
                except Exception:
                    traceback.print_exc()
                    if isinstance(handler, input_handlers.EventHandler):
                        handler.engine.message_log.add_message(traceback.format_exc(), colour.error)
                time.sleep(1/60)

        except exceptions.QuitWithoutSaving:
            raise
        except SystemExit:
            try:
                save_game(handler, handler.engine.player.name+".sav")
                raise
            except Exception:
                exceptions.QuitWithoutSaving
        except BaseException:
            try:
                save_game(handler, handler.engine.player.name+".sav")
                raise
            except Exception:
                exceptions.QuitWithoutSaving
            

            

            
    

if __name__ == "__main__":
    main()