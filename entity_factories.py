from components.ai import HostileEnemy, GasCloud, StationaryEnemy, MedicEnemy, ExplosivesEnemy, FlameEnemy, Fallen
from components import consumable, equippable
from components.equipment import Equipment
from components.soldier import Soldier
from components.inventory import Inventory
from components.level import Level
from entity import Actor, Item, GasGrenade

player_infantry = Actor(char="@", sprite="è", colour=(255, 255, 255), name="Player Infantry", ai_cls=HostileEnemy, equipment=Equipment(), soldier=Soldier(hp=30, base_strength=5, base_dexterity=5, base_vitality=4, base_composure=4, rifle_ammo=0), inventory=Inventory(capacity=26), level=Level(level_up_base=200))
#player_infantry = Actor(char="@", sprite="è", colour=(255, 255, 255), name="Player Infantry", ai_cls=HostileEnemy, equipment=Equipment(), soldier=Soldier(hp=30, base_strength=10, base_dexterity=10, base_vitality=10, base_composure=10, rifle_ammo=0), inventory=Inventory(capacity=26), level=Level(level_up_base=200))
player_medic = Actor(char="@", sprite="½", colour=(255, 255, 255), name="Player Medic", ai_cls=HostileEnemy, equipment=Equipment(), soldier=Soldier(hp=30, base_strength=4, base_dexterity=5, base_vitality=5, base_composure=4, rifle_ammo=0), inventory=Inventory(capacity=26), level=Level(level_up_base=200))
player_sergeant = Actor(char="@", sprite="¼", colour=(255, 255, 255), name="Player Sergeant", ai_cls=HostileEnemy, equipment=Equipment(), soldier=Soldier(hp=30, base_strength=5, base_dexterity=4, base_vitality=4, base_composure=5, rifle_ammo=0), inventory=Inventory(capacity=26), level=Level(level_up_base=200))
player_bruiser = Actor(char="@", sprite="α", colour=(255, 255, 255), name="Player Bruiser", ai_cls=HostileEnemy, equipment=Equipment(), soldier=Soldier(hp=30, base_strength=7, base_dexterity=3, base_vitality=4, base_composure=4, rifle_ammo=0), inventory=Inventory(capacity=26), level=Level(level_up_base=200))
player_officer = Actor(char="@", sprite="ß", colour=(255, 255, 255), name="Player Officer", ai_cls=HostileEnemy, equipment=Equipment(), soldier=Soldier(hp=30, base_strength=4, base_dexterity=5, base_vitality=4, base_composure=5, rifle_ammo=0), inventory=Inventory(capacity=26), level=Level(level_up_base=200))

fallen = Actor(char="%", sprite="⌐", colour=(255, 255, 255), name="Fallen", ai_cls=Fallen, equipment=Equipment(), soldier=Soldier(hp=0, base_strength=0, base_dexterity=0, base_vitality=0, base_composure=0, rifle_ammo=0), inventory=Inventory(capacity=0), level=Level(level_up_base=0))

digger = Actor(char="D", sprite="ï", colour=(64, 127, 63), name="Digger", ai_cls=HostileEnemy, equipment=Equipment(), soldier=Soldier(hp=7, base_strength=1, base_dexterity=1, base_vitality=1, base_composure=1, rifle_ammo=0), inventory=Inventory(capacity=26), level=Level(xp_given=17))
scout = Actor(char="S", sprite="î", colour=(64, 127, 63), name="Scout", ai_cls=HostileEnemy, equipment=Equipment(), soldier=Soldier(hp=7, base_strength=1, base_dexterity=5, base_vitality=1, base_composure=2, rifle_ammo=0), inventory=Inventory(capacity=26), level=Level(xp_given=23))
rifleman = Actor(char="R", sprite="ì", colour=(64, 127, 63), name="Rifleman", ai_cls=HostileEnemy, equipment=Equipment(), soldier=Soldier(hp=10, base_strength=2, base_dexterity=5, base_vitality=2, base_composure=3, rifle_ammo=6), inventory=Inventory(capacity=26), level=Level(xp_given=35))

heavy = Actor(char="H", sprite="Ä", colour=(0, 127, 0), name="Heavy", ai_cls=StationaryEnemy, equipment=Equipment(), soldier=Soldier(hp=12, base_strength=4, base_dexterity=5, base_vitality=3, base_composure=4, rifle_ammo=6), inventory=Inventory(capacity=26), level=Level(xp_given=50))
explosives = Actor(char="E", sprite="Å", colour=(64, 127, 63), name="Explosives", ai_cls=ExplosivesEnemy, equipment=Equipment(), soldier=Soldier(hp=10, base_strength=3, base_dexterity=5, base_vitality=4, base_composure=4, rifle_ammo=6), inventory=Inventory(capacity=26), level=Level(xp_given=62))
fieldMedic = Actor(char="F", sprite="É", colour=(64, 127, 63), name="FieldMedic", ai_cls=MedicEnemy, equipment=Equipment(), soldier=Soldier(hp=14, base_strength=3, base_dexterity=5, base_vitality=5, base_composure=4, rifle_ammo=0), inventory=Inventory(capacity=26), level=Level(xp_given=70))

marksman = Actor(char="M", sprite="æ", colour=(0, 127, 0), name="Marksman", ai_cls=HostileEnemy, equipment=Equipment(), soldier=Soldier(hp=16, base_strength=3, base_dexterity=6, base_vitality=4, base_composure=5, rifle_ammo=6), inventory=Inventory(capacity=26), level=Level(xp_given=84))
bruiser = Actor(char="B", sprite="Æ", colour=(64, 127, 63), name="Bruiser", ai_cls=HostileEnemy, equipment=Equipment(), soldier=Soldier(hp=16, base_strength=6, base_dexterity=3, base_vitality=5, base_composure=5, rifle_ammo=0), inventory=Inventory(capacity=26), level=Level(xp_given=100))
officer = Actor(char="O", sprite="ô", colour=(0, 127, 0), name="Officer", ai_cls=HostileEnemy, equipment=Equipment(), soldier=Soldier(hp=18, base_strength=5, base_dexterity=6, base_vitality=6, base_composure=8, rifle_ammo=6), inventory=Inventory(capacity=26), level=Level(xp_given=120))

heavy_boss = Actor(char="H", sprite="Ä", colour=(76, 0, 153), name="Heavy Boss", ai_cls=StationaryEnemy, equipment=Equipment(), soldier=Soldier(hp=20, base_strength=5, base_dexterity=6, base_vitality=6, base_composure=6, rifle_ammo=1), inventory=Inventory(capacity=26), level=Level(xp_given=300))
marksman_boss = Actor(char="M", sprite="æ", colour=(76, 0, 153), name="Marksman Boss", ai_cls=StationaryEnemy, equipment=Equipment(), soldier=Soldier(hp=24, base_strength=6, base_dexterity=8, base_vitality=8, base_composure=7, rifle_ammo=1), inventory=Inventory(capacity=26), level=Level(xp_given=500))
flamer_boss = Actor(char="W", sprite="ö", colour=(76, 0, 153), name="Flamer", ai_cls=FlameEnemy, equipment=Equipment(), soldier=Soldier(hp=30, base_strength=8, base_dexterity=8, base_vitality=10, base_composure=8, rifle_ammo=1), inventory=Inventory(capacity=26), level=Level(xp_given=0))

health_pack = Item(char="♥", sprite="ò", colour=(255, 0, 0), name="Health Pack", consumable=consumable.HealingConsumable(amount=5))

rifle_ammo_pack = Item(char=";", sprite="û", colour=(64, 255, 0), name="Rifle Ammo Pack", consumable=consumable.AmmoConsumable(amount=5, weapon="Rifle"))
pistol_ammo_pack = Item(char=";", sprite="û", colour=(0, 191, 255), name="Pistol Ammo Pack", consumable=consumable.AmmoConsumable(amount=5, weapon="Pistol"))
mg_ammo_pack = Item(char=";", sprite="û", colour=(96, 96, 96), name="MG Ammo Pack", consumable=consumable.AmmoConsumable(amount=20, weapon="MG"))

rifle_ammo_box = Item(char=":", sprite="ù", colour=(64, 255, 0), name="Rifle Ammo Box", consumable=consumable.AmmoConsumable(amount=30, weapon="Rifle"))
pistol_ammo_box = Item(char=":", sprite="ù", colour=(0, 191, 255), name="Pistol Ammo Box", consumable=consumable.AmmoConsumable(amount=20, weapon="Pistol"))
mg_ammo_box = Item(char=":", sprite="ù", colour=(96, 96, 96), name="MG Ammo Box", consumable=consumable.AmmoConsumable(amount=300, weapon="MG"))

throwing_knife = Item(char="≈", sprite="ÿ", colour=(255, 255, 0), name="Throwing Knife", consumable=consumable.KnifeThrow(damage=20, maximum_range=5))

rock = Item(char="•", sprite="Ö", colour=(207, 63, 255), name="Rock", consumable=consumable.ConfusionConsumable(number_of_turns=10))

grenade = Item(char="'", sprite="Ü", colour=(64, 127, 63), name="Grenade", consumable=consumable.GrenadeDamageConsumable(damage=12, radius=3))

mine = Item(char="¬", sprite="¢", colour=(64, 127, 63), name="Mine", consumable=consumable.MineConsumable())


spade = Item(char="/", sprite="£", colour=(138, 186, 0), name="Spade", equippable=equippable.Melee(strength_bonus=2))
knife = Item(char="~", sprite="¥", colour=(255, 0, 255), name="Knife", equippable=equippable.Melee(strength_bonus=3))
sword = Item(char="!", sprite="₧", colour=(255, 255, 255), name="Sword", equippable=equippable.Melee(strength_bonus=5))

LER = Item(char="|", sprite="ƒ", colour=(3, 130, 57), name="LE-Rifle", equippable=equippable.Rifle(shot_damage=10, current_ammo=6, max_ammo=10))
g98 = Item(char="|", sprite="ƒ", colour=(138, 86, 0), name="G98", equippable=equippable.Rifle(shot_damage=12, current_ammo=3, max_ammo=5))
LE186 = Item(char="|", sprite="ƒ", colour=(96, 96, 96), name="LE186", equippable=equippable.Rifle(shot_damage=11, current_ammo=5, max_ammo=8))
BERT = Item(char="|", sprite="ƒ", colour=(117, 112, 96), name="BERT", equippable=equippable.Rifle(shot_damage=13, current_ammo=3, max_ammo=5))
KARBA90 = Item(char="|", sprite="ƒ", colour=(147, 127, 111), name="KARBA98", equippable=equippable.Rifle(shot_damage=14, current_ammo=3, max_ammo=5))

WebRev = Item(char="r", sprite="á", colour=(3, 130, 57), name="WebRev", equippable=equippable.Pistol(shot_damage=6, current_ammo=4, max_ammo=7))
MC96  = Item(char="r", sprite="á", colour=(138, 86, 0), name="MC96", equippable=equippable.Pistol(shot_damage=4, current_ammo=6, max_ammo=10))
RevMod1892 = Item(char="r", sprite="á", colour=(96, 96, 96), name="RevMod1892", equippable=equippable.Pistol(shot_damage=8, current_ammo=3, max_ammo=6))
LangPistol = Item(char="r", sprite="á", colour=(117, 112, 96), name="LangPistol", equippable=equippable.Pistol(shot_damage=5, current_ammo=4, max_ammo=8))
SModel1908 = Item(char="r", sprite="á", colour=(147, 127, 111), name="SModel1908", equippable=equippable.Pistol(shot_damage=5, current_ammo=4, max_ammo=8))
LPistol = Item(char="r", sprite="á", colour=(80, 78, 79), name="LPistol", equippable=equippable.Pistol(shot_damage=7, current_ammo=4, max_ammo=8))

LERBayonet = Item(char="⌠", sprite="í", colour=(3, 130, 57), name="LE-Rifle w/ Bayo", equippable=equippable.RifleBayonet(shot_damage=10, current_ammo=6, max_ammo=10))
g98Bayonet = Item(char="⌠", sprite="í", colour=(138, 86, 0), name="G98 w/ Bayo", equippable=equippable.RifleBayonet(shot_damage=12, current_ammo=3, max_ammo=5))
LE186Bayonet = Item(char="⌠", sprite="í", colour=(96, 96, 96), name="LE186 w/ Bayo", equippable=equippable.RifleBayonet(shot_damage=11, current_ammo=5, max_ammo=8))
BERTBayonet = Item(char="⌠", sprite="í", colour=(117, 112, 96), name="BERT w/ Bayo", equippable=equippable.RifleBayonet(shot_damage=13, current_ammo=3, max_ammo=5))
KARBA90Bayonet = Item(char="⌠", sprite="í", colour=(147, 127, 111), name="KARBA98 w/ Bayo", equippable=equippable.RifleBayonet(shot_damage=14, current_ammo=3, max_ammo=5))

LERSniper = Item(char="⌡", sprite="ó", colour=(3, 130, 57), name="LE-Rifle w/ Scope", equippable=equippable.Sniper(shot_damage=10, current_ammo=6, max_ammo=10))
g98Sniper = Item(char="⌡", sprite="ó", colour=(138, 86, 0), name="G98 w/ Scope", equippable=equippable.Sniper(shot_damage=12, current_ammo=3, max_ammo=5))
LE186Sniper = Item(char="⌡", sprite="ó", colour=(96, 96, 96), name="LE186 w/ Scope", equippable=equippable.Sniper(shot_damage=11, current_ammo=5, max_ammo=8))
BERTSniper = Item(char="⌡", sprite="ó", colour=(117, 112, 96), name="BERT w/ Scope", equippable=equippable.Sniper(shot_damage=13, current_ammo=3, max_ammo=5))
KARBA90Sniper = Item(char="⌡", sprite="ó", colour=(147, 127, 111), name="KARBA98 w/ Scope", equippable=equippable.Sniper(shot_damage=14, current_ammo=3, max_ammo=5))

flamethrower = Item(char="t", sprite="ú", colour=(166, 165, 165), name="Flamethrower", equippable=equippable.Flamethrower(current_ammo=450, max_ammo=900))

MAG80 = Item(char="f", sprite="ñ", colour=(96, 96, 96), name="MAG-80", equippable=equippable.MachineGun(shot_damage=10, current_ammo=139, max_ammo=250))
VICR = Item(char="f", sprite="ñ", colour=(3, 130, 57), name="VICR", equippable=equippable.MachineGun(shot_damage=12, current_ammo=139, max_ammo=250))
MAG1580 = Item(char="f", sprite="ñ", colour=(147, 127, 111), name="MAG-1580", equippable=equippable.MachineGun(shot_damage=15, current_ammo=139, max_ammo=250))

gasMask = Item(char="g", sprite="Ñ", colour=(123, 178, 0), name="Gas Mask", equippable=equippable.GasMask())

helmetTypeA = Item(char="[", sprite="ª", colour=(3, 130, 57), name="Helmet Type A", equippable=equippable.helmetTypeA())

helmetMark1 = Item(char="[", sprite="ª", colour=(166, 165, 165), name="Helmet Mark 1", equippable=equippable.helmetMark1())

bulletProofVest = Item(char="]", sprite="º", colour=(3, 130, 57), name="Bullet Proof Vest", equippable=equippable.BodyArmour(defense_bonus = 1, dexterity_bonus =-1))
metalArmour = Item(char="]", sprite="º", colour=(166, 165, 165), name="Metal Armour", equippable=equippable.BodyArmour(defense_bonus = 3, dexterity_bonus =-2))

gas_canister = GasGrenade(char="=", sprite="¿", colour=(255, 213, 0), name="Gas Cloud", ai_cls=GasCloud, radius=0)

torch = Item(char="v", colour=(139, 69, 19), name="Torch", equippable=equippable.Torch())