from __future__ import annotations
import lzma
import pickle
import random
from typing import TYPE_CHECKING

from tcod.console import Console
from tcod.map import compute_fov

import exceptions
import entity_factories
import tile_types

from message_log import MessageLog
from components.ai import ConfusedEnemy
import render_functions

import sys
from os import path, makedirs

import g
import tcod

if getattr(sys, 'frozen', False):
    game_dir = path.dirname(sys.executable)
else:
    game_dir=path.dirname(__file__)

if TYPE_CHECKING:
    from entity import Entity
    from game_map import GameMap, GameWorld

class Engine:
    game_map: GameMap
    game_world: GameWorld
    music_name: str
    def __init__(self, player: Actor):
        self.message_log = MessageLog()
        self.mouse_location = (0, 0)
        self.player = player
        self.fov_radius = 8
        self.original_radius = 8
        self.inv_hover = False
        self.char_hover = False
        self.look_hover = False
        self.path_xy = []
        self.story_shown = False
        self.quit_called = False
        self.game_completed = False
        self.is_hardcore_mode = True
        

    def handle_enemy_turns(self) -> None:
        for entity in set(self.game_map.actors) - {self.player}:
            if entity.ai:
                try:
                    entity.ai.perform()
                except exceptions.Impossible:
                    pass
    def handle_entity_toxic_turns(self) -> None:
        for entity in set(self.game_map.actors):
            if entity.ai:
                try:
                    if self.game_map.tiles["toxic"][entity.x, entity.y]:
                        if self.game_map.tiles[entity.x, entity.y] == tile_types.fire:
                            if entity.name == "Flamer":
                                pass
                            else:
                                entity.soldier.hp -= 10
                                if entity.name == "Player Infantry" or entity.name == "Player Medic" or entity.name == "Player Sergeant":
                                    mixer = g.mixer
                                    sound, sample_rate = soundfile.read("media/Audio/hit.ogg")
                                    sound = mixer.device.convert(sound, sample_rate)
                                    channel = mixer.play(sound=sound, volume=g.mixer_volume)
                        else:
                            if(entity.equipment.mask_equiped()):
                                pass
                            else:
                                entity.soldier.hp -= 1
                                if entity.name == "Player Infantry" or entity.name == "Player Medic" or entity.name == "Player Sergeant":
                                    mixer = g.mixer
                                    sound, sample_rate = soundfile.read("media/Audio/cough.ogg")
                                    sound = mixer.device.convert(sound, sample_rate)
                                    channel = mixer.play(sound=sound, volume=g.mixer_volume)
                except exceptions.Impossible:
                    pass
    def handle_gas_turns(self) -> None:
        for entity in set(self.game_map.gasActors):
            if entity.ai:
                try:
                    entity.ai.perform()    
                    
                except exceptions.Impossible:
                    pass
                
    def save_as(self, filename: str) -> None:
        if not path.exists(path.join(game_dir, "saves")):
            makedirs("saves")
        save_data = lzma.compress(pickle.dumps(self))
        with open(path.join(game_dir, "saves/"+filename), "wb") as f:
            f.write(save_data)

    def get_fov_radius(self) -> int:
        return self.fov_radius

    def set_fov_radius(self, newRadius: int) -> None:
        self.fov_radius = newRadius

    def get_original_fov(self) -> int:
        return self.original_radius

    def set_original_fov(self, originalRadius: int) -> None:
        self.original_radius = originalRadius
    
    def update_fov(self) -> None:
        self.game_map.visible[:] = compute_fov(self.game_map.tiles["transparent"], (self.player.x, self.player.y), radius=self.fov_radius)
        # set tile as explored
        self.game_map.explored |= self.game_map.visible

    def hover_set(self, setValue: bool, select: int) -> None:
        if select == 1:
            self.inv_hover = setValue
        if select == 2:
            self.char_hover = setValue
        if select == 3:
            self.look_hover = setValue

    def render(self, console: Console) -> None:
        self.game_map.render(console)
        render_functions.render_frames(console=console, location1=(-1, 43), location2=(69, -1))
        self.message_log.render(console=console, x=21, y=45, width=40, height=5)
        render_functions.render_bar(console=console, current_value=self.player.soldier.hp, maximum_value=self.player.soldier.max_hp, total_width=20)
        render_functions.render_names_at_mouse_location(console=console, x=21, y=44, engine=self)
        render_functions.render_area_level(console=console, room_level=self.game_world.current_floor, location=(0, 46))
        render_functions.render_equipment(console=console, weapon_name=self.player.equipment.weapon_in_use(), ammo_amount=self.player.equipment.current_ammo(), max_ammo=self.player.equipment.max_ammo(),  helmet_name=self.player.equipment.helmet_in_use(), mask_equiped=self.player.equipment.mask_equiped(), armour_name=self.player.equipment.armour_in_use(), location=(61, 44), inv_hover=self.inv_hover)
        render_functions.render_btn(console=console, text="Char", is_hover=self.char_hover, location=(1, 48))
        render_functions.render_btn(console=console, text="Look", is_hover=self.look_hover, location=(6, 48))
        if self.player.panicNumber > 0 or self.player.soldier.hp < self.player.soldier.maxHp / 10:
            render_functions.render_player_panic(console=console, turns=self.player.panicNumber, location=(0, 49))

        if self.game_world.current_floor == 1 or self.game_world.current_floor == 11 or self.game_world.current_floor == 21 or self.game_world.current_floor == 31:
            if  self.story_shown == False:
                render_functions.render_story(console=console, area_lvl=self.game_world.current_floor)
        

    
        