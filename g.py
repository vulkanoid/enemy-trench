import tcod
import time
import soundfile
import tcod.sdl.audio
import sys
from os import path

if getattr(sys, 'frozen', False):
    game_dir = path.dirname(sys.executable)
else:
    game_dir=path.dirname(__file__)


context: tcod.context.Context
mixer = tcod.sdl.audio.BasicMixer(tcod.sdl.audio.open())
music = tcod.sdl.audio.BasicMixer(tcod.sdl.audio.open())

mixer_volume: float = 1.0
music_volume: float = 1.0

isAscii : bool = False

global_channel = None

main_menu = None