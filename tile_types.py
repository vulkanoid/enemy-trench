from typing import Tuple

import numpy as np
import g
#creates graphical data type
graphic_dt = np.dtype([("ch", np.int32), ("fg", "3B"), ("bg", "3B")])

#creates tile data type
tile_dt = np.dtype([("walkable", np.bool_), ("toxic", np.bool_), ("safe", np.bool_), ("mud", np.bool_), ("transparent", np.bool_), ("dark", graphic_dt), ("light", graphic_dt), ("dark_sprite", graphic_dt), ("light_sprite", graphic_dt)])

def new_tile(*, walkable: int, toxic: int, safe: int, mud: int, transparent: int, char: str = "?", dark: Tuple[int, Tuple[int, int, int], Tuple[int, int, int]], light: Tuple[int, Tuple[int, int, int], Tuple[int, int, int]], dark_sprite: Tuple[int, Tuple[int, int, int], Tuple[int, int, int]], light_sprite: Tuple[int, Tuple[int, int, int], Tuple[int, int, int]]) -> np.ndarray:
    """Helper function for defining individual tile types """
    #if g.isAscii == False:
    #    new_dark = list(dark)
    #    new_dark[0] = ord(char)
    #    dark = tuple(new_dark)
    return np.array((walkable, toxic, safe, mud, transparent, dark, light, dark_sprite, light_sprite), dtype=tile_dt)
#unexplored tiles
SHROUD = np.array((ord(" "), (255, 255, 255), (0, 0, 0)), dtype=graphic_dt)

floor = new_tile(walkable=True, toxic=False, safe=True, mud=False, transparent=True, char=".", dark=(ord("."), (182, 103, 32), (0, 0, 0)), light=(ord("."), (196, 157, 0), (0, 0, 0)), dark_sprite=(ord("Ç"), (182, 103, 32), (0, 0, 0)), light_sprite=(ord("Ç"), (196, 157, 0), (0, 0, 0)))
grass = new_tile(walkable=True, toxic=False, safe=True, mud=False, transparent=True, dark=(ord("."), (0, 102, 0), (0, 0, 0)), light=(ord("."), (76, 153, 0), (0, 0, 0)), dark_sprite=(ord("Ç"), (0, 102, 0), (0, 0, 0)), light_sprite=(ord("Ç"), (76, 153, 0), (0, 0, 0)))
pallet = new_tile(walkable=True, toxic=False, safe=True, mud=False, transparent=True, dark=(ord("≡"), (184, 181, 120), (0, 0, 0)), light=(ord("≡"), (255, 251, 175), (0, 0, 0)), dark_sprite=(ord("ü"), (184, 181, 120), (0, 0, 0)), light_sprite=(ord("ü"), (255, 251, 175), (0, 0, 0)))
mud = new_tile(walkable=True, toxic=False, safe=True, mud=True, transparent=True, dark=(ord("."), (71, 54, 32), (0, 0, 0)), light=(ord("."), (175, 133, 83), (0, 0, 0)), dark_sprite=(ord("Ç"), (71, 54, 32), (0, 0, 0)), light_sprite=(ord("Ç"), (175, 133, 83), (0, 0, 0)))
wall = new_tile(walkable=False, toxic=False, safe=False, mud=False, transparent=False, dark=(ord("#"), (65, 37, 0), (0, 0, 0)), light=(ord("#"), (202, 108, 0), (0, 0, 0)), dark_sprite=(ord("é"), (65, 37, 0), (0, 0, 0)), light_sprite=(ord("é"), (202, 108, 0), (0, 0, 0)))
tree = new_tile(walkable=False, toxic=False, safe=False, mud=False, transparent=False, dark=(ord("4"), (28, 38, 26), (0, 0, 0)), light=(ord("4"), (43, 60, 41), (0, 0, 0)), dark_sprite=(ord("â"), (28, 38, 26), (0, 0, 0)), light_sprite=(ord("â"), (43, 60, 41), (0, 0, 0)))
down_stairs = new_tile(walkable=True, toxic=False, safe=True, mud=False, transparent=True, dark=(ord(">"), (100, 100, 100), (0, 0, 0)), light=(ord(">"), (200, 200, 200), (0, 0, 0)), dark_sprite=(ord("ä"), (100, 100, 100), (0, 0, 0)), light_sprite=(ord("ä"), (200, 200, 200), (0, 0, 0)))
gas_cloud = new_tile(walkable=True, toxic=True, safe=True, mud=False, transparent=True, dark=(ord("."), (182, 103, 32), (255, 104, 0)), light=(ord("."), (196, 157, 0), (255, 171, 0)), dark_sprite=(ord("Ç"), (182, 103, 32), (255, 104, 0)), light_sprite=(ord("Ç"), (196, 157, 0), (255, 171, 0)))
gas_mud = new_tile(walkable=True, toxic=True, safe=True, mud=True, transparent=True, dark=(ord("."), (58, 60, 59), (255, 104, 0)), light=(ord("."), (170, 172, 174), (255, 171, 0)), dark_sprite=(ord("Ç"), (58, 60, 59), (255, 104, 0)), light_sprite=(ord("Ç"), (170, 172, 174), (255, 171, 0)))
gas_grass = new_tile(walkable=True, toxic=True, safe=True, mud=False, transparent=True, dark=(ord("."), (0, 102, 0), (255, 104, 0)), light=(ord("."), (76, 153, 0), (255, 171, 0)), dark_sprite=(ord("Ç"), (0, 102, 0), (255, 104, 0)), light_sprite=(ord("Ç"), (76, 153, 0), (255, 171, 0)))
gas_pallet = new_tile(walkable=True, toxic=True, safe=True, mud=False, transparent=True, dark=(ord("≡"), (184, 181, 120), (255, 104, 0)), light=(ord("≡"), (255, 251, 175), (255, 171, 0)), dark_sprite=(ord("ü"), (184, 181, 120), (255, 104, 0)), light_sprite=(ord("ü"), (255, 251, 175), (255, 171, 0)))
gas_door_closed = new_tile(walkable=True, toxic=True, safe=True, mud=False, transparent=False, dark=(ord("+"), (205, 205, 205), (255, 104, 0)), light=(ord("+"), (255, 255, 255), (255, 171, 0)), dark_sprite=(ord("å"), (205, 205, 205), (255, 104, 0)), light_sprite=(ord("å"), (255, 255, 255), (255, 171, 0)))
gas_door_open = new_tile(walkable=True, toxic=True, safe=True, mud=False, transparent=True, dark=(ord("-"), (205, 205, 205), (255, 104, 0)), light=(ord("-"), (255, 255, 255), (255, 171, 0)), dark_sprite=(ord("ç"), (205, 205, 205), (255, 104, 0)), light_sprite=(ord("ç"), (255, 255, 255), (255, 171, 0)))
fire = new_tile(walkable=True, toxic=True, safe=False, mud=False, transparent=True, dark=(ord("▓"), (220, 220, 0), (220, 0, 0)), light=(ord("▓"), (255, 255, 0), (255, 0, 0)), dark_sprite=(ord("à"), (220, 220, 0), (220, 0, 0)), light_sprite=(ord("à"), (255, 255, 0), (255, 0, 0)))
door_closed = new_tile(walkable=True, toxic=False, safe=True, mud=False, transparent=False, dark=(ord("+"), (205, 205, 205), (0, 0, 0)), light=(ord("+"), (255, 255, 255), (0, 0, 0)), dark_sprite=(ord("å"), (205, 205, 205), (0, 0, 0)), light_sprite=(ord("å"), (255, 255, 255), (0, 0, 0)))
door_open = new_tile(walkable=True, toxic=False, safe=True, mud=False, transparent=True, dark=(ord("-"), (205, 205, 205), (0, 0, 0)), light=(ord("-"), (255, 255, 255), (0, 0, 0)), dark_sprite=(ord("ç"), (205, 205, 205), (0, 0, 0)), light_sprite=(ord("ç"), (255, 255, 255), (0, 0, 0)))
mine_active = new_tile(walkable=True, toxic=False, safe=True, mud=False, transparent=True, dark=(ord("_"), (98, 99, 89), (0, 0, 0)), light=(ord("_"), (185, 186, 168), (0, 0, 0)), dark_sprite=(ord("ê"), (98, 99, 89), (0, 0, 0)), light_sprite=(ord("ê"), (185, 186, 168), (0, 0, 0)))
barbed_wire = new_tile(walkable=True, toxic=True, safe=True, mud=True, transparent=True, dark=(ord("░"), (71, 54, 32), (0, 0, 0)), light=(ord("░"), (175, 133, 83), (0, 0, 0)), dark_sprite=(ord("ë"), (71, 54, 32), (0, 0, 0)), light_sprite=(ord("ë"), (175, 133, 83), (0, 0, 0)))