from __future__ import annotations

from typing import List, TYPE_CHECKING

from components.base_component import BaseComponent

if TYPE_CHECKING:
    from entity import Actor, Item

class Inventory(BaseComponent):
    parent: Actor

    def __init__(self, capacity: int):
        self.capacity = capacity
        self.items: List[Item] = []


    def drop(self, item: Item) -> None:
        self.items.remove(item)
        item.place(self.parent.x, self.parent.y, self.gamemap)

        self.engine.message_log.add_message(f"You dropped the {item.name}.")

    def enemy_drop(self, item: Item) -> None:
        item.place(self.parent.x, self.parent.y, self.gamemap)
        self.engine.message_log.add_message(f"{self.parent.name} dropped {item.name}.")

    def inventory_empty(self) -> bool:
        if len(self.items) == 0:
            return True
        return False