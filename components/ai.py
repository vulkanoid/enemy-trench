from __future__ import annotations
import random
from typing import List, Optional, Tuple, TYPE_CHECKING

import numpy as np
import tcod


import g
import time
import soundfile
import tcod.sdl.audio

import tile_types

from actions import Action, BumpAction, MeleeAction, MovementAction, WaitAction, EnemyShootAction, SpawnAction, PanicAction, StuckAction, EnemyMachineGunShootAction, EnemyHealAction, EnemyExplosionAction, FlameAction

if TYPE_CHECKING:
    from entity import Actor

class BaseAI(Action):
    entity: Actor
    def perform(self) -> None:
        raise NotImplementedError()

    def get_safe_path_to(self, dest_x: int, dest_y: int) -> List[Tuple[int, int]]:

        #gets path to target position

        cost = np.array(self.entity.gamemap.tiles["safe"], dtype=np.int8)

        for entity in self.entity.gamemap.entities:
            if entity.blocks_movement and cost[entity.x, entity.y]:

                cost[entity.x, entity.y] += 10

        graph = tcod.path.SimpleGraph(cost=cost, cardinal=2, diagonal=3)
        pathfinder = tcod.path.Pathfinder(graph)

        pathfinder.add_root((self.entity.x, self.entity.y))

        path: List[List[int]] = pathfinder.path_to((dest_x, dest_y))[1:].tolist()

        return [(index[0], index[1]) for index in path]

    def get_path_to(self, dest_x: int, dest_y: int) -> List[Tuple[int, int]]:

        #gets path to target position

        cost = np.array(self.entity.gamemap.tiles["walkable"], dtype=np.int8)

        for entity in self.entity.gamemap.entities:
            if entity.blocks_movement and cost[entity.x, entity.y]:

                cost[entity.x, entity.y] += 10

        graph = tcod.path.SimpleGraph(cost=cost, cardinal=2, diagonal=3)
        pathfinder = tcod.path.Pathfinder(graph)

        pathfinder.add_root((self.entity.x, self.entity.y))

        path: List[List[int]] = pathfinder.path_to((dest_x, dest_y))[1:].tolist()

        return [(index[0], index[1]) for index in path]
    
    def is_blocked_path(self, dest_x: int, dest_y: int) -> List[Tuple[int, int]]:

        cost = np.array(self.entity.gamemap.tiles["walkable"], dtype=np.int8).toa

        for entity in self.entity.gamemap.entities:
            if entity.blocks_movement and cost[entity.x, entity.y]:

                cost[entity.x, entity.y] += 10
        
        graph = tcod.path.SimpleGraph(cost=cost, cardinal=2, diagonal=3)
        pathfinder = tcod.path.Pathfinder(graph)

        pathfinder.add_root((self.entity.x, self.entity.y))

        path: List[List[int]] = pathfinder.path_to((dest_x, dest_y))[1:].tolist()


        return [(index[0], index[1]) for index in path]

class PlayerMouseMovemnt(BaseAI):
    def __init__(self, entity: Actor, dest_xy: Tuple[int, int]):
        super().__init__(entity)
        self.path: List[Tuple[int, int]] = []

    def perform(self) -> None:
        dest_x, dest_y = self.dest_xy
        print(f'dest_xy: {self.dest_xy}')
        camera = self.engine.game_map.get_camera()
        dx = dest_x - self.entity.x
        dy = dest_y - self.entity.y
        distance = max(abs(dx), abs(dy))
        if(PanicAction.getPanic(self, self.entity)):
            return PanicAction(self.entity, self.entity.x, self.entity.y).perform()

        if self.engine.game_map.visible[self.entity.x, self.entity.y]:
            
            self.path = self.get_safe_path_to(dx, dy)
        else:
            self.path = None

        if self.path:
            dest_x, dest_y = self.path.pop(0)
            print(f'path.pop: {self.path.pop(0)}')
            if self.engine.game_map.tiles["mud"][self.entity.x, self.entity.y]:
                if(StuckAction.getMove(self, self.entity)):
                    return StuckAction(self.entity, self.entity.x, self.entity.y).perform()
            return MovementAction(self.entity, dest_x - self.entity.x, dest_y - self.entity.y).perform()
            

        return WaitAction(self.entity).perform()
class HostileEnemy(BaseAI):
    def __init__(self, entity: Actor):
        super().__init__(entity)
        self.path: List[Tuple[int, int]] = []

    def perform(self) -> None:
        target = self.engine.player
        dx = target.x - self.entity.x
        dy = target.y - self.entity.y
        shootPlayer: int
        shootPlayer = 0
        distance = max(abs(dx), abs(dy))
        if(PanicAction.getPanic(self, self.entity)):
            return PanicAction(self.entity, self.entity.x, self.entity.y).perform()

        if self.entity.equipment.ranged_equiped() and self.engine.game_map.visible[self.entity.x, self.entity.y]:
            shootPlayer = random.randint(0, 1)

        if self.engine.game_map.visible[self.entity.x, self.entity.y]:
            if distance <= 1:
                return MeleeAction(self.entity, dx, dy).perform()
            
            self.path = self.get_safe_path_to(target.x, target.y)

        if self.path:
            dest_x, dest_y = self.path.pop(0)
            if shootPlayer == 0:
                if self.engine.game_map.tiles["mud"][self.entity.x, self.entity.y]:
                    if(StuckAction.getMove(self, self.entity)):
                        return StuckAction(self.entity, self.entity.x, self.entity.y).perform()
                return MovementAction(self.entity, dest_x - self.entity.x, dest_y - self.entity.y).perform()
            else:
                return EnemyShootAction(self.entity, dx, dy).perform()

        return WaitAction(self.entity).perform()

class FlameEnemy(BaseAI):
    def __init__(self, entity: Actor):
        super().__init__(entity)
        self.path: List[Tuple[int, int]] = []

    def perform(self) -> None:
        target = self.engine.player
        dx = target.x - self.entity.x
        dy = target.y - self.entity.y
        distance = max(abs(dx), abs(dy))
        if(PanicAction.getPanic(self, self.entity)):
            return PanicAction(self.entity, self.entity.x, self.entity.y).perform()

        if self.engine.game_map.visible[self.entity.x, self.entity.y]:
            if distance <= 2:
                return FlameAction(self.entity, dx, dy).perform()

            self.path = self.get_path_to(target.x, target.y)

        if self.path:
            dest_x, dest_y = self.path.pop(0)
            if self.engine.game_map.tiles["mud"][self.entity.x, self.entity.y]:
                if(StuckAction.getMove(self, self.entity)):
                    return StuckAction(self.entity, self.entity.x, self.entity.y).perform()
            return MovementAction(self.entity, dest_x - self.entity.x, dest_y - self.entity.y).perform()

        return WaitAction(self.entity).perform()

class ExplosivesEnemy(BaseAI):
    def __init__(self, entity: Actor):
        super().__init__(entity)
        self.path: List[Tuple[int, int]] = []

    def perform(self) -> None:
        target = self.engine.player
        dx = target.x - self.entity.x
        dy = target.y - self.entity.y
        shootPlayer: int
        explodePlayer: int
        shootPlayer = 0
        explodePlayer = 0
        distance = max(abs(dx), abs(dy))
        if(PanicAction.getPanic(self, self.entity)):
            return PanicAction(self.entity, self.entity.x, self.entity.y).perform()

        if self.entity.equipment.ranged_equiped() and self.engine.game_map.visible[self.entity.x, self.entity.y]:
            shootPlayer = random.randint(0, 1)
            explodePlayer = random.randint(0, 6)

        if self.engine.game_map.visible[self.entity.x, self.entity.y]:
            if distance <= 1:
                return MeleeAction(self.entity, dx, dy).perform()

            self.path = self.get_path_to(target.x, target.y)

        if self.path:
            dest_x, dest_y = self.path.pop(0)
            if shootPlayer == 0:
                if self.engine.game_map.tiles["mud"][self.entity.x, self.entity.y]:
                    if(StuckAction.getMove(self, self.entity)):
                        return StuckAction(self.entity, self.entity.x, self.entity.y).perform()
                return MovementAction(self.entity, dest_x - self.entity.x, dest_y - self.entity.y).perform()
            else:
                if self.engine.player.is_alive:
                    if explodePlayer == 6:
                        return EnemyExplosionAction(self.entity, target.x, target.y).perform()
                    else:
                        return EnemyShootAction(self.entity, dx, dy).perform()

        return WaitAction(self.entity).perform()

class StationaryEnemy(BaseAI):
    def __init__(self, entity: Actor):
        super().__init__(entity)
        self.path: List[Tuple[int, int]] = []

    def perform(self) -> None:
        target = self.engine.player
        dx = target.x - self.entity.x
        dy = target.y - self.entity.y
        shootPlayer: int
        shootPlayer = 0
        distance = max(abs(dx), abs(dy))
        if(PanicAction.getPanic(self, self.entity)):
            return PanicAction(self.entity, self.entity.x, self.entity.y).perform()

        

        if self.engine.game_map.visible[self.entity.x, self.entity.y]:
            if distance <= 1:
                return MeleeAction(self.entity, dx, dy).perform()

            self.path = self.get_path_to(target.x, target.y)
        else:
            self.path = None

        if self.path:
            dest_x, dest_y = self.path.pop(0)
            if(self.entity.equipment.machine_gun_equipped()):
                return EnemyMachineGunShootAction(self.entity, dx, dy).perform()
            else:
                return EnemyShootAction(self.entity, dx, dy).perform()

        return WaitAction(self.entity).perform()

class MedicEnemy(BaseAI):
    def __init__(self, entity: Actor):
        super().__init__(entity)
        self.path: List[Tuple[int, int]] = []

    def perform(self) -> None:
        target = self.engine.player
        dx = target.x - self.entity.x
        dy = target.y - self.entity.y
        cur_ent_distance = 0
        for entity in set(self.engine.game_map.actors):
            if entity != self.engine.player and entity.name != "FieldMedic":
                cur_ent_distance = max(abs(dx), abs(dy))
                if cur_ent_distance <= 1 and entity.soldier.hp < entity.soldier.max_hp:
                    target = entity
                    dx = entity.x - self.entity.x
                    dy = entity.y - self.entity.y

        shootPlayer: int
        shootPlayer = 0
        distance = max(abs(dx), abs(dy))
        if(PanicAction.getPanic(self, self.entity)):
            return PanicAction(self.entity, self.entity.x, self.entity.y).perform()

        if self.entity.equipment.ranged_equiped() and self.engine.game_map.visible[self.entity.x, self.entity.y]:
            shootPlayer = random.randint(0, 2)

        if self.engine.game_map.visible[self.entity.x, self.entity.y]:
            if distance <= 1:
                if target == self.engine.player:
                    return MeleeAction(self.entity, dx, dy).perform()
                else:
                    return EnemyHealAction(self.entity, dx, dy).perform()

            self.path = self.get_path_to(target.x, target.y)

        if self.path:
            dest_x, dest_y = self.path.pop(0)
            if shootPlayer == 0 or target != self.engine.player:
                if self.engine.game_map.tiles["mud"][self.entity.x, self.entity.y]:
                    if(StuckAction.getMove(self, self.entity)):
                        return StuckAction(self.entity, self.entity.x, self.entity.y).perform()
                return MovementAction(self.entity, dest_x - self.entity.x, dest_y - self.entity.y).perform()
            else:
                return EnemyShootAction(self.entity, dx, dy).perform()

        return WaitAction(self.entity).perform()

class ConfusedEnemy(BaseAI):
    def __init__(self, entity: Actor, previous_ai: Optional[BaseAI], turns_remaining: int):
        super().__init__(entity)

        self.previous_ai = previous_ai
        self.turns_remaining = turns_remaining

    def perform(self) -> None:
        if self.turns_remaining <= 0:
            self.engine.message_log.add_message(f"The {self.entity.name} is no longer confused")
            self.entity.ai = self.previous_ai
        elif self.entity.name == "Marksman Boss":
            self.turns_remaining -= 1
            return WaitAction(self.entity).perform()
        else:
            direction_x, direction_y = random.choice([(-1, 1), (0, -1), (1, -1), (-1, 0), (1, 0), (-1, 1), (0, 1), (1, 1)])
            self.turns_remaining -= 1

            return BumpAction(self.entity, direction_x, direction_y).perform()
class Fallen(BaseAI):
    def __init__(self, entity: Actor):
        super().__init__(entity)
        
    def perform(self) -> None:
        return None

class GasCloud(BaseAI):
    def __init__(self, entity: Actor):
        super().__init__(entity)

    def perform(self) -> None:
        if self.engine.game_map.explored[self.entity.x, self.entity.y]:
            

            x_add = 0
            y_add = 0

            if self.entity.radius == 0:
                self.engine.game_map.tiles[self.entity.x, self.entity.y] = tile_types.gas_cloud
                self.engine.game_map.setGasActive
                self.engine.message_log.add_message("GAS!!!")
                self.entity.radius += 1
                mixer = g.mixer
                sound, sample_rate = soundfile.read("media/Audio/gas_attack.ogg")
                sound = mixer.device.convert(sound, sample_rate)
                channel = mixer.play(sound=sound, volume=g.mixer_volume)

            
            
            x_1 = self.entity.x - self.entity.radius
            y_1 = self.entity.y - self.entity.radius

            x_2 = self.entity.x + self.entity.radius
            y_2 = self.entity.y + self.entity.radius

            if x_1 < 1:
                x_1 = 1
            if x_1 > 119:
                x_1 = 119
            if x_2 < 1:
                x_2 = 1
            if x_2 > 119:
                x_2 = 119

            if x_1 < 1:
                x_1 = 1
            if x_1 > 119:
                x_1 = 119
            if x_2 < 1:
                x_2 = 1
            if x_2 > 119:
                x_2 = 119


            



            x_add = self.entity.x - self.entity.radius
            y_add = self.entity.y - self.entity.radius
            x_check = x_add
            y_check = y_add
            setter = 1
            count = self.entity.radius * 2
            i = 1
            for x in range(((self.entity.radius * 8))):
                if setter == 5:
                    break

                if setter == 1:
                    x_add += 1
                    count -= 1
                    if count == 0:
                        setter = 2
                        x_add = self.entity.x + self.entity.radius
                        y_add = self.entity.y - self.entity.radius
                        count = self.entity.radius * 2
                elif setter == 2:
                    y_add += 1
                    count -= 1
                    if count == 0:
                        setter = 3
                        count = self.entity.radius * 2
                        x_add = self.entity.x + self.entity.radius
                        y_add = self.entity.y + self.entity.radius
                elif setter == 3:
                    x_add -= 1
                    count -= 1
                    if count == 0:
                        setter = 4
                        count = self.entity.radius * 2
                        x_add = self.entity.x - self.entity.radius
                        y_add = self.entity.y + self.entity.radius
                elif setter == 4:
                    y_add -= 1
                    count -= 1
                    if count == 0:
                        setter = 5
                        count = self.entity.radius * 2
                        x_add = self.entity.x - self.entity.radius
                        y_add = self.entity.y - self.entity.radius

                if x_add >= 119:
                    x_add = 119
                if x_add <= 1:
                    x_add = 1
                if y_add >= 62:
                    y_add = 62
                if y_add <= 1:
                    y_add = 1

                if self.engine.game_map.tiles["walkable"][x_add, y_add]:
                    if self.engine.game_map.tiles[x_add, y_add] == tile_types.floor:
                        self.engine.game_map.tiles[x_add, y_add] = tile_types.gas_cloud
                    if self.engine.game_map.tiles[x_add, y_add] == tile_types.pallet:
                        self.engine.game_map.tiles[x_add, y_add] = tile_types.gas_pallet
                    if self.engine.game_map.tiles[x_add, y_add] == tile_types.door_closed:
                        self.engine.game_map.tiles[x_add, y_add] = tile_types.gas_door_closed
                    if self.engine.game_map.tiles[x_add, y_add] == tile_types.door_open:
                        self.engine.game_map.tiles[x_add, y_add] = tile_types.gas_door_open


                
            self.entity.radius+=1
            if self.entity.radius >= 120:
                self.entity.ai = None
            
           
                        
                            
                            
            