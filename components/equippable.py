from __future__ import annotations

from typing import TYPE_CHECKING

from components.base_component import BaseComponent
from equipment_types import EquipmentType

if TYPE_CHECKING:
    from entity import Item

class Equippable(BaseComponent):
    parent: Item

    def __init__(self, equipment_type: EquipmentType, strength_bonus: int = 0, defense_bonus: int = 0, dexterity_bonus: int = 0, vitality_bonus: int = 0, composure_bonus: int = 0, aim_bonus: int = 0, shot_damage: int = 0, current_ammo = 0, max_ammo = 0):
        self.equipment_type = equipment_type
        self.strength_bonus = strength_bonus
        self.defense_bonus = defense_bonus
        self.dexterity_bonus = dexterity_bonus
        self.vitality_bonus = vitality_bonus
        self.composure_bonus = composure_bonus
        self.aim_bonus = aim_bonus
        self.shot_damage = shot_damage
        self.current_ammo = current_ammo
        self.max_ammo = max_ammo

    def str_bonus(self) -> int:
        return self.strength_bonus

    def def_bonus(self) -> int:
        return self.defense_bonus

    def dex_bonus(self) -> int:
        return self.dexterity_bonus

    def vit_bonus(self) -> int:
        return self.vitality_bonus

    def com_bonus(self) -> int:
        return self.composure_bonus

    def am_bonus(self) -> int:
        return self.aim_bonus
    
    def shot_dam(self) -> int:
        return self.shot_damage

    def is_headarmour(self) -> bool:
        if self.equipment_type == EquipmentType.HEAD_ARMOUR:
            return True
        return False

    def is_weapon(self) -> bool:
        if self.equipment_type == EquipmentType.WEAPON:
            return True
        return False

    def is_bodyarmour(self) -> bool:
        if self.equipment_type == EquipmentType.BODY_ARMOUR:
            return True
        return False

    def is_mask(self) -> bool:
        if self.equipment_type == EquipmentType.MASK:
            return True
        return False

class Melee(Equippable):
    def __init__(self, strength_bonus: int = 0) -> None:
        super().__init__(equipment_type=EquipmentType.WEAPON, strength_bonus=strength_bonus)

class Pistol(Equippable):
    def __init__(self, shot_damage: int = 0, current_ammo: int = 0, max_ammo: int = 0) -> None:
        super().__init__(equipment_type=EquipmentType.WEAPON, dexterity_bonus=1, shot_damage=shot_damage, current_ammo=current_ammo, max_ammo=max_ammo, aim_bonus=2)

class Rifle(Equippable):
    def __init__(self, shot_damage: int = 0, current_ammo: int = 0, max_ammo: int = 0) -> None:
        super().__init__(equipment_type=EquipmentType.WEAPON, aim_bonus=1, shot_damage=shot_damage, current_ammo=current_ammo, max_ammo=max_ammo)

class RifleBayonet(Equippable):
    def __init__(self, shot_damage: int = 0, current_ammo: int = 0, max_ammo: int = 0) -> None:
        super().__init__(equipment_type=EquipmentType.WEAPON, aim_bonus=1, strength_bonus=2, shot_damage=shot_damage, current_ammo=current_ammo, max_ammo=max_ammo)

class Sniper(Equippable):
    def __init__(self, shot_damage: int = 0, current_ammo: int = 0, max_ammo: int = 0) -> None:
        super().__init__(equipment_type=EquipmentType.WEAPON, aim_bonus=3, shot_damage=shot_damage, current_ammo=current_ammo, max_ammo=max_ammo)

class MachineGun(Equippable):
    def __init__(self, shot_damage: int = 0, current_ammo: int = 0, max_ammo: int = 0) -> None:
        super().__init__(equipment_type=EquipmentType.WEAPON, shot_damage=shot_damage, current_ammo=current_ammo, max_ammo=max_ammo)

class Flamethrower(Equippable):
    def __init__(self, current_ammo: int = 0, max_ammo: int = 0) -> None:
        super().__init__(equipment_type=EquipmentType.WEAPON, aim_bonus=2, current_ammo=current_ammo, max_ammo=max_ammo)

class helmetTypeA(Equippable):
    def __init__(self) -> None:
        super().__init__(equipment_type=EquipmentType.HEAD_ARMOUR, defense_bonus=1)

class helmetMark1(Equippable):
    def __init__(self) -> None:
        super().__init__(equipment_type=EquipmentType.HEAD_ARMOUR, defense_bonus=3)

class BodyArmour(Equippable):
    def __init__(self, defense_bonus: int = 0, dexterity_bonus: int = 0) -> None:
        super().__init__(equipment_type=EquipmentType.BODY_ARMOUR, defense_bonus=defense_bonus, dexterity_bonus=dexterity_bonus)

class GasMask(Equippable):
    def __init__(self) -> None:
        super().__init__(equipment_type=EquipmentType.MASK, dexterity_bonus=-1)

class Torch(Equippable):
    def __init__(self) -> None:
        super().__init__(equipment_type=EquipmentType.TORCH)



