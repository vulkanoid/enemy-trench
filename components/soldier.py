from __future__ import annotations

from typing import TYPE_CHECKING

import colour

import entity_factories

import random

import time
import soundfile
import tcod.sdl.audio

import g

from components.base_component import BaseComponent
from render_order import RenderOrder

from actions import Action, EnemyExplosionAction

if TYPE_CHECKING:
    from entity import Actor

class Soldier(BaseComponent):
    parent: Actor
    def __init__(self, hp: int, base_strength: int, base_dexterity: int, base_vitality: int, base_composure: int, rifle_ammo: int):
        self.max_hp = base_vitality * 6
        self._hp = base_vitality * 6
        self.base_strength = base_strength
        self.base_dexterity = base_dexterity
        self.base_vitality = base_vitality
        self.base_composure = base_composure
        self.rifle_ammo = rifle_ammo

    @property
    def hp(self) -> int:
        return self._hp
    
    @hp.setter
    def hp(self, value: int) -> None:
        self._hp = max(0, min(value, self.max_hp))
        if self._hp == 0 and self.parent.ai:
            self.die()

    @property
    def defense(self) -> int:
        return round(self.base_strength/2) + self.defense_bonus

    @property
    def baseStrength(self) -> int:
        return self.base_strength

    @property
    def strength(self) -> int:
        return self.base_strength + self.strength_bonus

    @property
    def baseDexterity(self) -> int:
        return self.base_dexterity

    @property
    def dexterity(self) -> int:
        return self.base_dexterity + self.dexterity_bonus

    @property
    def baseVitality(self) -> int:
        return self.base_vitality

    @property
    def vitality(self) -> int:
        return self.base_vitality + self.vitality_bonus

    @property
    def baseComposure(self) -> int:
        return self.base_composure

    @property
    def composure(self) -> int:
        return self.base_composure + self.composure_bonus

    @property
    def maxHp(self) -> int:
        return self.max_hp

    @property
    def rifleAmmo(self) -> int:
        return self.rifle_ammo

    @property
    def defense_bonus(self) -> int:
        if self.parent.equipment:
            return self.parent.equipment.defense_bonus
        else:
            return 0

    @property
    def strength_bonus(self) -> int:
        if self.parent.equipment:
            return self.parent.equipment.strength_bonus
        else:
            return 0

    @property
    def dexterity_bonus(self) -> int:
        if self.parent.equipment:
            return self.parent.equipment.dexterity_bonus
        else:
            return 0

    @property
    def vitality_bonus(self) -> int:
        if self.parent.equipment:
            return self.parent.equipment.vitality_bonus
        else:
            return 0

    @property
    def composure_bonus(self) -> int:
        if self.parent.equipment:
            return self.parent.equipment.composure_bonus
        else:
            return 0

    @property
    def aim_bonus(self) -> int:
        if self.parent.equipment:
            return self.parent.equipment.aim_bonus
        else:
            return 0

    @property
    def shot_damage(self) -> int:
        if self.parent.equipment:
            return self.parent.equipment.shot_damage
        else:
            return 0

    def die(self) -> None:
        self.parent.ai = None
        if self.engine.player is self.parent:
            death_message = "YOU DIED!"
            death_message_colour = colour.player_die
            mixer = g.mixer
            sound, sample_rate = soundfile.read("media/Audio/player_death.ogg")
            sound = mixer.device.convert(sound, sample_rate)
            channel = mixer.play(sound=sound, volume=g.mixer_volume)
            g.music.close()
            g.music = tcod.sdl.audio.BasicMixer(tcod.sdl.audio.open())
            music = g.music
            sound, sample_rate = soundfile.read("media/Music/track_8.ogg")
            sound = music.device.convert(sound, sample_rate)
            channel = music.play(sound=sound, loops=-1,  volume=g.music_volume)
            g.MainMenu.menu_mode = 0
        else:
            death_message = f"{self.parent.name} is dead!"
            death_message_colour = colour.enemy_die
            mixer = g.mixer
            sound, sample_rate = soundfile.read("media/Audio/enemy_death.ogg")
            sound = mixer.device.convert(sound, sample_rate)
            channel = mixer.play(sound=sound, volume=g.mixer_volume)
            self.parent.name = f"remains of {self.parent.name}"
            for i, item in enumerate(self.parent.inventory.items):
                self.parent.inventory.enemy_drop(item)
            if self.engine.game_world.current_floor == 30:
                self.engine.game_completed = True


        self.parent.char = "%"
        self.parent.sprite = "⌐"
        self.parent.colour = (255, 255, 255)
        self.parent.blocks_movement = False
        self.parent.render_order = RenderOrder.CORPSE

        self.engine.message_log.add_message(death_message, death_message_colour)

        self.engine.player.level.add_xp(self.parent.level.xp_given)

    def heal(self, amount: int) -> int:
        if self.hp == self.max_hp:
            return 0

        new_hp_value = self.hp + amount

        if new_hp_value > self.max_hp:
            new_hp_value = self.max_hp

        amount_recovered = new_hp_value - self.hp

        self.hp = new_hp_value

        return amount_recovered

    def take_damage(self, amount: int) -> None:
        self.hp -= amount
        if self._hp == 0 and self.parent.ai:
            self.die()

    def equipMask(self) -> None:
        self.parent.equipment.toggle_equip(entity_factories.gasMask, add_message=False)