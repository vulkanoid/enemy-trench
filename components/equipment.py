from __future__ import annotations

from typing import Optional, TYPE_CHECKING

from components.base_component import BaseComponent
from components.equippable import Melee, MachineGun, Pistol, Rifle, RifleBayonet, Sniper
from equipment_types import EquipmentType

if TYPE_CHECKING:
    from entity import Actor, Item

class Equipment(BaseComponent):
    parent: Actor

    def __init__(self, weapon: Optional[Item] = None, HEAD_ARMOUR: Optional[Item] = None, BODY_ARMOUR: Optional[Item] = None, mask: Optional[Item] = None, torch: Optional[Item] = None):
        self.weapon = weapon
        self.HEAD_ARMOUR = HEAD_ARMOUR
        self.BODY_ARMOUR = BODY_ARMOUR
        self.mask = mask
        self.torch = torch

    @property
    def defense_bonus(self) -> int:
        bonus = 0

        if self.weapon is not None and self.weapon.equippable is not None:
            bonus += self.weapon.equippable.defense_bonus
        
        if self.HEAD_ARMOUR is not None and self.HEAD_ARMOUR.equippable is not None:
            bonus += self.HEAD_ARMOUR.equippable.defense_bonus

        if self.BODY_ARMOUR is not None and self.BODY_ARMOUR.equippable is not None:
            bonus += self.BODY_ARMOUR.equippable.defense_bonus

        return bonus

    @property
    def strength_bonus(self) -> int:
        bonus = 0

        if self.weapon is not None and self.weapon.equippable is not None:
            bonus += self.weapon.equippable.strength_bonus

        if self.HEAD_ARMOUR is not None and self.HEAD_ARMOUR.equippable is not None:
            bonus += self.HEAD_ARMOUR.equippable.strength_bonus

        if self.BODY_ARMOUR is not None and self.BODY_ARMOUR.equippable is not None:
            bonus += self.BODY_ARMOUR.equippable.strength_bonus

        return bonus

    @property
    def dexterity_bonus(self) -> int:
        bonus = 0

        if self.weapon is not None and self.weapon.equippable is not None:
            bonus += self.weapon.equippable.dexterity_bonus

        if self.HEAD_ARMOUR is not None and self.HEAD_ARMOUR.equippable is not None:
            bonus += self.HEAD_ARMOUR.equippable.dexterity_bonus

        if self.BODY_ARMOUR is not None and self.BODY_ARMOUR.equippable is not None:
            bonus += self.BODY_ARMOUR.equippable.dexterity_bonus

        return bonus

    @property
    def vitality_bonus(self) -> int:
        bonus = 0

        if self.weapon is not None and self.weapon.equippable is not None:
            bonus += self.weapon.equippable.vitality_bonus

        if self.HEAD_ARMOUR is not None and self.HEAD_ARMOUR.equippable is not None:
            bonus += self.HEAD_ARMOUR.equippable.vitality_bonus

        if self.BODY_ARMOUR is not None and self.BODY_ARMOUR.equippable is not None:
            bonus += self.BODY_ARMOUR.equippable.vitality_bonus

        return bonus
    
    @property
    def composure_bonus(self) -> int:
        bonus = 0

        if self.weapon is not None and self.weapon.equippable is not None:
            bonus += self.weapon.equippable.composure_bonus

        if self.HEAD_ARMOUR is not None and self.HEAD_ARMOUR.equippable is not None:
            bonus += self.HEAD_ARMOUR.equippable.composure_bonus

        if self.BODY_ARMOUR is not None and self.BODY_ARMOUR.equippable is not None:
            bonus += self.BODY_ARMOUR.equippable.composure_bonus

        return bonus

    @property
    def aim_bonus(self) -> int:
        bonus = 0

        if self.weapon is not None and self.weapon.equippable is not None:
            bonus += self.weapon.equippable.aim_bonus

        if self.HEAD_ARMOUR is not None and self.HEAD_ARMOUR.equippable is not None:
            bonus += self.HEAD_ARMOUR.equippable.aim_bonus

        if self.BODY_ARMOUR is not None and self.BODY_ARMOUR.equippable is not None:
            bonus += self.BODY_ARMOUR.equippable.aim_bonus

        return bonus

    @property
    def shot_damage(self) -> int:
        damage = 0

        if self.weapon is not None and self.weapon.equippable is not None:
            damage += self.weapon.equippable.shot_damage

        if self.HEAD_ARMOUR is not None and self.HEAD_ARMOUR.equippable is not None:
            damage += self.HEAD_ARMOUR.equippable.shot_damage

        if self.BODY_ARMOUR is not None and self.BODY_ARMOUR.equippable is not None:
            damage += self.BODY_ARMOUR.equippable.shot_damage

        return damage

    @property
    def fov_bonus(self) -> int:
        fov_bonus = 0
        return fov_bonus

    def ranged_equiped(self) -> bool:
        if(self.weapon == None):
            return False
        if(isinstance(self.weapon.equippable, Melee)):
            return False
        else:
            return True
    def rifle_equipped(self) -> bool:
        if(self.weapon == None):
            return False
        if(isinstance(self.weapon.equippable, Rifle)):
            return True
        else:
            return False
    def rifle_bayonet_equipped(self) -> bool:
        if(self.weapon == None):
            return False
        if(isinstance(self.weapon.equippable, RifleBayonet)):
            return True
        else:
            return False
    def sniper_equipped(self) -> bool:
        if(self.weapon == None):
            return False
        if(isinstance(self.weapon.equippable, Sniper)):
            return True
        else:
            return False
    def pistol_equipped(self) -> bool:
        if(self.weapon == None):
            return False
        if(isinstance(self.weapon.equippable, Pistol)):
            return True
        else:
            return False
    def machine_gun_equipped(self) -> bool:
        if(self.weapon == None):
            return False
        if(isinstance(self.weapon.equippable, MachineGun)):
            return True
        else:
            return False

    def mask_equiped(self) -> bool:
        if(self.mask == None):
            return False

        if(self.mask):
            return True
        else:
            return False

    def destroy_mask(self):
        item = self.mask
        self.toggle_equip(item, False)
        for i in self.parent.inventory.items.copy():
            if i.name == "Gas Mask":
                self.parent.inventory.items.remove(i)
                break
        self.mask = None

    def torch_equiped(self) -> bool:
        if(self.torch == None):
            return False

        if(self.torch):
            return True
        else:
            return False
    
    def destroy_torch(self):
        item = self.torch
        self.parent.inventory.items.remove(item)
        self.torch = None

    def weapon_in_use(self) -> str:
        name = ""

        if self.weapon is not None and self.weapon.equippable is not None:
            name = self.weapon.name

        return name

    def helmet_in_use(self) -> str:
        name = ""

        if self.HEAD_ARMOUR is not None and self.HEAD_ARMOUR.equippable is not None:
            name = self.HEAD_ARMOUR.name

        return name

    def armour_in_use(self) -> str:
        name = ""

        if self.BODY_ARMOUR is not None and self.BODY_ARMOUR.equippable is not None:
            name = self.BODY_ARMOUR.name

        return name

    def item_in_use(self, slot: str = "") -> Item:
        if slot == "HEAD":
            return self.HEAD_ARMOUR
        elif slot == "BODY":
            return self.BODY_ARMOUR
        elif slot == "WEAPON":
            return self.weapon
        elif slot == "MASK":
            return self.mask

        return None

    def current_ammo(self) -> int:
        ammo = 0

        if self.weapon is not None and self.weapon.equippable is not None:
            ammo = self.weapon.equippable.current_ammo

        return ammo

    def max_ammo(self) -> int:
        ammo = 0

        if self.weapon is not None and self.weapon.equippable is not None:
            ammo = self.weapon.equippable.max_ammo

        return ammo

    def add_to_ammo(self, amount: int = 0):
        self.weapon.equippable.current_ammo += amount

    def take_from_ammo(self, amount: int = 0):
        self.weapon.equippable.current_ammo -= amount


    def item_is_equipped(self, item: Item) -> bool:
        return self.weapon == item or self.HEAD_ARMOUR == item or self.BODY_ARMOUR == item or self.mask == item or self.torch == item

    def unequip_message(self, item_name: str) -> None:
        self.parent.gamemap.engine.message_log.add_message(f"{item_name} was unequipped")

    def equip_message(self, item_name: str) -> None:
        self.parent.gamemap.engine.message_log.add_message(f"{item_name} was equipped")

    def equip_to_slot(self, slot: str, item: Item, add_message: bool) -> None:
        current_item = getattr(self, slot)

        if current_item is not None:
            self.unequip_from_slot(slot, add_message)

        setattr(self, slot, item)

        if add_message:
            self.equip_message(item.name)

    def unequip_from_slot(self, slot: str, add_message: bool) -> None:
        current_item = getattr(self, slot)

        if add_message:
            self.unequip_message(current_item.name)

        setattr(self, slot, None)

    def toggle_equip(self, equippable_item: Item, add_message: bool = True) -> None:
        if (equippable_item.equippable and equippable_item.equippable.equipment_type == EquipmentType.WEAPON):
            slot = "weapon"
        if (equippable_item.equippable and equippable_item.equippable.equipment_type == EquipmentType.HEAD_ARMOUR):
            slot = "HEAD_ARMOUR"
        if (equippable_item.equippable and equippable_item.equippable.equipment_type == EquipmentType.BODY_ARMOUR):
            slot = "BODY_ARMOUR"
        if (equippable_item.equippable and equippable_item.equippable.equipment_type == EquipmentType.MASK):
            slot = "mask"
        if (equippable_item.equippable and equippable_item.equippable.equipment_type == EquipmentType.TORCH):
            slot = "torch"

        if getattr(self, slot) == equippable_item:
            self.unequip_from_slot(slot, add_message)
        else:
            self.equip_to_slot(slot, equippable_item, add_message)