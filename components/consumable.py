from __future__ import annotations

from typing import Optional, TYPE_CHECKING

import actions
import colour
import components.ai
import components.inventory
import components.equippable
import random
import tile_types
import math

import time
import soundfile
import tcod.sdl.audio

import g

from components.base_component import BaseComponent
from exceptions import Impossible
from input_handlers import SingleRangedAttackHandler, AreaRangedAttackHandler, ActionOrHandler

if TYPE_CHECKING:
    from entity import Actor, Item

class Consumable(BaseComponent):
    parent: Item

    def get_action(self, consumer: Actor) -> Optional[ActionOrHandler]:
        return actions.ItemAction(consumer, self.parent)

    def activate(self, action: actions.ItemAction) -> None:
        raise NotImplementedError()

    def consume(self) -> None:
        entity = self.parent
        inventory = entity.parent
        if isinstance(inventory, components.inventory.Inventory):
            inventory.items.remove(entity)

class HealingConsumable(Consumable):
    def __init__(self, amount: int):
        self.amount = amount

    def activate(self, action: actions.ItemAction) -> None:
        consumer = action.entity

        dice = random.randint(1, 12)
        addNumb = 0

        if consumer.soldier.baseVitality <= 1:
            addNumb = -3
        elif consumer.soldier.baseVitality == 2 or consumer.soldier.baseVitality == 3:
            addNumb = -2
        elif consumer.soldier.baseVitality == 4:
            addNumb = -1
        elif consumer.soldier.baseVitality == 5:
            addNumb = 0
        elif consumer.soldier.baseVitality == 6 or consumer.soldier.baseVitality == 7:
            addNumb = 1
        elif consumer.soldier.baseVitality == 8 or consumer.soldier.baseVitality == 9:
            addNumb = 2
        else:
            addNumb = 3
        total = dice + addNumb

        if total <= 3:
            healAmount = self.amount
        elif total > 3 and total <= 5:
            healAmount = self.amount + 2
        elif total > 5 and total <= 8:
            healAmount = self.amount + 4
        elif total > 8 and total <= 11:
            healAmount = self.amount + 6
        else:
            healAmount = consumer.soldier.maxHp



        amount_recovered = consumer.soldier.heal(healAmount)

        if amount_recovered > 0:
            mixer = g.mixer
            sound, sample_rate = soundfile.read("media/Audio/healthpack.ogg")
            sound = mixer.device.convert(sound, sample_rate)
            channel = mixer.play(sound=sound, volume=g.mixer_volume)
            self.engine.message_log.add_message(f"You consume the {self.parent.name}, and recover {amount_recovered}", colour.health_recovered)
            self.consume()
        else:
            raise Impossible(f"Your health is already full")

class KnifeThrow(Consumable):
    def __init__(self, damage: int, maximum_range: int):
        self.damage = damage
        self.maximum_range = maximum_range

    def activate(self, action: actions.ItemAction) -> None:
        consumer = action.entity
        target = None
        closest_distance = consumer.soldier.strength + 1.0

        for actor in self.engine.game_map.actors:
            if actor is not consumer and self.parent.gamemap.visible[actor.x, actor.y]:
                distance = consumer.distance(actor.x, actor.y)

                if distance < closest_distance:
                    target = actor
                    closest_distance = distance

        if target:
            mixer = g.mixer
            sound, sample_rate = soundfile.read("media/Audio/throw.ogg")
            sound = mixer.device.convert(sound, sample_rate)
            channel = mixer.play(sound=sound, volume=g.mixer_volume)
            self.engine.message_log.add_message(f"A knife strikes the {target.name} with a pierce that does {self.damage} damage!")
            target.soldier.take_damage(self.damage)
            self.consume()
        else:
            raise Impossible("No enemy in range")

class ConfusionConsumable(Consumable):
    def __init__(self, number_of_turns: int):
        self.number_of_turns = number_of_turns

    def get_action(self, consumer: Actor) -> SingleRangedAttackHandler:
        self.engine.message_log.add_message("Select a target.", colour.needs_target)
        return SingleRangedAttackHandler(self.engine, callback=lambda xy: actions.ItemAction(consumer, self.parent, xy))

        

    def activate(self, action: actions.ItemAction) -> None:
        consumer = action.entity
        target = action.target_actor

        if not self.engine.game_map.visible[action.target_xy]:
            raise Impossible("You cannot target an area you don't see")
        if not target:
            raise Impossible("You must select an enemy target.")
        if target is consumer:
            raise Impossible("You cannot target yourself!")

        mixer = g.mixer
        sound, sample_rate = soundfile.read("media/Audio/throw.ogg")
        sound = mixer.device.convert(sound, sample_rate)
        channel = mixer.play(sound=sound, volume=g.mixer_volume)

        self.engine.message_log.add_message(f"With fear in the {target.name}'s eyes, the soldier stumbles.")
        if target.name == "Heavy Boss" or target.name == "Marksman Boss" or target.name == "Flamer":
            if math.trunc(consumer.soldier.strength/2) >= 1:
                target.ai = components.ai.ConfusedEnemy(entity=target, previous_ai=target.ai, turns_remaining=math.trunc(consumer.soldier.strength/2))
            else:
                target.ai = components.ai.ConfusedEnemy(entity=target, previous_ai=target.ai, turns_remaining=1)
        else:
            target.ai = components.ai.ConfusedEnemy(entity=target, previous_ai=target.ai, turns_remaining=consumer.soldier.strength)
        mixer = g.mixer
        sound, sample_rate = soundfile.read("media/Audio/rockhit.ogg")
        sound = mixer.device.convert(sound, sample_rate)
        channel = mixer.play(sound=sound, volume=g.mixer_volume)
        self.consume()

class GrenadeDamageConsumable(Consumable):
    def __init__(self, damage: int, radius: int):
        self.damage = damage
        self.radius = radius

    def get_action(self, consumer: Actor) -> AreaRangedAttackHandler:
        self.engine.message_log.add_message("Select a target.", colour.needs_target)
        return AreaRangedAttackHandler(self.engine, radius=self.radius, callback=lambda xy: actions.ItemAction(consumer, self.parent, xy))

    def activate(self, action: actions.ItemAction) -> None:
        target_xy = action.target_xy

        if not self.engine.game_map.visible[target_xy]:
            raise Impossible("You cannot target an area you don't see")
        mixer = g.mixer
        sound, sample_rate = soundfile.read("media/Audio/throw.ogg")
        sound = mixer.device.convert(sound, sample_rate)
        channel = mixer.play(sound=sound, volume=g.mixer_volume)

        targets_hit = False
        for actor in list(self.engine.game_map.actors).copy():
            if actor.distance(*target_xy) <= self.radius+2:
                self.engine.message_log.add_message(f"The {actor.name} is hit by the explosion of the grenade, taking {self.damage} damage!")
                actor.soldier.take_damage(self.damage)
                targets_hit = True
                actor.panicNumber = 5

        mixer = g.mixer
        sound, sample_rate = soundfile.read("media/Audio/explode.ogg")
        sound = mixer.device.convert(sound, sample_rate)
        channel = mixer.play(sound=sound, volume=g.mixer_volume) 

        if not targets_hit:
            raise Impossible("No targets in that area.")

        self.consume()

class AmmoConsumable(Consumable):
    def __init__(self, amount: int, weapon: str):
        self.amount = amount
        self.weapon = weapon

    def activate(self, action: actions.ItemAction) -> None:
        consumer = action.entity

        if self.weapon == "Rifle":
            if consumer.equipment.rifle_equipped() == False and consumer.equipment.sniper_equipped() == False and consumer.equipment.rifle_bayonet_equipped() == False:
                raise Impossible(f"This ammo type won't work in your current weapon")

        if self.weapon == "Pistol":
            if consumer.equipment.pistol_equipped() == False:
                raise Impossible(f"This ammo type won't work in your current weapon")

        if self.weapon == "MG":
            if consumer.equipment.machine_gun_equipped() == False:
                raise Impossible(f"This ammo type won't work in your current weapon")

        if consumer.equipment.current_ammo() >= consumer.equipment.max_ammo():
            amount_recovered = 0
        elif consumer.equipment.current_ammo() + self.amount > consumer.equipment.max_ammo():
            amount_recovered = self.amount
            self.amount = (self.amount + consumer.equipment.current_ammo()) - consumer.equipment.max_ammo()
            amount_recovered = consumer.equipment.max_ammo() - consumer.equipment.current_ammo()
            consumer.equipment.add_to_ammo(amount_recovered)
        else:
            amount_recovered = self.amount
            consumer.equipment.add_to_ammo(self.amount)
            self.consume()

        if amount_recovered > 0:
            if self.weapon == "Rifle":
                mixer = g.mixer
                sound, sample_rate = soundfile.read("media/Audio/reload.ogg")
                sound = mixer.device.convert(sound, sample_rate)
                channel = mixer.play(sound=sound, volume=g.mixer_volume)
            if self.weapon == "Pistol":
                mixer = g.mixer
                sound, sample_rate = soundfile.read("media/Audio/pistol_reload.ogg")
                sound = mixer.device.convert(sound, sample_rate)
                channel = mixer.play(sound=sound, volume=g.mixer_volume)
            if self.weapon == "MG":
                mixer = g.mixer
                sound, sample_rate = soundfile.read("media/Audio/mg_reload.ogg")
                sound = mixer.device.convert(sound, sample_rate)
                channel = mixer.play(sound=sound, volume=g.mixer_volume)
            self.engine.message_log.add_message(f"You reloaded your weapon.", colour.health_recovered)
        else:
            raise Impossible(f"You're already at maximum ammo")

class MineConsumable(Consumable):

    def activate(self, action: actions.ItemAction) -> None:
        mixer = g.mixer
        sound, sample_rate = soundfile.read("media/Audio/mine_set.ogg")
        sound = mixer.device.convert(sound, sample_rate)
        channel = mixer.play(sound=sound, volume=g.mixer_volume)
        consumer = action.entity
        self.engine.game_map.tiles[consumer.x, consumer.y] = tile_types.mine_active
        self.consume()