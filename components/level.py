from __future__ import annotations
from typing import TYPE_CHECKING
from components.base_component import BaseComponent

if TYPE_CHECKING:
    from entity import Actor

class Level(BaseComponent):
    parent: Actor

    def __init__(self, current_level: int = 1, current_xp: int = 0, level_up_base: int = 0, level_up_factor: int = 150, xp_given: int = 0):
        self.current_level = current_level
        self.current_xp = current_xp
        self.level_up_base = level_up_base
        self.level_up_factor = level_up_factor
        self.xp_given = xp_given

    @property
    def experience_to_next_level(self) -> int:
        return self.level_up_base + self.current_level * self.level_up_factor

    @property
    def requires_level_up(self) -> bool:
        if self.current_level < 22:
            return self.current_xp > self.experience_to_next_level

    def add_xp(self, xp: int) -> None:
        if xp == 0 or self.level_up_base == 0:
            return

        self.current_xp += xp

        self.engine.message_log.add_message(f"You gain {xp} exerience points.")

        if self.requires_level_up:
            self.engine.message_log.add_message(f"You reached level {self.current_level + 1}!")

    def increase_level(self) -> None:
        self.current_xp = 0

        self.current_level += 1

    def increase_max_hp(self, amount: int = 20) -> None:
        self.parent.soldier.max_hp += amount
        self.parent.soldier.hp += amount

        self.engine.message_log.add_message("Your health has increased")

        self.increase_level()

    def increase_strength(self, amount: int = 1) -> None:
        self.parent.soldier.base_strength += amount
        self.engine.message_log.add_message("You become stronger")
        self.increase_level()

    def increase_dexterity(self, amount: int = 1) -> None:
        self.parent.soldier.base_dexterity += amount
        self.engine.message_log.add_message("You become more dexterous")
        self.increase_level()

    def increase_vitality(self, amount: int = 1) -> None:
        self.parent.soldier.base_vitality += amount
        self.parent.soldier.max_hp += self.parent.soldier.base_vitality * 6
        self.parent.soldier.hp += self.parent.soldier.base_vitality * 6
        self.engine.message_log.add_message("Your health increased")
        self.increase_level()

    def increase_composure(self, amount: int = 1) -> None:
        self.parent.soldier.base_composure += amount
        self.engine.message_log.add_message("You have become more composed")
        self.increase_level()

    def increase_defense(self, amount: int = 1) -> None:
        self.parent.soldier.base_strength += amount
        self.engine.message_log.add_message("You can take more hits")
        self.increase_level()